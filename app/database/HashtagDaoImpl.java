package database;

import core.Query;
import core.RequestContext;
import core.RequestParameter;
import entities.Hashtag;
import entities.HashtagFollow;
import play.db.jpa.JPAApi;
import utils.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Singleton
public class HashtagDaoImpl extends AbstractJpaDao implements HashtagDao {

  @Inject
  public HashtagDaoImpl(DatabaseExecutionContext executionContext, JPAApi jpaApi) {
    super(executionContext, jpaApi);
  }

  /**** HASHTAGS ****/

  @Override
  public CompletableFuture<List<Hashtag>> queryHashtags(RequestContext context, Query query) {
    String currentUserId = context.getCurrentUserId();
    String input = query.getString(RequestParameter.INPUT);

    String sql =
      "SELECT DISTINCT hashtag, hashtagFollow.hashtagFollowId " +
        "FROM Hashtag hashtag " +
        "LEFT JOIN HashtagFollow hashtagFollow ON hashtagFollow.hashtag.hashtagId = hashtag.hashtagId AND hashtagFollow.createdBy.userId = ?0 " +
        "WHERE 1 = 1";

    if (StringUtils.isNotBlank(input)) {
      sql += " AND LOWER(hashtag.name) = '%" + input.toLowerCase() + "%'";
    }

    sql += " ORDER BY hashtag.postCount DESC, hashtag.name ASC";

    return selectResults(query, sql, currentUserId).thenApply(results -> results.stream()
                                                                                .map(result -> {
                                                                                  Hashtag hashtag = (Hashtag) result[0];
                                                                                  hashtag.setFollowing(result[1] != null);
                                                                                  return hashtag;
                                                                                })
                                                                                .collect(Collectors.toList()));
  }

  @Override
  public CompletableFuture<Hashtag> getHashtagById(RequestContext context, String hashtagId) {
    String query =
      "SELECT hashtag " +
        "FROM Hashtag hashtag " +
        "LEFT JOIN FETCH hashtag.createdBy " +
        "WHERE hashtag.hashtagId = ?0 OR hashtag.name = ?0";
    return findEntity(query, hashtagId);
  }

  @Override
  public CompletableFuture<Hashtag> resolveHashtag(RequestContext context, String name) {
    String sql =
      "SELECT hashtag " +
        "FROM Hashtag hashtag " +
        "LEFT JOIN FETCH hashtag.createdBy " +
        "WHERE LOWER(hashtag.name) = ?0";

    CompletableFuture<Hashtag> hashtagFuture = findEntity(sql, name.toLowerCase());

    return hashtagFuture.thenComposeAsync(hashtag -> {
      if (hashtag == null) {
        hashtag = new Hashtag();
        hashtag.setName(name);
        hashtag.setCreatedBy(context.getCurrentUser());
        return saveHashtag(context, hashtag);
      }

      return CompletableFuture.completedFuture(hashtag);
    });
  }

  @Override
  public CompletableFuture<Hashtag> saveHashtag(RequestContext context, Hashtag hashtag) {
    return saveEntity(hashtag);
  }

  /**** HASHTAG FOLLOWS ****/

  @Override
  public CompletableFuture<List<HashtagFollow>> queryHashtagFollows(RequestContext context, String hashtagId, Query query) {
    String sql =
      "SELECT DISTINCT hashtagFollow " +
        "FROM HashtagFollow hashtagFollow " +
        "LEFT JOIN FETCH hashtagFollow.hashtag hashtag" +
        "LEFT JOIN FETCH hashtagFollow.createdBy createdBy " +
        "LEFT JOIN FETCH createdBy.profileImage " +
        "WHERE 1 = 1";

    if (StringUtils.isNotBlank(hashtagId)) {
      sql += " hashtag.hashtagId = '" + hashtagId + "'";
    }

    sql += " ORDER BY hashtag.name ASC, createdBy.displayName ASC";

    return selectEntities(query, sql);
  }

  @Override
  public CompletableFuture<HashtagFollow> getHashtagFollow(RequestContext context, String hashtagId) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT hashtagFollow " +
        "FROM HashtagFollow hashtagFollow " +
        "WHERE hashtagFollow.hashtag.hashtagId = ?0 AND hashtagFollow.createdBy.userId = ?1";

    return findEntity(sql, hashtagId, currentUserId);
  }

  @Override
  public CompletableFuture<HashtagFollow> saveHashtagFollow(RequestContext context, HashtagFollow hashtagFollow) {
    return saveEntity(hashtagFollow);
  }

  @Override
  public CompletableFuture<Void> removeHashtagFollow(RequestContext context, HashtagFollow hashtagFollow) {
    return removeEntity(hashtagFollow);
  }

}
