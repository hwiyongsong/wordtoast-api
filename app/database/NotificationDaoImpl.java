package database;

import core.Query;
import core.RequestContext;
import entities.Notification;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Singleton
public class NotificationDaoImpl extends AbstractJpaDao implements NotificationDao {

  @Inject
  public NotificationDaoImpl(DatabaseExecutionContext executionContext, JPAApi jpaApi) {
    super(executionContext, jpaApi);
  }

  @Override
  public CompletableFuture<List<Notification>> queryNotifications(RequestContext context, Query query) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT DISTINCT notification " +
        "FROM Notification notification " +
        "LEFT JOIN FETCH notification.recipient " +
        "LEFT JOIN FETCH notification.actor notificationActor " +
        "LEFT JOIN FETCH notificationActor.profileImage " +
        "LEFT JOIN FETCH notification.post " +
        "LEFT JOIN FETCH notification.postReaction " +
        "LEFT JOIN FETCH notification.postComment " +
        "WHERE notification.recipient.userId = ?0 " +
        "ORDER BY notification.createdAt DESC";

    return selectEntities(query, sql, currentUserId);
  }

  @Override
  public CompletableFuture<Notification> getNotificationById(RequestContext context, String notificationId) {
    String currentUserId = context.getCurrentUserId();

    String query =
      "SELECT notification " +
        "FROM Notification notification " +
        "LEFT JOIN FETCH notification.recipient " +
        "LEFT JOIN FETCH notification.actor notificationActor " +
        "LEFT JOIN FETCH notificationActor.profileImage " +
        "LEFT JOIN FETCH notification.post " +
        "LEFT JOIN FETCH notification.postReaction " +
        "LEFT JOIN FETCH notification.postComment " +
        "WHERE notification.notificationId = ?0 AND notification.recipient.userId = ?1";

    return findEntity(query, notificationId, currentUserId);
  }

  @Override
  public CompletableFuture<Notification> saveNotification(RequestContext context, Notification notification) {
    return saveEntity(notification);
  }

  @Override
  public CompletableFuture<Void> removeNotification(RequestContext context, Notification notification) {
    return removeEntity(notification);
  }

  @Override
  public CompletableFuture<Integer> countUnreadNotifications(RequestContext context) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT COUNT(notification) " +
        "FROM Notification notification " +
        "WHERE notification.recipient.userId = ?0 AND notification.read = false";

    return count(sql, currentUserId);
  }

  @Override
  public CompletableFuture<Integer> markNotificationsAsRead(RequestContext context) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "UPDATE Notification notification " +
        "SET notification.read = true " +
        "WHERE notification.recipient.userId = ?0";

    return update(sql, currentUserId);
  }

}
