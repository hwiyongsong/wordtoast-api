package database;

import core.Query;
import core.RequestContext;
import core.RequestParameter;
import entities.Post;
import entities.PostBlock;
import entities.PostBookmark;
import entities.PostComment;
import entities.PostReaction;
import entities.PostType;
import entities.PublishStatus;
import play.db.jpa.JPAApi;
import utils.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Singleton
public class PostDaoImpl extends AbstractJpaDao implements PostDao {

  @Inject
  public PostDaoImpl(DatabaseExecutionContext executionContext, JPAApi jpaApi) {
    super(executionContext, jpaApi);
  }

  /* POSTS */

  @Override
  public CompletableFuture<List<Post>> queryPosts(RequestContext context, Query query) {
    PostType postType = query.getEnum(PostType.class, RequestParameter.POST_TYPE);
    PublishStatus publishStatus = query.getEnumOrElse(PublishStatus.class, RequestParameter.PUBLISH_STATUS, PublishStatus.PUBLISHED);
    String userId = query.getString(RequestParameter.USER_ID);
    String topicId = query.getString(RequestParameter.TOPIC_ID);
    String promptId = query.getString(RequestParameter.PROMPT_ID);
    String hashtagId = query.getString(RequestParameter.HASHTAG_ID);
    String input = query.getString(RequestParameter.INPUT);

    String sql =
      "SELECT DISTINCT post " +
        "FROM Post post " +
        "LEFT JOIN FETCH post.coverImage " +
        "LEFT JOIN FETCH post.topic postTopic " +
        "LEFT JOIN FETCH post.prompt postPrompt " +
        "LEFT JOIN FETCH post.hashtags postHashtags " +
        "LEFT JOIN FETCH post.createdBy postCreatedBy " +
        "LEFT JOIN FETCH postCreatedBy.profileImage " +
        "WHERE 1 = 1";

    if (postType != null) {
      sql += " AND post.postType = '" + postType + "'";
    }

    if (publishStatus != null) {
      sql += " AND post.publishStatus = '" + publishStatus + "'";
    }

    if (StringUtils.isNotBlank(input)) {
      sql += " AND (UPPER(post.title) LIKE '%" + input.toUpperCase() + "%' OR UPPER(postCreatedBy.displayName) LIKE '%" + input.toUpperCase() + "%')";
    }

    if (StringUtils.isNotBlank(userId)) {
      sql += " AND postCreatedBy.userId = '" + userId + "'";
    }

    if (StringUtils.isNotBlank(topicId)) {
      sql += " AND postTopic.topicId = '" + topicId + "'";
    }

    if (StringUtils.isNotBlank(promptId)) {
      sql += " AND postPrompt.postId = '" + promptId + "'";
    }

    if (StringUtils.isNotBlank(hashtagId)) {
      sql += " AND (postHashtags.hashtagId = '" + hashtagId + "' OR postHashtags.name = '" + hashtagId + "')";
    }

    sql += " ORDER BY post.createdAt DESC";

    return selectEntities(query, sql);
  }

  @Override
  public CompletableFuture<Post> getPostById(RequestContext context, String postId) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT post, currentUserFollow.userFollowId, currentUserFriend.userFriendId " +
        "FROM Post post " +
        "LEFT JOIN FETCH post.coverImage " +
        "LEFT JOIN FETCH post.topic " +
        "LEFT JOIN FETCH post.prompt " +
        "LEFT JOIN FETCH post.createdBy postCreatedBy " +
        "LEFT JOIN FETCH postCreatedBy.profileImage " +
        "LEFT JOIN FETCH post.hashtags " +
        "LEFT JOIN UserFollow currentUserFollow ON currentUserFollow.user.userId = postCreatedBy.userId AND currentUserFollow.createdBy.userId = ?0 " +
        "LEFT JOIN UserFriend currentUserFriend ON currentUserFriend.user.userId = postCreatedBy.userId AND currentUserFriend.createdBy.userId = ?0 " +
        "WHERE post.postId = ?1";

    return findResult(sql, currentUserId, postId).thenApply(result -> {
      if (result == null) {
        return null;
      }

      Post post = (Post) result[0];
      post.getCreatedBy().setFollowing(result[1] != null);
      post.getCreatedBy().setFriended(result[2] != null);
      return post;
    });
  }

  @Override
  public CompletableFuture<Post> savePost(RequestContext context, Post post) {
    return saveEntity(post);
  }

  @Override
  public CompletableFuture<Void> removePost(RequestContext context, Post post) {
    return removeEntity(post);
  }

  /* POST REACTIONS */

  @Override
  public CompletableFuture<List<PostReaction>> queryPostReactions(RequestContext context, String postId, Query query) {
    String sql =
      "SELECT DISTINCT postReaction " +
        "FROM PostReaction postReaction " +
        "LEFT JOIN FETCH postReaction.post postReactionPost " +
        "LEFT JOIN FETCH postReaction.createdBy postReactionCreatedBy " +
        "LEFT JOIN FETCH postReactionCreatedBy.profileImage " +
        "WHERE postReactionPost.postId = ?0 " +
        "ORDER BY postReaction.createdAt DESC";

    return selectEntities(query, sql, postId);
  }

  @Override
  public CompletableFuture<PostReaction> getPostReaction(RequestContext context, String postId) {
    String currentUserId = context.getCurrentUserId();

    String query =
      "SELECT postReaction " +
        "FROM PostReaction postReaction " +
        "WHERE postReaction.post.postId = ?0 AND postReaction.createdBy.userId = ?1";

    return findEntity(query, postId, currentUserId);
  }

  @Override
  public CompletableFuture<PostReaction> savePostReaction(RequestContext context, PostReaction postReaction) {
    return saveEntity(postReaction);
  }

  @Override
  public CompletableFuture<Void> removePostReaction(RequestContext context, PostReaction postReaction) {
    return removeEntity(postReaction);
  }

  /* POST COMMENTS */

  @Override
  public CompletableFuture<List<PostComment>> queryPostComments(RequestContext context, String postId, Query query) {
    String sql =
      "SELECT DISTINCT postComment " +
        "FROM PostComment postComment " +
        "LEFT JOIN FETCH postComment.post postCommentPost " +
        "LEFT JOIN FETCH postComment.createdBy postCommentCreatedBy " +
        "LEFT JOIN FETCH postCommentCreatedBy.profileImage " +
        "WHERE postCommentPost.postId = ?0 " +
        "ORDER BY postComment.createdAt DESC";

    return selectEntities(query, sql, postId);
  }

  @Override
  public CompletableFuture<PostComment> getPostCommentById(RequestContext context, String postCommentId) {
    String sql =
      "SELECT postComment " +
        "FROM PostComment postComment " +
        "LEFT JOIN FETCH postComment.post " +
        "LEFT JOIN FETCH postComment.createdBy postCommentCreatedBy " +
        "LEFT JOIN FETCH postCommentCreatedBy.profileImage " +
        "WHERE postComment.postCommentId = ?0";

    return findEntity(sql, postCommentId);
  }

  @Override
  public CompletableFuture<PostComment> savePostComment(RequestContext context, PostComment postComment) {
    return saveEntity(postComment);
  }

  @Override
  public CompletableFuture<Void> removePostComment(RequestContext context, PostComment postComment) {
    return removeEntity(postComment);
  }

  /* POST BOOKMARKS */

  @Override
  public CompletableFuture<List<PostBookmark>> queryPostBookmarks(RequestContext context, Query query) {
    String userId = query.getString(RequestParameter.USER_ID);
    String postId = query.getString(RequestParameter.POST_ID);

    String sql =
      "SELECT DISTINCT pb " +
        "FROM PostBookmark pb " +
        "LEFT JOIN FETCH pb.post p " +
        "LEFT JOIN FETCH p.coverImage " +
        "LEFT JOIN FETCH pb.createdBy cb " +
        "LEFT JOIN FETCH cb.profileImage " +
        "WHERE 1 = 1";

    if (StringUtils.isNotBlank(userId)) {
      sql += " AND pb.createdBy.userId = '" + userId + "'";
    }

    if (StringUtils.isNotBlank(postId)) {
      sql += " AND pb.post.postId = '" + postId + "'";
    }

    sql += " ORDER BY pb.post.title ASC";

    return selectEntities(query, sql);
  }

  @Override
  public CompletableFuture<PostBookmark> getPostBookmark(RequestContext context, String postId, String userId) {
    String query =
      "SELECT pb " +
        "FROM PostBookmark pb " +
        "LEFT JOIN FETCH pb.post p " +
        "LEFT JOIN FETCH p.coverImage " +
        "LEFT JOIN FETCH pb.createdBy cb " +
        "LEFT JOIN FETCH cb.profileImage " +
        "WHERE pb.post.postId = ?0 AND pb.createdBy.userId = ?1";
    return findEntity(query, postId, userId);
  }

  @Override
  public CompletableFuture<PostBookmark> savePostBookmark(RequestContext context, PostBookmark postBookmark) {
    return saveEntity(postBookmark);
  }

  @Override
  public CompletableFuture<Void> removePostBookmark(RequestContext context, PostBookmark postBookmark) {
    return removeEntity(postBookmark);
  }

  /* POST BLOCKS */

  @Override
  public CompletableFuture<PostBlock> getPostBlock(RequestContext context, String postId) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT postBlock " +
        "FROM PostBlock postBlock " +
        "WHERE postBlock.post.postId = ?0 AND postBlock.createdBy.userId = ?1";

    return findEntity(sql, postId, currentUserId);
  }

  @Override
  public CompletableFuture<PostBlock> savePostBlock(RequestContext context, PostBlock postBlock) {
    return saveEntity(postBlock);
  }

  @Override
  public CompletableFuture<Void> removePostBlock(RequestContext context, PostBlock postBlock) {
    return removeEntity(postBlock);
  }

}
