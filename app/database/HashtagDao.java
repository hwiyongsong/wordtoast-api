package database;

import com.google.inject.ImplementedBy;
import core.Query;
import core.RequestContext;
import entities.Hashtag;
import entities.HashtagFollow;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@ImplementedBy(HashtagDaoImpl.class)
public interface HashtagDao {

  /* HASHTAGS */

  CompletableFuture<List<Hashtag>> queryHashtags(RequestContext context, Query query);

  CompletableFuture<Hashtag> getHashtagById(RequestContext context, String hashtagId);

  CompletableFuture<Hashtag> resolveHashtag(RequestContext context, String name);

  CompletableFuture<Hashtag> saveHashtag(RequestContext context, Hashtag hashtag);

  /* HASHTAG FOLLOWS */

  CompletableFuture<List<HashtagFollow>> queryHashtagFollows(RequestContext context, String hashtagId, Query query);

  CompletableFuture<HashtagFollow> getHashtagFollow(RequestContext context, String hashtagId);

  CompletableFuture<HashtagFollow> saveHashtagFollow(RequestContext context, HashtagFollow hashtagFollow);

  CompletableFuture<Void> removeHashtagFollow(RequestContext context, HashtagFollow hashtagFollow);

}
