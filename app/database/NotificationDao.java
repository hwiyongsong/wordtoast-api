package database;

import com.google.inject.ImplementedBy;
import core.Query;
import core.RequestContext;
import entities.Notification;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@ImplementedBy(NotificationDaoImpl.class)
public interface NotificationDao {

  CompletableFuture<List<Notification>> queryNotifications(RequestContext context, Query query);

  CompletableFuture<Notification> getNotificationById(RequestContext context, String notificationId);

  CompletableFuture<Notification> saveNotification(RequestContext context, Notification notification);

  CompletableFuture<Void> removeNotification(RequestContext context, Notification notification);

  CompletableFuture<Integer> countUnreadNotifications(RequestContext context);

  CompletableFuture<Integer> markNotificationsAsRead(RequestContext context);

}
