package database;

import core.Query;
import core.RequestPagination;
import entities.AbstractEntity;
import play.db.jpa.JPAApi;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import static java.util.concurrent.CompletableFuture.runAsync;
import static java.util.concurrent.CompletableFuture.supplyAsync;

public abstract class AbstractJpaDao {

  private DatabaseExecutionContext executionContext;
  private JPAApi jpaApi;

  public AbstractJpaDao(DatabaseExecutionContext executionContext, JPAApi jpaApi) {
    this.executionContext = executionContext;
    this.jpaApi = jpaApi;
  }

  protected CompletableFuture<List<Object[]>> selectResults(String sql, Object... paramValues) {
    return selectResults(null, sql, paramValues);
  }

  @SuppressWarnings("unchecked")
  protected CompletableFuture<List<Object[]>> selectResults(Query query, String sql, Object... paramValues) {
    return supplyAsync(() -> {
      return withTransaction(em -> {
        javax.persistence.Query nativeQuery = em.createQuery(sql);
        decorateWithParameter(nativeQuery, paramValues);
        decorateWithPagination(nativeQuery, query.getPagination());
        return nativeQuery.getResultList();
      });
    }, executionContext);
  }

  protected <T extends AbstractEntity> CompletableFuture<List<T>> selectEntities(String sql, Object... paramValues) {
    return selectEntities(null, sql, paramValues);
  }

  @SuppressWarnings("unchecked")
  protected <T extends AbstractEntity> CompletableFuture<List<T>> selectEntities(Query query, String sql, Object... paramValues) {
    return supplyAsync(() -> {
      return withTransaction(em -> {
        javax.persistence.Query nativeQuery = em.createQuery(sql);
        decorateWithParameter(nativeQuery, paramValues);
        decorateWithPagination(nativeQuery, query.getPagination());
        return (List<T>) nativeQuery.getResultList();
      });
    }, executionContext);
  }

  @SuppressWarnings("unchecked")
  protected CompletableFuture<Object[]> findResult(String sql, Object... paramValues) {
    return supplyAsync(() -> {
      return withTransaction(em -> {
        javax.persistence.Query nativeQuery = em.createQuery(sql);
        decorateWithParameter(nativeQuery, paramValues);
        List<Object[]> results = nativeQuery.getResultList();
        return results.size() > 0 ? results.get(0) : null;
      });
    }, executionContext);
  }

  @SuppressWarnings("unchecked")
  protected <T extends AbstractEntity> CompletableFuture<T> findEntity(String sql, Object... paramValues) {
    return supplyAsync(() -> {
      return withTransaction(em -> {
        try {
          javax.persistence.Query query = em.createQuery(sql);
          decorateWithParameter(query, paramValues);
          return (T) query.getSingleResult();
        } catch (NoResultException e) {
          return null;
        }
      });
    }, executionContext);
  }

  protected <T extends AbstractEntity> CompletableFuture<T> saveEntity(T entity) {
    return supplyAsync(() -> {
      return withTransaction(em -> em.merge(entity));
    }, executionContext);
  }

  protected <T extends AbstractEntity> CompletableFuture<Void> removeEntity(T entity) {
    return runAsync(() -> {
      withTransaction(em -> {
        T merged = em.merge(entity);
        em.remove(merged);
        return null;
      });
    }, executionContext);
  }

  protected CompletableFuture<Integer> update(String sql, Object... paramValues) {
    return supplyAsync(() -> {
      return withTransaction(em -> {
        javax.persistence.Query query = em.createQuery(sql);
        decorateWithParameter(query, paramValues);
        return query.executeUpdate();
      });
    }, executionContext);
  }

  protected CompletableFuture<Integer> count(String sql, Object... paramValues) {
    return supplyAsync(() -> {
      return withTransaction(em -> {
        javax.persistence.Query query = em.createQuery(sql);
        decorateWithParameter(query, paramValues);
        Number result = (Number) query.getSingleResult();
        return result.intValue();
      });
    }, executionContext);
  }

  private <T> T withTransaction(Function<EntityManager, T> function) {
    return jpaApi.withTransaction(function);
  }

  private javax.persistence.Query decorateWithParameter(javax.persistence.Query query, Object... paramValues) {
    for (int paramIndex = 0; paramIndex < paramValues.length; paramIndex++) {
      Object paramValue = paramValues[paramIndex];

      if (paramValue instanceof String) {
        query.setParameter(paramIndex, paramValue.toString());
      } else if (paramValue instanceof LocalDate) {
        LocalDate localDate = (LocalDate) paramValue;
        query.setParameter(paramIndex, java.sql.Date.valueOf(localDate));
      } else {
        query.setParameter(paramIndex, paramValue);
      }
    }

    return query;
  }

  private javax.persistence.Query decorateWithPagination(javax.persistence.Query query, RequestPagination requestPagination) {
    if (requestPagination != null) {
      int limit = requestPagination.getLimit();
      int offset = requestPagination.getOffset();

      query.setMaxResults(limit);
      query.setFirstResult(offset);
    }

    return query;
  }

}
