package database;

import core.Query;
import core.RequestContext;
import core.RequestParameter;
import entities.FriendRequest;
import entities.User;
import entities.UserBlock;
import entities.UserFollow;
import entities.UserFriend;
import play.db.jpa.JPAApi;
import utils.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Singleton
public class UserDaoImpl extends AbstractJpaDao implements UserDao {

  @Inject
  public UserDaoImpl(DatabaseExecutionContext executionContext, JPAApi jpaApi) {
    super(executionContext, jpaApi);
  }

  /**** USERS ****/

  @Override
  public CompletableFuture<List<User>> queryUsers(RequestContext context, Query query) {
    String currentUserId = context.getCurrentUserId();
    String input = query.getString(RequestParameter.INPUT);

    String sql =
      "SELECT DISTINCT user, currentUserFollow.userFollowId, currentUserFriend.userFriendId " +
        "FROM User user " +
        "LEFT JOIN FETCH user.profileImage " +
        "LEFT JOIN UserFollow currentUserFollow ON currentUserFollow.user.userId = user.userId AND currentUserFollow.createdBy.userId = ?0 " +
        "LEFT JOIN UserFriend currentUserFriend ON currentUserFriend.user.userId = user.userId AND currentUserFriend.createdBy.userId = ?0 " +
        "WHERE 1 = 1";

    if (StringUtils.isNotBlank(input)) {
      sql += " AND UPPER(user.displayName) LIKE '%" + input.toUpperCase() + "%'";
    }

    sql += " ORDER BY user.displayName";

    return selectResults(query, sql, currentUserId).thenApply(results -> results.stream()
                                                                                .map(result -> {
                                                                                  User user = (User) result[0];
                                                                                  user.setFollowing(result[1] != null);
                                                                                  user.setFriended(result[2] != null);
                                                                                  return user;
                                                                                })
                                                                                .collect(Collectors.toList()));
  }

  @Override
  public CompletableFuture<User> getUserById(RequestContext context, String userId) {
    String currentUserId = context != null ? context.getCurrentUserId() : StringUtils.EMPTY;

    String sql =
      "SELECT DISTINCT user, currentUserFollow.userFollowId, currentUserFriend.userFriendId, currentFriendRequest.friendRequestId " +
        "FROM User user " +
        "LEFT JOIN FETCH user.profileImage " +
        "LEFT JOIN UserFollow currentUserFollow ON currentUserFollow.user.userId = user.userId AND currentUserFollow.createdBy.userId = ?0 " +
        "LEFT JOIN UserFriend currentUserFriend ON currentUserFriend.user.userId = user.userId AND currentUserFriend.createdBy.userId = ?0 " +
        "LEFT JOIN FriendRequest currentFriendRequest ON currentFriendRequest.user.userId = user.userId AND currentFriendRequest.createdBy.userId = ?0 AND currentFriendRequest.status = 'PENDING' " +
        "WHERE user.userId = ?1 OR user.firebaseUserId = ?1";

    return findResult(sql, currentUserId, userId).thenApply(result -> {
      if (result == null) {
        return null;
      }

      User user = (User) result[0];
      user.setFollowing(result[1] != null);
      user.setFriended(result[2] != null);
      user.setFriending(result[3] != null);
      return user;
    });
  }

  @Override
  public CompletableFuture<User> getUserByFirebaseUserId(String firebaseUserId) {
    String sql =
      "SELECT user " +
        "FROM User user " +
        "WHERE user.firebaseUserId = ?0";

    return findEntity(sql, firebaseUserId);
  }

  @Override
  public CompletableFuture<User> saveUser(RequestContext context, User user) {
    return saveEntity(user);
  }

  /**** USER FOLLOWS ****/

  @Override
  public CompletableFuture<List<User>> queryUserFollowers(RequestContext context, Query query) {
    String currentUserId = context.getCurrentUserId();
    String userId = query.getString(RequestParameter.USER_ID);

    String sql =
      "SELECT DISTINCT userFollow.createdBy, currentUserFollow.userFollowId, currentUserFriend.userFriendId, currentFriendRequest.friendRequestId " +
        "FROM UserFollow userFollow " +
        "LEFT JOIN UserFollow currentUserFollow ON currentUserFollow.user.userId = userFollow.createdBy.userId AND currentUserFollow.createdBy.userId = ?0 " +
        "LEFT JOIN UserFriend currentUserFriend ON currentUserFriend.user.userId = userFollow.createdBy.userId AND currentUserFriend.createdBy.userId = ?0 " +
        "LEFT JOIN FriendRequest currentFriendRequest ON currentFriendRequest.user.userId = userFollow.createdBy.userId AND currentFriendRequest.createdBy.userId = ?0 AND currentFriendRequest.status = 'PENDING' " +
        "WHERE userFollow.user.userId = ?1 " +
        "ORDER BY userFollow.createdBy.displayName ASC";

    return selectResults(query, sql, currentUserId, userId).thenApply(results -> results.stream()
                                                                                        .map(result -> {
                                                                                          User user = (User) result[0];
                                                                                          user.setFollowing(result[1] != null);
                                                                                          user.setFriended(result[2] != null);
                                                                                          user.setFriending(result[3] != null);
                                                                                          return user;
                                                                                        })
                                                                                        .collect(Collectors.toList()));
  }

  @Override
  public CompletableFuture<List<User>> queryUserFollowings(RequestContext context, Query query) {
    String currentUserId = context.getCurrentUserId();
    String userId = query.getString(RequestParameter.USER_ID);

    String sql =
      "SELECT DISTINCT userFollow.user, currentUserFollow.userFollowId, currentUserFriend.userFriendId, currentFriendRequest.friendRequestId " +
        "FROM UserFollow userFollow " +
        "LEFT JOIN UserFollow currentUserFollow ON currentUserFollow.user.userId = userFollow.user.userId AND currentUserFollow.createdBy.userId = ?0 " +
        "LEFT JOIN UserFriend currentUserFriend ON currentUserFriend.user.userId = userFollow.user.userId AND currentUserFriend.createdBy.userId = ?0 " +
        "LEFT JOIN FriendRequest currentFriendRequest ON currentFriendRequest.user.userId = userFollow.user.userId AND currentFriendRequest.createdBy.userId = ?0 AND currentFriendRequest.status = 'PENDING' " +
        "WHERE userFollow.createdBy.userId = ?1 " +
        "ORDER BY userFollow.user.displayName ASC";

    return selectResults(query, sql, currentUserId, userId).thenApply(results -> results.stream()
                                                                                        .map(result -> {
                                                                                          User user = (User) result[0];
                                                                                          user.setFollowing(result[1] != null);
                                                                                          user.setFriended(result[2] != null);
                                                                                          user.setFriending(result[3] != null);
                                                                                          return user;
                                                                                        })
                                                                                        .collect(Collectors.toList()));
  }

  @Override
  public CompletableFuture<UserFollow> getUserFollow(RequestContext context, String userId) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT userFollow " +
        "FROM UserFollow userFollow " +
        "WHERE userFollow.user.userId = ?0 AND userFollow.createdBy.userId = ?1";

    return findEntity(sql, userId, currentUserId);
  }

  @Override
  public CompletableFuture<UserFollow> saveUserFollow(RequestContext context, UserFollow follow) {
    return saveEntity(follow);
  }

  @Override
  public CompletableFuture<Void> removeUserFollow(RequestContext context, UserFollow follow) {
    return removeEntity(follow);
  }

  /* USER FRIENDS */

  @Override
  public CompletableFuture<List<User>> queryUserFriends(RequestContext context, String userId, Query query) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT DISTINCT userFriend.user, currentUserFollow.userFollowId, currentUserFriend.userFriendId, currentFriendRequest.friendRequestId " +
        "FROM UserFriend userFriend " +
        "LEFT JOIN FETCH userFriend.user.profileImage " +
        "LEFT JOIN UserFollow currentUserFollow ON currentUserFollow.user.userId = userFriend.user.userId AND currentUserFollow.createdBy.userId = ?0 " +
        "LEFT JOIN UserFriend currentUserFriend ON currentUserFriend.user.userId = userFriend.user.userId AND currentUserFriend.createdBy.userId = ?0 " +
        "LEFT JOIN FriendRequest currentFriendRequest ON currentFriendRequest.user.userId = userFriend.user.userId AND currentFriendRequest.createdBy.userId = ?0 AND currentFriendRequest.status = 'PENDING' " +
        "WHERE userFriend.createdBy.userId = ?1 " +
        "ORDER BY userFriend.user.displayName DESC";

    return selectResults(query, sql, currentUserId, userId).thenApply(results -> results.stream()
                                                                                        .map(result -> {
                                                                                          User user = (User) result[0];
                                                                                          user.setFollowing(result[1] != null);
                                                                                          user.setFriended(result[2] != null);
                                                                                          user.setFriending(result[3] != null);
                                                                                          return user;
                                                                                        })
                                                                                        .collect(Collectors.toList()));
  }

  @Override
  public CompletableFuture<UserFriend> saveUserFriend(RequestContext context, UserFriend userFriend) {
    return saveEntity(userFriend);
  }

  /* FRIEND REQUESTS */

  @Override
  public CompletableFuture<FriendRequest> getFriendRequestById(RequestContext context, String friendRequestId) {
    String sql =
      "SELECT friendRequest " +
        "FROM FriendRequest friendRequest " +
        "LEFT JOIN FETCH friendRequest.user friendRequestUser " +
        "LEFT JOIN FETCH friendRequestUser.profileImage " +
        "LEFT JOIN FETCH friendRequest.createdBy friendRequestCreatedBy " +
        "LEFT JOIN FETCH friendRequestCreatedBy.profileImage " +
        "WHERE friendRequest.friendRequestId = ?0";

    return findEntity(sql, friendRequestId);
  }

  @Override
  public CompletableFuture<FriendRequest> saveFriendRequest(RequestContext context, FriendRequest friendRequest) {
    return saveEntity(friendRequest);
  }

  @Override
  public CompletableFuture<Void> removeFriendRequest(RequestContext context, FriendRequest friendRequest) {
    return removeEntity(friendRequest);
  }

  /* USER BLOCKS */

  @Override
  public CompletableFuture<UserBlock> getUserBlock(RequestContext context, String userId) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT userBlock " +
        "FROM UserBlock userBlock " +
        "WHERE userBlock.userId = ?0 AND userBlock.createdBy.userId = ?1";

    return findEntity(sql, userId, currentUserId);
  }

  @Override
  public CompletableFuture<UserBlock> saveUserBlock(RequestContext context, UserBlock userBlock) {
    return saveEntity(userBlock);
  }

  @Override
  public CompletableFuture<Void> removeUserBlock(RequestContext context, UserBlock userBlock) {
    return removeEntity(userBlock);
  }

}
