package database;

import com.google.inject.ImplementedBy;
import core.RequestContext;
import entities.Image;

import java.util.concurrent.CompletableFuture;

@ImplementedBy(ImageDaoImpl.class)
public interface ImageDao {

  CompletableFuture<Image> getImageById(RequestContext context, String imageId);

  CompletableFuture<Image> saveImage(RequestContext context, Image image);

}
