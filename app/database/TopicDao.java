package database;

import com.google.inject.ImplementedBy;
import core.Query;
import core.RequestContext;
import entities.Topic;
import entities.TopicFollow;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@ImplementedBy(TopicDaoImpl.class)
public interface TopicDao {

  /**** TOPICS ****/

  CompletableFuture<List<Topic>> queryTopics(RequestContext context, Query query);

  CompletableFuture<Topic> getTopicById(RequestContext context, String topicId);

  /**** TOPIC FOLLOWS ****/

  CompletableFuture<TopicFollow> getTopicFollow(RequestContext context, String topicId);

  CompletableFuture<TopicFollow> saveTopicFollow(RequestContext context, TopicFollow follow);

  CompletableFuture<Void> removeTopicFollow(RequestContext context, TopicFollow follow);

}
