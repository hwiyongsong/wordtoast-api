package database;

import core.Query;
import core.RequestContext;
import core.RequestParameter;
import entities.Topic;
import entities.TopicFollow;
import play.db.jpa.JPAApi;
import utils.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Singleton
public class TopicDaoImpl extends AbstractJpaDao implements TopicDao {

  @Inject
  public TopicDaoImpl(DatabaseExecutionContext executionContext, JPAApi jpaApi) {
    super(executionContext, jpaApi);
  }

  /**** TOPICS ****/

  @Override
  public CompletableFuture<List<Topic>> queryTopics(RequestContext context, Query query) {
    String currentUserId = context.getCurrentUserId();
    String input = query.getString(RequestParameter.INPUT);

    String sql =
      "SELECT DISTINCT topic, currentTopicFollow.topicFollowId " +
        "FROM Topic topic " +
        "LEFT JOIN FETCH topic.posterImage " +
        "LEFT JOIN TopicFollow currentTopicFollow ON currentTopicFollow.topic.topicId = topic.topicId AND currentTopicFollow.createdBy.userId = ?0 " +
        "WHERE 1 = 1";

    if (StringUtils.isNotBlank(input)) {
      sql += " AND UPPER(topic.title) LIKE '%" + input.toUpperCase() + "%'";
    }

    sql += " ORDER BY topic.title";

    return selectResults(query, sql, currentUserId).thenApply(results -> results.stream()
                                                                                .map(result -> {
                                                                                  Topic topic = (Topic) result[0];
                                                                                  topic.setFollowing(result[1] != null);
                                                                                  return topic;
                                                                                })
                                                                                .collect(Collectors.toList()));
  }

  @Override
  public CompletableFuture<Topic> getTopicById(RequestContext context, String topicId) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT topic, currentTopicFollow.topicFollowId " +
        "FROM Topic topic " +
        "LEFT JOIN FETCH topic.posterImage " +
        "LEFT JOIN TopicFollow currentTopicFollow ON currentTopicFollow.topic.topicId = topic.topicId AND currentTopicFollow.createdBy.userId = ?0 " +
        "WHERE topic.topicId = ?1";

    return findResult(sql, currentUserId, topicId).thenApply(result -> {
      if (result == null) {
        return null;
      }

      Topic topic = (Topic) result[0];
      topic.setFollowing(result[1] != null);
      return topic;
    });
  }

  /**** TOPIC FOLLOWS ****/

  @Override
  public CompletableFuture<TopicFollow> getTopicFollow(RequestContext context, String topicId) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT topicFollow " +
        "FROM TopicFollow topicFollow " +
        "WHERE topicFollow.topic.topicId = ?0 AND topicFollow.createdBy.userId = ?1";

    return findEntity(sql, topicId, currentUserId);
  }

  @Override
  public CompletableFuture<TopicFollow> saveTopicFollow(RequestContext context, TopicFollow follow) {
    return saveEntity(follow);
  }

  @Override
  public CompletableFuture<Void> removeTopicFollow(RequestContext context, TopicFollow follow) {
    return removeEntity(follow);
  }

}
