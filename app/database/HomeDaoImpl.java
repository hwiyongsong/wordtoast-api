package database;

import core.Query;
import core.RequestContext;
import entities.AbstractEntity;
import entities.Post;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Singleton
public class HomeDaoImpl extends AbstractJpaDao implements HomeDao {

  @Inject
  public HomeDaoImpl(DatabaseExecutionContext executionContext, JPAApi jpaApi) {
    super(executionContext, jpaApi);
  }

  @Override
  public CompletableFuture<List<AbstractEntity>> queryForYou(RequestContext context, Query query) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT DISTINCT post, postReaction.postReactionId, postBookmark.postBookmarkId, " +
        "((UNIX_TIMESTAMP(post.updatedAt) - 1577836800) / 100000) + " +
        "(post.likeCount * 0.25) + (post.commentCount * 0.25) + " +
        "(postCreatedBy.followerCount * 0.1) + " +
        "((CASE WHEN userFriend.userFriendId IS NOT NULL THEN 1 ELSE 0 END) * 100) AS relevanceScore " +
        "FROM Post post " +
        "LEFT JOIN FETCH post.coverImage " +
        "LEFT JOIN FETCH post.topic " +
        "LEFT JOIN FETCH post.prompt " +
        "LEFT JOIN FETCH post.createdBy postCreatedBy " +
        "LEFT JOIN FETCH postCreatedBy.profileImage " +
        "LEFT JOIN PostReaction postReaction ON postReaction.post.postId = post.postId AND postReaction.createdBy.userId = ?0 " +
        "LEFT JOIN PostBookmark postBookmark ON postBookmark.post.postId = post.postId AND postBookmark.createdBy.userId = ?0 " +
        "LEFT JOIN TopicFollow topicFollow ON topicFollow.topic.topicId = post.topic.topicId AND topicFollow.createdBy.userId = ?0 " +
        "LEFT JOIN UserFollow userFollow ON userFollow.user.userId = post.createdBy.userId AND userFollow.createdBy.userId = ?0 " +
        "LEFT JOIN UserFriend userFriend ON userFriend.user.userId = post.createdBy.userId AND userFriend.createdBy.userId = ?0 " +
        "LEFT JOIN PostBlock postBlock ON postBlock.post.postId = post.postId AND postBlock.createdBy.userId = ?0 " +
        "LEFT JOIN UserBlock userBlock ON userBlock.user.userId = post.createdBy.userId AND userBlock.createdBy.userId = ?0 " +
        "WHERE post.publishStatus = 'PUBLISHED' " +
        "AND (post.createdBy.userId = ?0 OR topicFollow.topicFollowId IS NOT NULL OR userFollow.userFollowId IS NOT NULL) " +
        "AND postBlock.postBlockId IS NULL " +
        "AND userBlock.userBlockId IS NULL " +
        "ORDER BY relevanceScore DESC ";

    return selectResults(query, sql, currentUserId).thenApply(results -> results.stream()
                                                                 .map(result -> {
                                                                   Post post = (Post) result[0];
                                                                   post.setReacted(result[1] != null);
                                                                   post.setBookmarked(result[2] != null);
                                                                   return post;
                                                                 })
                                                                 .collect(Collectors.toList()));
  }

  @Override
  public CompletableFuture<List<AbstractEntity>> queryFollowing(RequestContext context, Query query) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT DISTINCT post, postReaction.postReactionId, postBookmark.postBookmarkId, " +
        "((UNIX_TIMESTAMP(post.updatedAt) - 1577836800) / 100000) + " +
        "(post.likeCount * 0.25) + (post.commentCount * 0.25) + " +
        "(postCreatedBy.followerCount * 0.1) AS relevanceScore " +
        "FROM Post post " +
        "LEFT JOIN FETCH post.coverImage " +
        "LEFT JOIN FETCH post.topic " +
        "LEFT JOIN FETCH post.prompt " +
        "LEFT JOIN FETCH post.createdBy postCreatedBy " +
        "LEFT JOIN FETCH postCreatedBy.profileImage " +
        "LEFT JOIN PostReaction postReaction ON postReaction.post.postId = post.postId AND postReaction.createdBy.userId = ?0 " +
        "LEFT JOIN PostBookmark postBookmark ON postBookmark.post.postId = post.postId AND postBookmark.createdBy.userId = ?0 " +
        "LEFT JOIN TopicFollow topicFollow ON topicFollow.topic.topicId = post.topic.topicId AND topicFollow.createdBy.userId = ?0 " +
        "LEFT JOIN UserFollow userFollow ON userFollow.user.userId = post.createdBy.userId AND userFollow.createdBy.userId = ?0 " +
        "LEFT JOIN PostBlock postBlock ON postBlock.post.postId = post.postId AND postBlock.createdBy.userId = ?0 " +
        "LEFT JOIN UserBlock userBlock ON userBlock.user.userId = post.createdBy.userId AND userBlock.createdBy.userId = ?0 " +
        "WHERE post.publishStatus = 'PUBLISHED' " +
        "AND (post.createdBy.userId = ?0 OR topicFollow.topicFollowId IS NOT NULL OR userFollow.userFollowId IS NOT NULL) " +
        "AND postBlock.postBlockId IS NULL " +
        "AND userBlock.userBlockId IS NULL " +
        "ORDER BY relevanceScore DESC ";

    return selectResults(query, sql, currentUserId).thenApply(results -> results.stream()
                                                                 .map(result -> {
                                                                   Post post = (Post) result[0];
                                                                   post.setReacted(result[1] != null);
                                                                   post.setBookmarked(result[2] != null);
                                                                   return post;
                                                                 })
                                                                 .collect(Collectors.toList()));
  }

  @Override
  public CompletableFuture<List<AbstractEntity>> queryPopular(RequestContext context, Query query) {
    String currentUserId = context.getCurrentUserId();

    String sql =
      "SELECT DISTINCT post, postReaction.postReactionId, postBookmark.postBookmarkId, " +
        "((UNIX_TIMESTAMP(post.updatedAt) - 1577836800) / 100000) + " +
        "(post.likeCount * 0.25) + (post.commentCount * 0.25) + " +
        "(postCreatedBy.followerCount * 0.1) AS relevanceScore " +
        "FROM Post post " +
        "LEFT JOIN FETCH post.coverImage " +
        "LEFT JOIN FETCH post.topic " +
        "LEFT JOIN FETCH post.prompt " +
        "LEFT JOIN FETCH post.createdBy postCreatedBy " +
        "LEFT JOIN FETCH postCreatedBy.profileImage " +
        "LEFT JOIN PostReaction postReaction ON postReaction.post.postId = post.postId AND postReaction.createdBy.userId = ?0 " +
        "LEFT JOIN PostBookmark postBookmark ON postBookmark.post.postId = post.postId AND postBookmark.createdBy.userId = ?0 " +
        "LEFT JOIN PostBlock postBlock ON postBlock.post.postId = post.postId AND postBlock.createdBy.userId = ?0 " +
        "LEFT JOIN UserBlock userBlock ON userBlock.user.userId = post.createdBy.userId AND userBlock.createdBy.userId = ?0 " +
        "WHERE post.publishStatus = 'PUBLISHED' " +
        "AND postBlock.postBlockId IS NULL " +
        "AND userBlock.userBlockId IS NULL " +
        "ORDER BY relevanceScore DESC ";

    return selectResults(query, sql, currentUserId).thenApply(results -> results.stream()
                                                                 .map(result -> {
                                                                   Post post = (Post) result[0];
                                                                   post.setReacted(result[1] != null);
                                                                   post.setBookmarked(result[2] != null);
                                                                   return post;
                                                                 })
                                                                 .collect(Collectors.toList()));
  }

}
