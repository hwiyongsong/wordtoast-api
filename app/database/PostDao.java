package database;

import com.google.inject.ImplementedBy;
import core.Query;
import core.RequestContext;
import entities.Post;
import entities.PostBlock;
import entities.PostBookmark;
import entities.PostComment;
import entities.PostReaction;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@ImplementedBy(PostDaoImpl.class)
public interface PostDao {

  /* POSTS */

  CompletableFuture<List<Post>> queryPosts(RequestContext context, Query query);

  CompletableFuture<Post> getPostById(RequestContext context, String postId);

  CompletableFuture<Post> savePost(RequestContext context, Post post);

  CompletableFuture<Void> removePost(RequestContext context, Post post);

  /* POST REACTIONS */

  CompletableFuture<List<PostReaction>> queryPostReactions(RequestContext context, String postId, Query query);

  CompletableFuture<PostReaction> getPostReaction(RequestContext context, String postId);

  CompletableFuture<PostReaction> savePostReaction(RequestContext context, PostReaction postReaction);

  CompletableFuture<Void> removePostReaction(RequestContext context, PostReaction postReaction);

  /* POST COMMENTS */

  CompletableFuture<List<PostComment>> queryPostComments(RequestContext context, String postId, Query query);

  CompletableFuture<PostComment> getPostCommentById(RequestContext context, String postCommentId);

  CompletableFuture<PostComment> savePostComment(RequestContext context, PostComment postComment);

  CompletableFuture<Void> removePostComment(RequestContext context, PostComment postComment);

  /* POST BOOKMARKS */

  CompletableFuture<List<PostBookmark>> queryPostBookmarks(RequestContext context, Query query);

  CompletableFuture<PostBookmark> getPostBookmark(RequestContext context, String postId, String userId);

  CompletableFuture<PostBookmark> savePostBookmark(RequestContext context, PostBookmark postBookmark);

  CompletableFuture<Void> removePostBookmark(RequestContext context, PostBookmark postBookmark);

  /* POST BLOCKS */

  CompletableFuture<PostBlock> getPostBlock(RequestContext context, String postId);

  CompletableFuture<PostBlock> savePostBlock(RequestContext context, PostBlock postBlock);

  CompletableFuture<Void> removePostBlock(RequestContext context, PostBlock postBlock);

}
