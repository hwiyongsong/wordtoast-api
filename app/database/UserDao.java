package database;

import com.google.inject.ImplementedBy;
import core.Query;
import core.RequestContext;
import entities.FriendRequest;
import entities.User;
import entities.UserBlock;
import entities.UserFollow;
import entities.UserFriend;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@ImplementedBy(UserDaoImpl.class)
public interface UserDao {

  /**** USERS ****/

  CompletableFuture<List<User>> queryUsers(RequestContext context, Query query);

  CompletableFuture<User> getUserById(RequestContext context, String userId);

  CompletableFuture<User> getUserByFirebaseUserId(String firebaseUserId);

  CompletableFuture<User> saveUser(RequestContext context, User user);

  /**** USER FOLLOWS ****/

  CompletableFuture<List<User>> queryUserFollowers(RequestContext context, Query query);

  CompletableFuture<List<User>> queryUserFollowings(RequestContext context, Query query);

  CompletableFuture<UserFollow> getUserFollow(RequestContext context, String userId);

  CompletableFuture<UserFollow> saveUserFollow(RequestContext context, UserFollow userFollow);

  CompletableFuture<Void> removeUserFollow(RequestContext context, UserFollow userFollow);

  /**** USER FRIENDS ****/

  CompletableFuture<List<User>> queryUserFriends(RequestContext context, String userId, Query query);

  CompletableFuture<UserFriend> saveUserFriend(RequestContext context, UserFriend userFriend);

  /**** FRIEND REQUESTS ****/

  CompletableFuture<FriendRequest> getFriendRequestById(RequestContext context, String friendRequestId);

  CompletableFuture<FriendRequest> saveFriendRequest(RequestContext context, FriendRequest friendRequest);

  CompletableFuture<Void> removeFriendRequest(RequestContext context, FriendRequest friendRequest);

  /* USER BLOCKS */

  CompletableFuture<UserBlock> getUserBlock(RequestContext context, String userId);

  CompletableFuture<UserBlock> saveUserBlock(RequestContext context, UserBlock userBlock);

  CompletableFuture<Void> removeUserBlock(RequestContext context, UserBlock userBlock);

}
