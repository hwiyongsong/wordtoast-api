package database;

import core.RequestContext;
import entities.Image;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;

@Singleton
public class ImageDaoImpl extends AbstractJpaDao implements ImageDao {

  @Inject
  public ImageDaoImpl(DatabaseExecutionContext executionContext, JPAApi jpaApi) {
    super(executionContext, jpaApi);
  }

  @Override
  public CompletableFuture<Image> getImageById(RequestContext context, String imageId) {
    String sql =
      "SELECT image " +
        "FROM Image image " +
        "LEFT JOIN FETCH image.createdBy imageCreatedBy " +
        "LEFT JOIN FETCH imageCreatedBy.profileImage " +
        "WHERE image.imageId = ?0";

    return findEntity(sql, imageId);
  }

  @Override
  public CompletableFuture<Image> saveImage(RequestContext context, Image image) {
    return saveEntity(image);
  }

}
