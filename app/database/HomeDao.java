package database;

import com.google.inject.ImplementedBy;
import core.Query;
import core.RequestContext;
import entities.AbstractEntity;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@ImplementedBy(HomeDaoImpl.class)
public interface HomeDao {

  CompletableFuture<List<AbstractEntity>> queryForYou(RequestContext context, Query query);

  CompletableFuture<List<AbstractEntity>> queryFollowing(RequestContext context, Query query);

  CompletableFuture<List<AbstractEntity>> queryPopular(RequestContext context, Query query);

}
