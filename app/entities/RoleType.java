package entities;

public enum RoleType {

  ADMIN,
  GUEST,
  MEMBER,
  SYSTEM

}
