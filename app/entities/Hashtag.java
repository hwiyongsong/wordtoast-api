package entities;

import core.RequestContext;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.OffsetDateTime;

@Entity
@Table(name = "Hashtag")
public class Hashtag extends AbstractEntity {

  @Id
  @GenericGenerator(name = "UuidGenerator", strategy = "core.UuidGenerator")
  @GeneratedValue(generator = "UuidGenerator")
  private String hashtagId;

  @Basic
  private String name;

  @Basic
  private Integer postCount = 0;

  @Basic
  private Integer followerCount = 0;

  @ManyToOne
  @JoinColumn(name = "createdBy")
  private User createdBy;

  @Basic
  @CreationTimestamp
  private OffsetDateTime createdAt;

  @Transient
  private boolean isFollowing;

  public String getHashtagId() {
    return hashtagId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getPostCount() {
    return postCount;
  }

  public Integer getFollowerCount() {
    return followerCount;
  }

  public User getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(User createdBy) {
    this.createdBy = createdBy;
  }

  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  public void setFollowing(boolean following) {
    isFollowing = following;
  }

  public Boolean isFollowing() {
    return false;
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("name", getName())
                   .put("postCount", getPostCount())
                   .put("followerCount", getFollowerCount())
                   .put("following", isFollowing())
                   .build();
  }

  @Override
  public String getIdentifierId() {
    return hashtagId;
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.HASHTAG;
  }

}
