package entities;

import core.RequestContext;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Topic")
public class Topic extends AbstractEntity {

  @Id
  @GenericGenerator(name = "UuidGenerator", strategy = "core.UuidGenerator")
  @GeneratedValue(generator = "UuidGenerator")
  private String topicId;

  @Basic
  private String title;

  @Basic
  private String description;

  @OneToOne
  @JoinColumn(name = "posterImageId")
  private Image posterImage;

  @Basic
  private Integer followerCount;

  @Transient
  private Boolean following;

  public String getTopicId() {
    return topicId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Image getPosterImage() {
    return posterImage;
  }

  public void setPosterImage(Image posterImage) {
    this.posterImage = posterImage;
  }

  public Integer getFollowerCount() {
    return followerCount;
  }

  public Boolean isFollowing() {
    return following;
  }

  public void setFollowing(Boolean following) {
    this.following = following;
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("title", getTitle())
                   .put("description", getDescription())
                   .put("posterImage", getPosterImage() != null ? getPosterImage().toCompactPayload(context) : null)
                   .put("followerCount", getFollowerCount())
                   .put("following", isFollowing())
                   .build();
  }

  @Override
  public Object toCompactPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("title", getTitle())
                   .put("posterImage", getPosterImage() != null ? getPosterImage().toCompactPayload(context) : null)
                   .build();
  }

  @Override
  public String getIdentifierId() {
    return topicId;
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.TOPIC;
  }

}
