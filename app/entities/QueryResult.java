package entities;

import com.google.common.collect.Lists;
import core.Query;
import core.RequestContext;
import core.RequestParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.ObjectUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class QueryResult extends AbstractEntity {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private Query query;
  private List<? extends AbstractEntity> entities = Lists.newArrayList();

  public QueryResult(Query query, List<? extends AbstractEntity> entities) {
    this.query = query;
    this.entities = entities;
  }

  @SuppressWarnings("unchecked")
  public <T extends AbstractEntity> List<T> getEntities() {
    return (List<T>) entities;
  }

  public int size() {
    return entities.size();
  }

  private Map<String, Object> getMetadata() {
    return mapper().put("limit", query.getInteger(RequestParameter.LIMIT))
                   .put("offset", query.getInteger(RequestParameter.OFFSET))
                   .build();
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("meta", getMetadata())
                   .put("entities", getEntities().stream()
                                                 .map(entity -> {
                                                   try {
                                                     return entity.toDataPayload(context);
                                                   } catch (Exception e) {
                                                     logger.error("Unable to get data map for entity: " + entity.getIdentifierUrn(), e);
                                                     return null;
                                                   }
                                                 })
                                                 .filter(map -> map != null)
                                                 .collect(Collectors.toList()))
                   .build();
  }

  @Override
  public Object toSummaryPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("meta", getMetadata())
                   .put("entities", getEntities().stream()
                                                 .map(entity -> {
                                                   try {
                                                     return entity.toSummaryPayload(context);
                                                   } catch (Exception e) {
                                                     logger.error("Unable to get summary map for entity: " + entity.getIdentifierUrn(), e);
                                                     return null;
                                                   }
                                                 })
                                                 .filter(map -> map != null)
                                                 .collect(Collectors.toList()))
                   .build();
  }

  @Override
  public Object toCompactPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("meta", getMetadata())
                   .put("entities", getEntities().stream()
                                                 .map(entity -> {
                                                   try {
                                                     return entity.toCompactPayload(context);
                                                   } catch (Exception e) {
                                                     logger.error("Unable to get compact map for entity: " + entity.getIdentifierUrn(), e);
                                                     return null;
                                                   }
                                                 })
                                                 .filter(map -> map != null)
                                                 .collect(Collectors.toList()))
                   .build();
  }

  @Override
  public String getIdentifierId() {
    return ObjectUtils.generateUuid();
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.QUERY_RESULT;
  }

}
