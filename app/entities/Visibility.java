package entities;

public enum Visibility {

  PRIVATE,
  FRIENDS,
  PUBLIC;

}
