package entities;

import core.RequestContext;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import utils.StringUtils;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.OffsetDateTime;

@Entity
@Table(name = "User")
public class User extends AbstractEntity {

  @Id
  @GenericGenerator(name = "UuidGenerator", strategy = "core.UuidGenerator")
  @GeneratedValue(generator = "UuidGenerator")
  private String userId;

  @Basic
  private String displayName;

  @Basic
  private String headline;

  @Basic
  private String email;

  @Basic
  private Boolean emailVerified = false;

  @Basic
  private String phoneNumber;

  @Basic
  private String phoneNumberVerificationCode;

  @Basic
  private Boolean phoneNumberVerified = false;

  @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "profileImageId")
  private Image profileImage;

  @Basic
  private String handle;

  @Basic
  private String firebaseUserId;

  @Basic
  private String facebookUserId;

  @Enumerated(EnumType.STRING)
  private RoleType roleType;

  @Enumerated(EnumType.STRING)
  private PlanType planType = PlanType.FREE;

  @Basic
  private Integer reactionPoints = 1;

  @Basic
  private Integer followerCount = 0;

  @Basic
  private Integer followingCount = 0;

  @Basic
  private Integer friendCount = 0;

  @Basic
  private String referralCode;

  @Basic
  private String referredCode;

  @Basic
  @CreationTimestamp
  private OffsetDateTime createdAt;

  @Transient
  private boolean isFollowing;

  @Transient
  private boolean isFriended;

  @Transient
  private boolean isFriending;

  public String getUserId() {
    return userId;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getHeadline() {
    return headline;
  }

  public void setHeadline(String headline) {
    this.headline = headline;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Boolean isEmailVerified() {
    return emailVerified;
  }

  public void setEmailVerified(Boolean emailVerified) {
    this.emailVerified = emailVerified;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getPhoneNumberVerificationCode() {
    return phoneNumberVerificationCode;
  }

  public void setPhoneNumberVerificationCode(String phoneNumberVerificationCode) {
    this.phoneNumberVerificationCode = phoneNumberVerificationCode;
  }

  public Boolean isPhoneNumberVerified() {
    return phoneNumberVerified;
  }

  public void setPhoneNumberVerified(Boolean phoneNumberVerified) {
    this.phoneNumberVerified = phoneNumberVerified;
  }

  public Image getProfileImage() {
    return profileImage;
  }

  public void setProfileImage(Image profileImage) {
    this.profileImage = profileImage;
  }

  public String getHandle() {
    return handle;
  }

  public void setHandle(String handle) {
    this.handle = handle;
  }

  public String getFirebaseUserId() {
    return firebaseUserId;
  }

  public void setFirebaseUserId(String firebaseUserId) {
    this.firebaseUserId = firebaseUserId;
  }

  public String getFacebookUserId() {
    return facebookUserId;
  }

  public void setFacebookUserId(String facebookUserId) {
    this.facebookUserId = facebookUserId;
  }

  public RoleType getRoleType() {
    return roleType;
  }

  public void setRoleType(RoleType roleType) {
    this.roleType = roleType;
  }

  public PlanType getPlanType() {
    return planType;
  }

  public void setPlanType(PlanType planType) {
    this.planType = planType;
  }

  public Integer getTotalPoints() {
    return reactionPoints;
  }

  public Integer getReactionPoints() {
    return reactionPoints;
  }

  public Integer getFollowerCount() {
    return followerCount;
  }

  public Integer getFollowingCount() {
    return followingCount;
  }

  public Integer getFriendCount() {
    return friendCount;
  }

  public String getReferralCode() {
    return referralCode;
  }

  public void setReferralCode(String referralCode) {
    this.referralCode = referralCode;
  }

  public String getReferredCode() {
    return referredCode;
  }

  public void setReferredCode(String referredCode) {
    this.referredCode = referredCode;
  }

  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  public boolean isFollowing() {
    return isFollowing;
  }

  public void setFollowing(boolean following) {
    isFollowing = following;
  }

  public boolean isFriended() {
    return isFriended;
  }

  public void setFriended(boolean friended) {
    isFriended = friended;
  }

  public boolean isFriending() {
    return isFriending;
  }

  public void setFriending(boolean friending) {
    isFriending = friending;
  }

  public boolean isGuest() {
    return roleType == RoleType.GUEST;
  }

  public boolean isMember() {
    return roleType == RoleType.MEMBER;
  }

  public boolean isAdmin() {
    return roleType == RoleType.ADMIN;
  }

  public Boolean isCurrentUser(RequestContext context) {
    if (context.isAuthenticated()) {
      return StringUtils.equals(context.getCurrentUserId(), getUserId());
    }

    return false;
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("displayName", getDisplayName())
                   .put("headline", getHeadline())
                   .put("email", getEmail())
                   .put("emailVerified", isEmailVerified())
                   .put("phoneNumber", getPhoneNumber())
                   .put("phoneNumberVerified", isPhoneNumberVerified())
                   .put("profileImage", getProfileImage() != null ? getProfileImage().toCompactPayload(context) : null)
                   .put("handle", getHandle())
                   .put("roleType", getRoleType())
                   .put("planType", getPlanType())
                   .put("totalPoints", getTotalPoints())
                   .put("followerCount", getFollowerCount())
                   .put("followingCount", getFollowingCount())
                   .put("friendCount", getFriendCount())
                   .put("following", isFollowing())
                   .put("friended", isFriended())
                   .put("friending", isFriending())
                   .put("editable", isCurrentUser(context))
                   .put("referralCode", getReferralCode())
                   .build();
  }

  @Override
  public Object toSummaryPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("displayName", getDisplayName())
                   .put("headline", getHeadline())
                   .put("profileImage", getProfileImage() != null ? getProfileImage().toCompactPayload(context) : null)
                   .put("handle", getHandle())
                   .put("following", isFollowing())
                   .put("friended", isFriended())
                   .put("editable", isCurrentUser(context))
                   .build();
  }

  @Override
  public Object toCompactPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("displayName", getDisplayName())
                   .put("headline", getHeadline())
                   .put("profileImage", getProfileImage() != null ? getProfileImage().toCompactPayload(context) : null)
                   .put("handle", getHandle())
                   .build();
  }

  @Override
  public String getIdentifierId() {
    return userId;
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.USER;
  }

}
