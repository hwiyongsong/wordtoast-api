package entities;

public enum PublishStatus {

  DRAFT,
  PUBLISHED,
  ARCHIVED

}
