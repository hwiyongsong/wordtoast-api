package entities;

import com.google.common.collect.Lists;
import core.RequestContext;
import org.apache.commons.collections4.ListUtils;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import utils.DateUtils;
import utils.StringUtils;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "Post")
public class Post extends AbstractEntity {

  @Id
  @GenericGenerator(name = "UuidGenerator", strategy = "core.UuidGenerator")
  @GeneratedValue(generator = "UuidGenerator")
  private String postId;

  @Enumerated(EnumType.STRING)
  private PostType postType;

  @Basic
  private String title;

  @Basic
  private String body;

  @OneToOne
  @JoinColumn(name = "coverImageId")
  private Image coverImage;

  @ManyToOne
  @JoinColumn(name = "topicId")
  private Topic topic;

  @ManyToOne
  @JoinColumn(name = "promptId")
  private Post prompt;

  @Basic
  private String hashtagText;

  @Basic
  private String slug;

  @Enumerated(EnumType.STRING)
  private Visibility visibility = Visibility.FRIENDS;

  @Enumerated(EnumType.STRING)
  private PublishStatus publishStatus = PublishStatus.PUBLISHED;

  @Basic
  private Integer likeCount = 0;

  @Basic
  private Integer loveCount = 0;

  @Basic
  private Integer hahaCount = 0;

  @Basic
  private Integer sadCount = 0;

  @Basic
  private Integer commentCount = 0;

  @Basic
  private Integer bookmarkCount = 0;

  @ManyToOne
  @JoinColumn(name = "createdBy")
  private User createdBy;

  @Basic
  @CreationTimestamp
  private OffsetDateTime createdAt;

  @Basic
  @UpdateTimestamp
  private OffsetDateTime updatedAt;

  @ManyToMany
  @JoinTable(name = "PostHashtag", joinColumns = { @JoinColumn(name = "postId") }, inverseJoinColumns = { @JoinColumn(name = "hashtagId") })
  private List<Hashtag> hashtags = Lists.newArrayList();

  @Transient
  private Boolean reacted = false;

  @Transient
  private Boolean bookmarked = false;

  public String getPostId() {
    return postId;
  }

  public PostType getPostType() {
    return postType;
  }

  public void setPostType(PostType postType) {
    this.postType = postType;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubtitle() {
    String sanitizedBody = body.replaceAll("&nbsp;", " ").replaceAll("<br>", " ").replaceAll("\\<.*?\\>", "");
    return StringUtils.abbreviate(sanitizedBody, 100);
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public Image getCoverImage() {
    return coverImage;
  }

  public void setCoverImage(Image coverImage) {
    this.coverImage = coverImage;
  }

  public Topic getTopic() {
    return topic;
  }

  public void setTopic(Topic topic) {
    this.topic = topic;
  }

  public Post getPrompt() {
    return prompt;
  }

  public void setPrompt(Post prompt) {
    this.prompt = prompt;
  }

  public String getHashtagText() {
    return hashtagText;
  }

  public void setHashtagText(String hashtagText) {
    this.hashtagText = hashtagText;
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public Visibility getVisibility() {
    return visibility;
  }

  public void setVisibility(Visibility visibility) {
    this.visibility = visibility;
  }

  public PublishStatus getPublishStatus() {
    return publishStatus;
  }

  public void setPublishStatus(PublishStatus publishStatus) {
    this.publishStatus = publishStatus;
  }

  public Integer getLikeCount() {
    return likeCount;
  }

  public Integer getLoveCount() {
    return loveCount;
  }

  public Integer getHahaCount() {
    return hahaCount;
  }

  public Integer getSadCount() {
    return sadCount;
  }

  public Integer getReactionCount() {
    return likeCount + loveCount + hahaCount + sadCount;
  }

  public Integer getCommentCount() {
    return commentCount;
  }

  public Integer getBookmarkCount() {
    return bookmarkCount;
  }

  public User getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(User createdBy) {
    this.createdBy = createdBy;
  }

  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  public OffsetDateTime getUpdatedAt() {
    return updatedAt;
  }

  public List<Hashtag> getHashtags() {
    return hashtags;
  }

  public void setHashtags(List<Hashtag> hashtags) {
    List<Hashtag> added = ListUtils.subtract(hashtags, this.hashtags);
    List<Hashtag> removed = ListUtils.subtract(this.hashtags, hashtags);
    this.hashtags.addAll(added);
    this.hashtags.removeAll(removed);
  }

  public Boolean isReacted() {
    return reacted;
  }

  public void setReacted(Boolean reacted) {
    this.reacted = reacted;
  }

  public Boolean isBookmarked() {
    return bookmarked;
  }

  public void setBookmarked(Boolean bookmarked) {
    this.bookmarked = bookmarked;
  }

  private boolean isEditable(RequestContext context) {
    if (context.isAuthenticated()) {
      User currentUser = context.getCurrentUser();
      return getCreatedBy().equals(currentUser);
    }

    return false;
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("postType", getPostType())
                   .put("title", getTitle())
                   .put("subtitle", getSubtitle())
                   .put("body", getBody())
                   .put("coverImage", getCoverImage() != null ? getCoverImage().toCompactPayload(context) : null)
                   .put("topic", getTopic() != null ? getTopic().toCompactPayload(context) : null)
                   .put("prompt", getPrompt() != null ? getPrompt().toSummaryPayload(context) : null)
                   .put("hashtagText", getHashtagText())
                   .put("slug", getSlug())
                   .put("visibility", getVisibility())
                   .put("publishStatus", getPublishStatus())
                   .put("reactionCount", getReactionCount())
                   .put("commentCount", getCommentCount())
                   .put("bookmarkCount", getBookmarkCount())
                   .put("reacted", isReacted())
                   .put("bookmarked", isBookmarked())
                   .put("createdBy", getCreatedBy().toSummaryPayload(context))
                   .put("createdAt", getCreatedAt() != null ? DateUtils.formatIso(getCreatedAt()) : null)
                   .put("updatedAt", getUpdatedAt() != null ? DateUtils.formatIso(getUpdatedAt()) : null)
                   .put("hashtags", getHashtags().stream()
                                                 .map(hashtag -> hashtag.toCompactPayload(context))
                                                 .collect(Collectors.toList()))
                   .build();
  }

  @Override
  public Object toSummaryPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("postType", getPostType())
                   .put("title", getTitle())
                   .put("subtitle", getSubtitle())
                   .put("body", getBody())
                   .put("coverImage", getCoverImage() != null ? getCoverImage().toCompactPayload(context) : null)
                   .put("topic", getTopic() != null ? getTopic().toCompactPayload(context) : null)
                   .put("prompt", getPrompt() != null ? getPrompt().toCompactPayload(context) : null)
                   .put("publishStatus", getPublishStatus())
                   .put("reactionCount", getReactionCount())
                   .put("commentCount", getCommentCount())
                   .put("bookmarkCount", getBookmarkCount())
                   .put("reacted", isReacted())
                   .put("createdBy", getCreatedBy().toSummaryPayload(context))
                   .put("createdAt", getCreatedAt() != null ? DateUtils.formatIso(getCreatedAt()) : null)
                   .put("updatedAt", getUpdatedAt() != null ? DateUtils.formatIso(getUpdatedAt()) : null)
                   .build();
  }

  @Override
  public Object toCompactPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("postType", getPostType())
                   .put("title", getTitle())
                   .put("subtitle", getSubtitle())
                   .put("coverImage", getCoverImage() != null ? getCoverImage().toCompactPayload(context) : null)
                   .put("createdBy", getCreatedBy().toSummaryPayload(context))
                   .put("createdAt", getCreatedAt() != null ? DateUtils.formatIso(getCreatedAt()) : null)
                   .put("updatedAt", getUpdatedAt() != null ? DateUtils.formatIso(getUpdatedAt()) : null)
                   .build();
  }

  @Override
  public String getIdentifierId() {
    return postId;
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.POST;
  }

}
