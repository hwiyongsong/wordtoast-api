package entities;

import core.RequestContext;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import utils.DateUtils;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Entity
@Table(name = "PostReaction")
public class PostReaction extends AbstractEntity {

  @Id
  @GenericGenerator(name = "UuidGenerator", strategy = "core.UuidGenerator")
  @GeneratedValue(generator = "UuidGenerator")
  private String postReactionId;

  @ManyToOne
  @JoinColumn(name = "postId")
  private Post post;

  @Enumerated(EnumType.STRING)
  private ReactionType reactionType;

  @ManyToOne
  @JoinColumn(name = "createdBy")
  private User createdBy;

  @Basic
  @CreationTimestamp
  private OffsetDateTime createdAt;

  public String getPostReactionId() {
    return postReactionId;
  }

  public Post getPost() {
    return post;
  }

  public void setPost(Post post) {
    this.post = post;
  }

  public ReactionType getReactionType() {
    return reactionType;
  }

  public void setReactionType(ReactionType reactionType) {
    this.reactionType = reactionType;
  }

  public User getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(User createdBy) {
    this.createdBy = createdBy;
  }

  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("post", getPost().toCompactPayload(context))
                   .put("reactionType", getReactionType())
                   .put("createdBy", getCreatedBy().toCompactPayload(context))
                   .put("createdAt", getCreatedAt() != null ? DateUtils.formatIso(getCreatedAt()) : null)
                   .build();
  }

  @Override
  public String getIdentifierId() {
    return postReactionId;
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.POST_REACTION;
  }

}
