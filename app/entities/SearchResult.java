package entities;

import com.google.common.collect.Lists;
import core.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.ObjectUtils;

import java.util.List;
import java.util.stream.Collectors;

public class SearchResult extends AbstractEntity {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private String input;
  private List<User> users = Lists.newArrayList();
  private List<Post> posts = Lists.newArrayList();

  public SearchResult(String input) {
    this.input = input;
  }

  public String getInput() {
    return input;
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

  public List<Post> getPosts() {
    return posts;
  }

  public void setPosts(List<Post> posts) {
    this.posts = posts;
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("input", getInput())
                   .put("users", getUsers().stream()
                                           .map(user -> {
                                             try {
                                               return user.toCompactPayload(context);
                                             } catch (Exception e) {
                                               logger.error("Unable to get compact map for user: " + user.getIdentifierUrn(), e);
                                               return null;
                                             }
                                           })
                                           .filter(map -> map != null)
                                           .collect(Collectors.toList()))
                   .put("posts", getPosts().stream()
                                           .map(post -> {
                                             try {
                                               return post.toCompactPayload(context);
                                             } catch (Exception e) {
                                               logger.error("Unable to get compact map for post: " + post.getIdentifierUrn(), e);
                                               return null;
                                             }
                                           })
                                           .filter(map -> map != null)
                                           .collect(Collectors.toList()))
                   .build();
  }

  @Override
  public String getIdentifierId() {
    return ObjectUtils.generateUuid();
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.SEARCH_RESULT;
  }

}
