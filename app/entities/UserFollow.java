package entities;

import core.RequestContext;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import utils.DateUtils;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Entity
@Table(name = "UserFollow")
public class UserFollow extends AbstractEntity {

  @Id
  @GenericGenerator(name = "UuidGenerator", strategy = "core.UuidGenerator")
  @GeneratedValue(generator = "UuidGenerator")
  private String userFollowId;

  @ManyToOne
  @JoinColumn(name = "userId")
  private User user;

  @ManyToOne
  @JoinColumn(name = "createdBy")
  private User createdBy;

  @Basic
  @CreationTimestamp
  private OffsetDateTime createdAt;

  public String getUserFollowId() {
    return userFollowId;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public User getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(User createdBy) {
    this.createdBy = createdBy;
  }

  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("user", getUser().toSummaryPayload(context))
                   .put("createdBy", getCreatedBy().toSummaryPayload(context))
                   .put("createdAt", getCreatedAt() != null ? DateUtils.formatIso(getCreatedAt()) : null)
                   .build();
  }

  @Override
  public String getIdentifierId() {
    return userFollowId;
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.USER_FOLLOW;
  }

}
