package entities;

import core.RequestContext;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Entity
@Table(name = "Image")
public class Image extends AbstractEntity {

  @Id
  @GenericGenerator(name = "UuidGenerator", strategy = "core.UuidGenerator")
  @GeneratedValue(generator = "UuidGenerator")
  private String imageId;

  @Basic
  private String sourceUrl;

  @Basic
  private String bucket;

  @Basic
  private String filename;

  @ManyToOne
  @JoinColumn(name = "createdBy")
  private User createdBy;

  @Basic
  @CreationTimestamp
  private OffsetDateTime createdAt;

  public String getImageId() {
    return imageId;
  }

  public String getSourceUrl() {
    return sourceUrl;
  }

  public void setSourceUrl(String sourceUrl) {
    this.sourceUrl = sourceUrl;
  }

  public String getBucket() {
    return bucket;
  }

  public void setBucket(String bucket) {
    this.bucket = bucket;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public User getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(User createdBy) {
    this.createdBy = createdBy;
  }

  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(OffsetDateTime createdAt) {
    this.createdAt = createdAt;
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("sourceUrl", getSourceUrl())
                   .put("bucket", getBucket())
                   .put("filename", getFilename())
                   .build();
  }

  @Override
  public Object toCompactPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("sourceUrl", getSourceUrl())
                   .build();
  }

  @Override
  public String getIdentifierId() {
    return imageId;
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.IMAGE;
  }

}
