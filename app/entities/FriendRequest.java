package entities;

import core.RequestContext;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import utils.DateUtils;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Entity
@Table(name = "FriendRequest")
public class FriendRequest extends AbstractEntity {

  @Id
  @GenericGenerator(name = "UuidGenerator", strategy = "core.UuidGenerator")
  @GeneratedValue(generator = "UuidGenerator")
  private String friendRequestId;

  @ManyToOne
  @JoinColumn(name = "userId")
  private User user;

  @Basic
  private String notes;

  @Enumerated(EnumType.STRING)
  private FriendRequestStatus status = FriendRequestStatus.PENDING;

  @ManyToOne
  @JoinColumn(name = "createdBy")
  private User createdBy;

  @Basic
  @CreationTimestamp
  private OffsetDateTime createdAt;

  @Basic
  private OffsetDateTime respondedAt;

  public String getFriendRequestId() {
    return friendRequestId;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public FriendRequestStatus getStatus() {
    return status;
  }

  public void setStatus(FriendRequestStatus status) {
    this.status = status;
  }

  public User getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(User createdBy) {
    this.createdBy = createdBy;
  }

  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  public OffsetDateTime getRespondedAt() {
    return respondedAt;
  }

  public void setRespondedAt(OffsetDateTime respondedAt) {
    this.respondedAt = respondedAt;
  }

  public boolean isPending() {
    return status == FriendRequestStatus.PENDING;
  }

  public boolean isAccepted() {
    return status == FriendRequestStatus.ACCEPTED;
  }

  public boolean isDeclined() {
    return status == FriendRequestStatus.DECLINED;
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("user", getUser().toCompactPayload(context))
                   .put("notes", getNotes())
                   .put("status", getStatus())
                   .put("createdBy", getCreatedBy().toCompactPayload(context))
                   .put("createdAt", getCreatedAt() != null ? DateUtils.formatIso(getCreatedAt()) : null)
                   .put("respondedAt", getRespondedAt() != null ? DateUtils.formatIso(getRespondedAt()) : null)
                   .build();
  }

  @Override
  public Object toCompactPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("user", getUser().toCompactPayload(context))
                   .put("status", getStatus())
                   .build();
  }

  @Override
  public String getIdentifierId() {
    return friendRequestId;
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.FRIEND_REQUEST;
  }

}
