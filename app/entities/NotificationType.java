package entities;

public enum NotificationType {

  FRIEND_REQUEST_CREATE,
  FRIEND_REQUEST_ACCEPT,
  POST_COMMENT,
  POST_CREATE,
  POST_REACT,
  USER_FOLLOW

}
