package entities;

import core.DataMapper;
import core.RequestContext;
import utils.ObjectUtils;
import utils.StringUtils;
import utils.UrnUtils;

import java.io.Serializable;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public abstract class AbstractEntity implements Serializable, Comparable<AbstractEntity> {

  public Object toDataPayload(RequestContext context) {
    return CompletableFuture.completedFuture(mapper());
  }

  public Object toSummaryPayload(RequestContext context) {
    return toDataPayload(context);
  }

  public Object toCompactPayload(RequestContext context) {
    return toSummaryPayload(context);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof AbstractEntity) {
      AbstractEntity that = (AbstractEntity) obj;
      return Objects.equals(this.getIdentifierId(), that.getIdentifierId());
    }

    return super.equals(obj);
  }

  @Override
  public int hashCode() {
    return (getIdentifierId() != null) ? getIdentifierId().hashCode() : super.hashCode();
  }

  @Override
  public int compareTo(AbstractEntity that) {
    return (getComparable() != null) ? ObjectUtils.compare(this.getComparable(), that.getComparable()) : 0;
  }

  public String toString() {
    return ObjectUtils.reflectionToString(this);
  }

  public String getIdentifierId() {
    return null;
  }

  public EntityType getIdentifierType() {
    return null;
  }

  public String getIdentifierUrn() {
    EntityType identityType = getIdentifierType();
    String identiferId = getIdentifierId();

    if (StringUtils.isBlank(identiferId)) {
      identiferId = "undefined";
    }

    return UrnUtils.createUrn(identityType, identiferId);
  }

  protected Object getComparable() {
    return getIdentifierId();
  }

  protected DataMapper mapper() {
    return new DataMapper();
  }

}
