package entities;

public enum FriendRequestStatus {

  ACCEPTED,
  DECLINED,
  PENDING

}
