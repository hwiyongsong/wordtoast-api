package entities;

public enum EntityType {

  ACCESS_TOKEN("access-token"),
  CHROME_HEADER("chrome-header"),
  FRIEND_REQUEST("friend-request"),
  IMAGE("image"),
  HASHTAG("hashtag"),
  HASHTAG_FOLLOW("hashtag-follow"),
  NAV_ITEM("nav-item"),
  NOTIFICATION("notification"),
  POST("post"),
  POST_BLOCK("post-block"),
  POST_BOOKMARK("post-bookmark"),
  POST_COMMENT("post-comment"),
  POST_REACTION("post-reaction"),
  QUERY_RESULT("query-result"),
  SEARCH_RESULT("search-result"),
  TOPIC("topic"),
  TOPIC_FOLLOW("topic-follow"),
  USER("user"),
  USER_BLOCK("user-block"),
  USER_FOLLOW("user-follow"),
  USER_FRIEND("user-friend");

  private String key;

  private EntityType(String key) {
    this.key = key;
  }

  public String getKey() {
    return key;
  }

  public static EntityType of(String key) {
    for (EntityType entityType : values()) {
      if (entityType.key.equals(key) || entityType.name().equals(key)) {
        return entityType;
      }
    }

    throw new IllegalArgumentException("Unknown EntityType: " + key);
  }

}
