package entities;

public enum ReactionType {

  HAHA,
  LIKE,
  LOVE,
  SAD,
  WOW

}
