package entities;

import core.RequestContext;
import utils.ObjectUtils;

public class NavItem extends AbstractEntity {

  private String title;
  private String url;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("title", getTitle())
                   .put("url", getUrl())
                   .build();
  }

  public String getIdentifierId() {
    return ObjectUtils.generateUuid();
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.NAV_ITEM;
  }

}
