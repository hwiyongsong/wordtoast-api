package entities;

import core.RequestContext;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import utils.DateUtils;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Entity
@Table(name = "Notification")
public class Notification extends AbstractEntity {

  @Id
  @GenericGenerator(name = "UuidGenerator", strategy = "core.UuidGenerator")
  @GeneratedValue(generator = "UuidGenerator")
  private String notificationId;

  @Enumerated(EnumType.STRING)
  private NotificationType notificationType;

  @ManyToOne
  @JoinColumn(name = "recipientId")
  private User recipient;

  @ManyToOne
  @JoinColumn(name = "actorId")
  private User actor;

  @ManyToOne
  @JoinColumn(name = "friendRequestId")
  private FriendRequest friendRequest;

  @ManyToOne
  @JoinColumn(name = "userFollowId")
  private UserFollow userFollow;

  @ManyToOne
  @JoinColumn(name = "postId")
  private Post post;

  @ManyToOne
  @JoinColumn(name = "postReactionId")
  private PostReaction postReaction;

  @ManyToOne
  @JoinColumn(name = "postCommentId")
  private PostComment postComment;

  @Basic
  private Boolean read = false;

  @Basic
  @CreationTimestamp
  private OffsetDateTime createdAt;

  public String getNotificationId() {
    return notificationId;
  }

  public NotificationType getNotificationType() {
    return notificationType;
  }

  public void setNotificationType(NotificationType notificationType) {
    this.notificationType = notificationType;
  }

  public User getRecipient() {
    return recipient;
  }

  public void setRecipient(User recipient) {
    this.recipient = recipient;
  }

  public User getActor() {
    return actor;
  }

  public void setActor(User actor) {
    this.actor = actor;
  }

  public FriendRequest getFriendRequest() {
    return friendRequest;
  }

  public void setFriendRequest(FriendRequest friendRequest) {
    this.friendRequest = friendRequest;
  }

  public UserFollow getUserFollow() {
    return userFollow;
  }

  public void setUserFollow(UserFollow userFollow) {
    this.userFollow = userFollow;
  }

  public Post getPost() {
    return post;
  }

  public void setPost(Post post) {
    this.post = post;
  }

  public PostReaction getPostReaction() {
    return postReaction;
  }

  public void setPostReaction(PostReaction postReaction) {
    this.postReaction = postReaction;
  }

  public PostComment getPostComment() {
    return postComment;
  }

  public void setPostComment(PostComment postComment) {
    this.postComment = postComment;
  }

  public Boolean isRead() {
    return read;
  }

  public void setRead(Boolean read) {
    this.read = read;
  }

  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("notificationType", getNotificationType())
                   .put("actor", getActor() != null ? getActor().toCompactPayload(context) : null)
                   .put("friendRequest", getFriendRequest() != null ? getFriendRequest().toCompactPayload(context) : null)
                   .put("userFollow", getUserFollow() != null ? getUserFollow().toCompactPayload(context) : null)
                   .put("post", getPost() != null ? getPost().toCompactPayload(context) : null)
                   .put("postReaction", getPostReaction() != null ? getPostReaction().toCompactPayload(context) : null)
                   .put("postComment", getPostComment() != null ? getPostComment().toCompactPayload(context) : null)
                   .put("read", isRead())
                   .put("createdAt", getCreatedAt() != null ? DateUtils.formatIso(getCreatedAt()) : null)
                   .build();
  }

  @Override
  public String getIdentifierId() {
    return notificationId;
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.NOTIFICATION;
  }

}
