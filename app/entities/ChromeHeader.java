package entities;

import com.google.common.collect.Lists;
import core.RequestContext;
import utils.ObjectUtils;

import java.util.List;
import java.util.stream.Collectors;

public class ChromeHeader extends AbstractEntity {

  private List<AbstractEntity> navItems = Lists.newArrayList();

  public List<AbstractEntity> getNavItems() {
    return navItems;
  }

  public void addNavItems(List<? extends AbstractEntity> items) {
    this.navItems.addAll(items);
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("navItems", getNavItems().stream()
                                                 .map(item -> item.toCompactPayload(context))
                                                 .collect(Collectors.toList()))
                   .build();
  }

  public String getIdentifierId() {
    return ObjectUtils.generateUuid();
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.CHROME_HEADER;
  }

}
