package entities;

import core.RequestContext;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import utils.DateUtils;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Entity
@Table(name = "HashtagFollow")
public class HashtagFollow extends AbstractEntity {

  @Id
  @GenericGenerator(name = "UuidGenerator", strategy = "core.UuidGenerator")
  @GeneratedValue(generator = "UuidGenerator")
  private String hashtagFollowId;

  @ManyToOne
  @JoinColumn(name = "hashtagId")
  private Hashtag hashtag;

  @ManyToOne
  @JoinColumn(name = "createdBy")
  private User createdBy;

  @Basic
  @CreationTimestamp
  private OffsetDateTime createdAt;

  public String getHashtagFollowId() {
    return hashtagFollowId;
  }

  public Hashtag getHashtag() {
    return hashtag;
  }

  public void setHashtag(Hashtag hashtag) {
    this.hashtag = hashtag;
  }

  public User getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(User createdBy) {
    this.createdBy = createdBy;
  }

  public OffsetDateTime getCreatedAt() {
    return createdAt;
  }

  @Override
  public Object toDataPayload(RequestContext context) {
    return mapper().put("urn", getIdentifierUrn())
                   .put("createdBy", getCreatedBy().toCompactPayload(context))
                   .put("createdAt", getCreatedAt() != null ? DateUtils.formatIso(getCreatedAt()) : null)
                   .build();
  }

  @Override
  public String getIdentifierId() {
    return hashtagFollowId;
  }

  @Override
  public EntityType getIdentifierType() {
    return EntityType.HASHTAG_FOLLOW;
  }

}
