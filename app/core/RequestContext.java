package core;

import entities.User;
import play.mvc.Http;
import services.ServiceProvider;
import utils.ObjectUtils;

import java.time.Duration;
import java.time.Instant;

public class RequestContext {

  private Instant start;
  private Http.Request request;
  private ServiceProvider serviceProvider;
  private User currentUser;

  public RequestContext(Http.Request request,
                        ServiceProvider serviceProvider) {
    this(request, serviceProvider, null);
  }

  public RequestContext(Http.Request request,
                        ServiceProvider serviceProvider,
                        User currentUser) {
    this.start = Instant.now();
    this.request = request;
    this.serviceProvider = serviceProvider;
    this.currentUser = currentUser;
  }

  public long getElapsedMillis() {
    Instant end = Instant.now();
    return Duration.between(start, end).toMillis();
  }

  public User getCurrentUser() {
    return currentUser;
  }

  public void setCurrentUser(User currentUser) {
    this.currentUser = currentUser;
  }

  public String getCurrentUserId() {
    return isAuthenticated() ? currentUser.getUserId() : null;
  }

  public boolean isAuthenticated() {
    return currentUser != null;
  }

  public ServiceProvider getServiceProvider() {
    return serviceProvider;
  }

  @Override
  public String toString() {
    return ObjectUtils.reflectionToString(this);
  }

}
