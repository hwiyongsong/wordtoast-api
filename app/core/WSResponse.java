package core;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

public class WSResponse {

  private RequestContext requestContext;
  private Object data;

  public WSResponse(RequestContext requestContext) {
    this.requestContext = requestContext;
  }

  public static WSResponse of(RequestContext requestContext) {
    return new WSResponse(requestContext);
  }

  public WSResponse withData(Object data) {
    this.data = data;
    return this;
  }

  public JsonNode toJson() {
    ObjectNode json = Json.newObject();
    json.set("meta", getMetaJson());

    if (data != null) {
      json.set("data", Json.toJson(data));
    }

    return json;
  }

  @SuppressWarnings("rawtypes")
  private JsonNode getMetaJson() {
    ObjectNode json = Json.newObject();
    json.put("elapsed", requestContext.getElapsedMillis() + "ms");
    return json;
  }

}
