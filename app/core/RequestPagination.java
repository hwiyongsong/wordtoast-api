package core;

public class RequestPagination {

  private int limit;
  private int offset;

  public RequestPagination() {
    this(10);
  }

  public RequestPagination(int limit) {
    this(limit, 0);
  }

  public RequestPagination(int limit, int offset) {
    this.limit = limit;
    this.offset = offset;
  }

  public int getLimit() {
    return limit;
  }

  public int getOffset() {
    return offset;
  }

}
