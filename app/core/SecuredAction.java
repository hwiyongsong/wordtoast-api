package core;

import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import java.util.Optional;

public class SecuredAction extends Security.Authenticator {

  @Override
  public Optional<String> getUsername(Http.Request request) {
    return request.getHeaders().get("Authorization");
  }

  @Override
  public Result onUnauthorized(Http.Request request) {
    return super.onUnauthorized(request);
  }

}
