package core;

import play.mvc.Http;
import utils.StringUtils;

import java.util.Optional;

public class AccessToken {

  private String value;

  public AccessToken(Http.Request request) {
    Optional<String> authorization = request.getHeaders().get("Authorization");

    if (authorization.isPresent()) {
      this.value = StringUtils.substringAfter(authorization.get(), "Bearer ");
    }
  }

  public boolean isPresent() {
    return StringUtils.isNotBlank(value);
  }

  public String toString() {
    return value;
  }

}
