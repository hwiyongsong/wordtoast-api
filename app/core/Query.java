package core;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import entities.User;
import play.libs.Json;
import utils.ObjectUtils;
import utils.StringUtils;

import java.time.LocalDate;
import java.util.Map;

public class Query {

  private User currentUser;
  private Map<String, String> data;

  public Query(User currentUser, Map<String, String> data) {
    this.currentUser = currentUser;
    this.data = data;
  }

  public String getCurrentUserId() {
    return currentUser != null ? currentUser.getUserId() : StringUtils.EMPTY;
  }

  public void put(String key, Object value) {
    data.put(key, value != null ? value.toString() : null);
  }

  public String getString(String key) {
    return getStringOrElse(key, null);
  }

  public String getStringOrElse(String key, String defaultValue) {
    String value = data.get(key);
    return value != null ? value : defaultValue;
  }

  public Integer getInteger(String key) {
    return getIntegerOrElse(key, null);
  }

  public Integer getIntegerOrElse(String key, Integer defaultValue) {
    String value = getString(key);
    return value != null ? Integer.parseInt(value) : defaultValue;
  }

  public Boolean getBoolean(String key) {
    return getBooleanOrElse(key, null);
  }

  public Boolean getBooleanOrElse(String key, Boolean defaultValue) {
    String value = getString(key);
    return value != null ? StringUtils.equalsIgnoreCase(value, "true") || StringUtils.equalsIgnoreCase(value, "on") : defaultValue;
  }

  public LocalDate getDate(String key) {
    return getDateOrElse(key, null);
  }

  public LocalDate getDateOrElse(String key, LocalDate defaultValue) {
    String value = getString(key);
    return value != null ? LocalDate.parse(value) : defaultValue;
  }

  public <T extends Enum<T>> T getEnum(Class<T> enumClass, String key) {
    return getEnumOrElse(enumClass, key, null);
  }

  public <T extends Enum<T>> T getEnumOrElse(Class<T> enumClass, String key, T defaultValue) {
    String value = getString(key);
    return value != null ? Enum.valueOf(enumClass, value) : defaultValue;
  }

  public RequestPagination getPagination() {
    Integer limit = getIntegerOrElse(RequestParameter.LIMIT, 10);
    Integer offset = getIntegerOrElse(RequestParameter.OFFSET, 0);
    return new RequestPagination(limit, offset);
  }

  public boolean contains(String key) {
    return data.containsKey(key);
  }

  public JsonNode toJson() {
    ObjectNode json = Json.newObject();

    for (Map.Entry<String, String> entry : data.entrySet()) {
      json.put(entry.getKey(), entry.getValue());
    }

    return json;
  }

  @Override
  public String toString() {
    return ObjectUtils.reflectionToString(this);
  }

}
