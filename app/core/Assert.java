package core;

import utils.StringUtils;

import java.util.Map;
import java.util.Objects;

public class Assert extends org.springframework.util.Assert {

  public static void isPresent(Object value) {
    isPresent(value, StringUtils.EMPTY);
  }

  public static void isPresent(Object value, String message) {
    notNull(value, message);

    if (value instanceof String) {
      String str = (String) value;

      if (StringUtils.isBlank(str)) {
        throw new IllegalArgumentException(message);
      }
    }
  }

  public static void isExists(Object value) {
    isExists(value, StringUtils.EMPTY);
  }

  public static void isExists(Object value, String message) {
    notNull(value, message);

    if (value instanceof String) {
      String str = (String) value;

      if (StringUtils.isBlank(str)) {
        throw new IllegalArgumentException(message);
      }
    }
  }

  public static void isBlank(String value, String message) {
    isTrue(StringUtils.isBlank(value), message);
  }

  public static void isNotBlank(String value, String message) {
    isTrue(StringUtils.isNotBlank(value), message);
  }

  public static void equals(Object value1, Object value2) {
    equals(value1, value2, StringUtils.EMPTY);
  }

  public static void equals(Object value1, Object value2, String message) {
    isTrue(Objects.equals(value1, value2), message);
  }

  public static void isFalse(boolean condition, String message) {
    if (condition) {
      throw new IllegalArgumentException(message);
    }
  }

  @SuppressWarnings("rawtypes")
  public static void contains(Map map, Object key) {
    if (!map.containsKey(key)) {
      throw new IllegalArgumentException();
    }
  }

  @SuppressWarnings("rawtypes")
  public static void notContains(Map map, Object key) {
    if (map.containsKey(key)) {
      throw new IllegalArgumentException();
    }
  }

}
