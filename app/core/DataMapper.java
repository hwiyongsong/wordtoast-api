package core;

import com.google.common.collect.Maps;

import java.util.Map;

public class DataMapper {

  private Map<String, Object> data = Maps.newLinkedHashMap();

  public DataMapper put(String key, Object value) {
    data.put(key, value);
    return this;
  }

  public Map<String, Object> build() {
    return data;
  }

}
