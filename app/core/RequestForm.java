package core;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;
import utils.ObjectUtils;
import utils.StringUtils;

import java.time.LocalDate;
import java.util.Map;

public class RequestForm {

  private Map<String, String> data;

  public RequestForm(Map<String, String> data) {
    this.data = data;
  }

  public void put(String key, Object value) {
    data.put(key, value != null ? value.toString() : null);
  }

  public String getString(String key) {
    return data.get(key);
  }

  public Integer getInteger(String key) {
    String value = getString(key);
    return StringUtils.isNotBlank(value) ? Integer.parseInt(value) : null;
  }

  public Boolean getBoolean(String key) {
    String value = getString(key);
    return StringUtils.equalsIgnoreCase(value, "true") || StringUtils.equalsIgnoreCase(value, "on");
  }

  public LocalDate getDate(String key) {
    String value = getString(key);
    return StringUtils.isNotBlank(value) ? LocalDate.parse(value) : null;
  }

  public <T extends Enum<T>> T getEnum(Class<T> enumClass, String key) {
    String value = getString(key);
    return StringUtils.isNotBlank(value) ? Enum.valueOf(enumClass, value) : null;
  }

  public boolean contains(String key) {
    return data.containsKey(key);
  }

  public JsonNode toJson() {
    ObjectNode json = Json.newObject();

    for (Map.Entry<String, String> entry : data.entrySet()) {
      json.put(entry.getKey(), entry.getValue());
    }

    return json;
  }

  @Override
  public String toString() {
    return ObjectUtils.reflectionToString(this);
  }

}
