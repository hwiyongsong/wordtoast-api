package core;

public class RequestParameter {

  public static final String BODY = "body";
  public static final String BUCKET = "bucket";
  public static final String COVER_IMAGE_ID = "coverImageId";
  public static final String DISPLAY_NAME = "displayName";
  public static final String EMAIL = "email";
  public static final String FILENAME = "filename";
  public static final String FACEBOOK_USER_ID = "facebookUserId";
  public static final String FIREBASE_USER_ID = "firebaseUserId";
  public static final String HANDLE = "handle";
  public static final String HASHTAG_ID = "hashtagId";
  public static final String HASHTAG_TEXT = "hashtagText";
  public static final String HEADLINE = "headline";
  public static final String INPUT = "input";
  public static final String LIMIT = "limit";
  public static final String NOTES = "notes";
  public static final String OFFSET = "offset";
  public static final String POST_ID = "postId";
  public static final String PROMPT_ID = "promptId";
  public static final String POST_TYPE = "postType";
  public static final String PROFILE_IMAGE_ID = "profileImageId";
  public static final String PUBLISH_STATUS = "publishStatus";
  public static final String REACTION_TYPE = "reactionType";
  public static final String REFERRED_CODE = "referredCode";
  public static final String SEGMENT = "segment";
  public static final String SLUG = "slug";
  public static final String SOURCE_URL = "sourceUrl";
  public static final String STATUS = "status";
  public static final String TITLE = "title";
  public static final String TOPIC_ID = "topicId";
  public static final String VISIBILITY = "visibility";
  public static final String USER_ID = "userId";

}
