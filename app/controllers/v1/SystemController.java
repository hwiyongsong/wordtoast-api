package controllers.v1;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.typesafe.config.Config;
import core.WSResponse;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Http;
import play.mvc.Result;
import utils.ObjectUtils;

import javax.inject.Inject;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class SystemController extends AbstractController {

  private Config config;
  private HttpExecutionContext executionContext;

  @Inject
  public SystemController(Config config, HttpExecutionContext executionContext) {
    this.config = config;
    this.executionContext = executionContext;
  }

  public CompletionStage<Result> ping(Http.Request request) {
    return CompletableFuture.completedFuture(ok("OK"));
  }

  public CompletionStage<Result> generateUniqueKeys(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      int limit = 10;

      List<String> keys = Lists.newArrayList();
      for (int i = 0; i < limit; i++) {
        String key = ObjectUtils.generateUuid();
        keys.add(key);
      }

      WSResponse response = WSResponse.of(context).withData(keys);
      return CompletableFuture.completedFuture(ok(response.toJson()));
    });
  }

  public CompletionStage<Result> getHealth(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      long BYTES_IN_MB = 1024 * 1024;
      long maxMemoryInBytes = Runtime.getRuntime().maxMemory();
      long totalMemoryInBytes = Runtime.getRuntime().totalMemory();
      long freeMemoryInBytes = Runtime.getRuntime().freeMemory();
      long usedMemoryInBytes = totalMemoryInBytes - freeMemoryInBytes;

      Map<String, Object> data = Maps.newLinkedHashMap();
      data.put("app.environment", config.getString("app.environment"));
      data.put("jvm.maxMemory", maxMemoryInBytes / BYTES_IN_MB + " MB");
      data.put("jvm.totalMemory", totalMemoryInBytes / BYTES_IN_MB + " MB");
      data.put("jvm.freeMemory", freeMemoryInBytes / BYTES_IN_MB + " MB");
      data.put("jvm.usedMemory", usedMemoryInBytes / BYTES_IN_MB + " MB");
      data.put("jvm.timeZone", ZoneId.systemDefault().toString());
      data.put("jvm.timeZone.offset", OffsetDateTime.now().getOffset().toString());
      data.put("play.db.prototype.hikaricp.minimumIdle", config.getInt("db.default.hikaricp.minimumIdle"));
      data.put("play.db.prototype.hikaricp.maximumPoolSize", config.getInt("db.default.hikaricp.maximumPoolSize"));
      data.put("database.dispatcher.thread-pool-executor.fixed-pool-size", config.getInt("database.dispatcher.thread-pool-executor.fixed-pool-size"));

      WSResponse response = WSResponse.of(context).withData(data);
      return CompletableFuture.completedFuture(ok(response.toJson()));
    });
  }

}
