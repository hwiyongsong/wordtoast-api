package controllers.v1;

import core.Query;
import core.WSResponse;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

public class HomeController extends AbstractController {

  public CompletionStage<Result> getHomeFeed(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getHomeService().queryHomeFeed(context, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

}
