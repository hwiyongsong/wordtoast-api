package controllers.v1;

import core.WSResponse;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class OnboardingController extends AbstractController {

  public CompletionStage<Result> getNextStep(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      String nextStep = "NONE";

      WSResponse response = new WSResponse(context).withData(nextStep);
      return CompletableFuture.completedFuture(ok(response.toJson()));
    });
  }

}
