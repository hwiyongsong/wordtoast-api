package controllers.v1;

import core.Query;
import core.RequestForm;
import core.SecuredAction;
import core.WSResponse;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import java.util.concurrent.CompletionStage;

public class UserController extends AbstractController {

  /* USERS */

  public CompletionStage<Result> queryUsers(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getUserService().queryUsers(context, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> getUser(Http.Request request, String userId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getUserService().getUserById(context, userId).thenApplyAsync(user -> {
        if (user == null) {
          return notFound();
        }

        Object payload = user.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> createUser(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      RequestForm form = getRequestForm(request);

      return getUserService().createUser(context, form).thenApplyAsync(user -> {
        Object payload = user.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> updateUser(Http.Request request, String userId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      RequestForm form = getRequestForm(request);

      return getUserService().updateUser(context, userId, form).thenApplyAsync(user -> {
        Object payload = user.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> queryUserPosts(Http.Request request, String userId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getUserService().queryUserPosts(context, userId, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> queryUserBookmarks(Http.Request request, String userId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getUserService().queryUserBookmarks(context, userId, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> queryUserFriends(Http.Request request, String userId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getUserService().queryUserFriends(context, userId, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> queryUserFollowings(Http.Request request, String userId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getUserService().queryUserFollowings(context, userId, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> queryUserFollowers(Http.Request request, String userId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getUserService().queryUserFollowers(context, userId, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> createUserFollow(Http.Request request, String userId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getUserService().createUserFollow(context, userId).thenApplyAsync(follow -> {
        Object payload = follow.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> removeUserFollow(Http.Request request, String userId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getUserService().removeUserFollow(context, userId).thenApplyAsync(result -> {
        WSResponse response = WSResponse.of(context);
        return ok(response.toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> createUserBlock(Http.Request request, String userId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getUserService().createUserBlock(context, userId).thenApplyAsync(userBlock -> {
        Object payload = userBlock.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> removeUserBlock(Http.Request request, String userId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getUserService().removeUserBlock(context, userId).thenApplyAsync(result -> {
        WSResponse response = WSResponse.of(context);
        return ok(response.toJson());
      }, executionContext.current());
    });
  }

}
