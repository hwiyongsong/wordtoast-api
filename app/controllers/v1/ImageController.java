package controllers.v1;

import core.RequestForm;
import core.WSResponse;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

public class ImageController extends AbstractController {

  public CompletionStage<Result> createImage(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      RequestForm form = getRequestForm(request);

      return getImageService().createImage(context, form).thenApplyAsync(image -> {
        Object payload = image.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> getImage(Http.Request request, String imageId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getImageService().getImageById(context, imageId).thenApplyAsync(image -> {
        if (image == null) {
          return notFound();
        }

        Object payload = image.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

}
