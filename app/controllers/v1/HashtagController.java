package controllers.v1;

import core.Query;
import core.WSResponse;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

public class HashtagController extends AbstractController {

  public CompletionStage<Result> queryHashtags(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getHashtagService().queryHashtags(context, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> getHashtag(Http.Request request, String hashtagId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getHashtagService().getHashtagById(context, hashtagId).thenApplyAsync(hashtag -> {
        if (hashtag == null) {
          return notFound();
        }

        Object payload = hashtag.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

}
