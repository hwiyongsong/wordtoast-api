package controllers.v1;

import core.Query;
import core.WSResponse;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

public class SearchController extends AbstractController {

  public CompletionStage<Result> search(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getSearchService().search(context, query).thenApplyAsync(searchResults -> {
        Object payload = searchResults.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

}
