package controllers.v1;

import core.RequestParameter;
import core.Query;
import core.SecuredAction;
import core.WSResponse;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import java.util.concurrent.CompletionStage;

public class TopicController extends AbstractController {

  public CompletionStage<Result> queryTopics(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);
      query.put(RequestParameter.LIMIT, 100);

      return getTopicService().queryTopics(context, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> getTopic(Http.Request request, String topicId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getTopicService().getTopicById(context, topicId).thenApplyAsync(topic -> {
        if (topic == null) {
          return notFound();
        }

        Object payload = topic.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> createTopicFollow(Http.Request request, String topicId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getTopicService().createTopicFollow(context, topicId).thenApplyAsync(follow -> {
        Object payload = follow.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> removeTopicFollow(Http.Request request, String topicId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getTopicService().removeTopicFollow(context, topicId).thenApplyAsync(result -> {
        WSResponse response = WSResponse.of(context);
        return ok(response.toJson());
      }, executionContext.current());
    });
  }

}
