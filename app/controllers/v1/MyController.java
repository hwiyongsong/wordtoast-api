package controllers.v1;

import core.Query;
import core.SecuredAction;
import core.WSResponse;
import entities.User;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import java.util.concurrent.CompletionStage;

public class MyController extends AbstractController {

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> getMyself(Http.Request request) {
    return getRequestContext(request).thenApplyAsync(context -> {
      User currentUser = context.getCurrentUser();
      return ok(WSResponse.of(context).withData(currentUser).toJson());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> queryMyPosts(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getMyService().queryMyPosts(context, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> queryMyBookmarks(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getMyService().queryMyBookmarks(context, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> queryMyFollowings(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getMyService().queryMyFollowings(context, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> queryMyFollowers(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getMyService().queryMyFollowers(context, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> queryMyNotifications(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getMyService().queryMyNotifications(context, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> countMyNotifications(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getMyService().countMyUnreadNotifications(context).thenApplyAsync(count -> {
        return ok(WSResponse.of(context).withData(count).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> markMyNotificationsAsRead(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getMyService().markMyNotificationsAsRead(context).thenApplyAsync(numUpdated -> {
        return ok(WSResponse.of(context).withData(numUpdated).toJson());
      }, executionContext.current());
    });
  }

}
