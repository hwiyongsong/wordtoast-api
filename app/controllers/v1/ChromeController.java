package controllers.v1;

import core.Query;
import core.WSResponse;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

public class ChromeController extends AbstractController {

  public CompletionStage<Result> getChromeHeader(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getChromeService().getChromeHeader(context, query).thenApplyAsync(chromeHeader -> {
        Object payload = chromeHeader.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

}
