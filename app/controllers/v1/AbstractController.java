package controllers.v1;

import com.google.common.collect.Maps;
import core.AccessToken;
import core.RequestContext;
import core.RequestForm;
import core.Query;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http;
import services.ChromeService;
import services.ExploreService;
import services.HashtagService;
import services.HomeService;
import services.ImageService;
import services.MyService;
import services.NotificationService;
import services.PostService;
import services.SearchService;
import services.ServiceProvider;
import services.TopicService;
import services.UserService;
import utils.ArrayUtils;

import javax.inject.Inject;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public abstract class AbstractController extends Controller {

  @Inject
  protected ServiceProvider serviceProvider;

  @Inject
  protected HttpExecutionContext executionContext;

  protected CompletionStage<RequestContext> getRequestContext(Http.Request request) {
    AccessToken accessToken = new AccessToken(request);

    if (accessToken.isPresent()) {
      return getUserService().getUserByAccessToken(accessToken).thenApplyAsync(currentUser -> {
        return new RequestContext(request, serviceProvider, currentUser);
      });
    } else {
      RequestContext requestContext = new RequestContext(request, serviceProvider);
      return CompletableFuture.completedFuture(requestContext);
    }
  }

  protected Query getRequestQuery(RequestContext requestContext, Http.Request request) {
    Map<String, String[]> queryBody = request.queryString();
    Map<String, String> data = queryBody.entrySet()
                                        .stream()
                                        .collect(Collectors.toMap(entry -> entry.getKey(), entry -> ArrayUtils.getFirst(entry.getValue())));
    return new Query(requestContext.getCurrentUser(), data);
  }

  protected RequestForm getRequestForm(Http.Request request) {
    Map<String, String[]> formBody = request.hasBody() ? request.body().asFormUrlEncoded() : Maps.newHashMap();
    Map<String, String> data = formBody.entrySet()
                                       .stream()
                                       .collect(Collectors.toMap(entry -> entry.getKey(), entry -> ArrayUtils.getFirst(entry.getValue())));
    return new RequestForm(data);
  }

  protected ChromeService getChromeService() {
    return serviceProvider.getChromeService();
  }

  protected ExploreService getExploreService() {
    return serviceProvider.getExploreService();
  }

  protected HashtagService getHashtagService() {
    return serviceProvider.getHashtagService();
  }

  protected HomeService getHomeService() {
    return serviceProvider.getHomeService();
  }

  protected ImageService getImageService() {
    return serviceProvider.getImageService();
  }

  protected MyService getMyService() {
    return serviceProvider.getMyService();
  }

  protected NotificationService getNotificationService() {
    return serviceProvider.getNotificationService();
  }

  protected PostService getPostService() {
    return serviceProvider.getPostService();
  }

  protected SearchService getSearchService() {
    return serviceProvider.getSearchService();
  }

  protected TopicService getTopicService() {
    return serviceProvider.getTopicService();
  }

  protected UserService getUserService() {
    return serviceProvider.getUserService();
  }

}
