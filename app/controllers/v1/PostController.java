package controllers.v1;

import core.Query;
import core.RequestForm;
import core.RequestParameter;
import core.WSResponse;
import entities.ReactionType;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

public class PostController extends AbstractController {

  public CompletionStage<Result> queryPosts(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getPostService().queryPosts(context, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> getPost(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getPostService().getPostById(context, postId).thenApplyAsync(post -> {
        if (post == null) {
          return notFound();
        }

        Object payload = post.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> createPost(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      RequestForm form = getRequestForm(request);

      return getPostService().createPost(context, form).thenApplyAsync(post -> {
        Object payload = post.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> updatePost(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      RequestForm form = getRequestForm(request);

      return getPostService().updatePost(context, postId, form).thenApplyAsync(post -> {
        Object payload = post.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> removePost(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getPostService().removePost(context, postId).thenApplyAsync(result -> {
        WSResponse response = WSResponse.of(context);
        return ok(response.toJson());
      }, executionContext.current());
    });
  }

  /**** POST REACTIONS ****/

  public CompletionStage<Result> queryPostReactions(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getPostService().queryPostReactions(context, postId, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> createPostReaction(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      RequestForm form = getRequestForm(request);
      ReactionType reactionType = form.getEnum(ReactionType.class, RequestParameter.REACTION_TYPE);

      if (reactionType == null) {
        reactionType = ReactionType.LIKE;
      }

      return getPostService().createPostReaction(context, postId, reactionType).thenApplyAsync(postReaction -> {
        Object payload = postReaction.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> removePostReaction(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getPostService().removePostReaction(context, postId).thenApplyAsync(result -> {
        return ok(WSResponse.of(context).toJson());
      }, executionContext.current());
    });
  }

  /**** POST COMMENTS ****/

  public CompletionStage<Result> queryPostComments(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getPostService().queryPostComments(context, postId, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> createPostComment(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      RequestForm form = getRequestForm(request);

      return getPostService().createPostComment(context, postId, form).thenApplyAsync(postReaction -> {
        Object payload = postReaction.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> updatePostComment(Http.Request request, String postId, String postCommentId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      RequestForm form = getRequestForm(request);

      return getPostService().updatePostComment(context, postCommentId, form).thenApplyAsync(postReaction -> {
        Object payload = postReaction.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> removePostComment(Http.Request request, String postId, String postCommentId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getPostService().removePostComment(context, postCommentId).thenApplyAsync(result -> {
        return ok(WSResponse.of(context).toJson());
      }, executionContext.current());
    });
  }

  /* POST BLOCKS */

  public CompletionStage<Result> createPostBlock(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getPostService().createPostBlock(context, postId).thenApplyAsync(postBlock -> {
        Object payload = postBlock.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> removePostBlock(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getPostService().removePostBlock(context, postId).thenApplyAsync(result -> {
        return ok(WSResponse.of(context).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> queryPostBookmarks(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);
      query.put(RequestParameter.POST_ID, postId);

      return getPostService().queryPostBookmarks(context, query).thenApplyAsync(feed -> {
        Object payload = feed.toSummaryPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> createPostBookmark(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      RequestForm form = getRequestForm(request);

      return getPostService().createPostBookmark(context, postId).thenApplyAsync(postReaction -> {
        Object payload = postReaction.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  public CompletionStage<Result> removePostBookmark(Http.Request request, String postId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getPostService().removePostBookmark(context, postId).thenApplyAsync(result -> {
        return ok(WSResponse.of(context).toJson());
      }, executionContext.current());
    });
  }

}
