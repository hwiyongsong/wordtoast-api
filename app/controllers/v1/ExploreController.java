package controllers.v1;

import core.Query;
import core.WSResponse;
import play.mvc.Http;
import play.mvc.Result;

import java.util.concurrent.CompletionStage;

public class ExploreController extends AbstractController {

  public CompletionStage<Result> getExploreFeed(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      Query query = getRequestQuery(context, request);

      return getExploreService().queryExploreFeed(context, query).thenApplyAsync(feed -> {
        Object payload = feed.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

}
