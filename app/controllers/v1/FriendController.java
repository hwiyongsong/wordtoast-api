package controllers.v1;

import core.RequestForm;
import core.SecuredAction;
import core.WSResponse;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import java.util.concurrent.CompletionStage;

public class FriendController extends AbstractController {

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> createFriendRequest(Http.Request request) {
    return getRequestContext(request).thenComposeAsync(context -> {
      RequestForm form = getRequestForm(request);

      return getUserService().createFriendRequest(context, form).thenApplyAsync(friendRequest -> {
        Object payload = friendRequest.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> getFriendRequest(Http.Request request, String friendRequestId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getUserService().getFriendRequestById(context, friendRequestId).thenApplyAsync(friendRequest -> {
        Object payload = friendRequest.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> updateFriendRequest(Http.Request request, String friendRequestId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      RequestForm form = getRequestForm(request);

      return getUserService().updateFriendRequest(context, friendRequestId, form).thenApplyAsync(userFriend -> {
        Object payload = userFriend.toDataPayload(context);
        return ok(WSResponse.of(context).withData(payload).toJson());
      }, executionContext.current());
    });
  }

  @Security.Authenticated(SecuredAction.class)
  public CompletionStage<Result> removeFriendRequest(Http.Request request, String friendRequestId) {
    return getRequestContext(request).thenComposeAsync(context -> {
      return getUserService().removeFriendRequest(context, friendRequestId).thenApplyAsync(result -> {
        return ok(WSResponse.of(context).toJson());
      }, executionContext.current());
    });
  }

}
