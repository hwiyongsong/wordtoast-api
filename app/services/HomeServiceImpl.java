package services;

import core.Query;
import core.RequestContext;
import core.RequestParameter;
import database.HomeDao;
import entities.HomeSegment;
import entities.QueryResult;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;

@Singleton
public class HomeServiceImpl implements HomeService {

  private HomeDao homeDao;
  private PostService postService;

  @Inject
  public HomeServiceImpl(HomeDao homeDao, PostService postService) {
    this.homeDao = homeDao;
    this.postService = postService;
  }

  @Override
  public CompletableFuture<QueryResult> queryHomeFeed(RequestContext context, Query query) {
    HomeSegment homeSegment = getHomeSegment(context, query);

    switch (homeSegment) {
      case FOR_YOU:
        return homeDao.queryForYou(context, query).thenApplyAsync(entities -> new QueryResult(query, entities));
      case FOLLOWING:
        return homeDao.queryFollowing(context, query).thenApplyAsync(entities -> new QueryResult(query, entities));
      case POPULAR:
        return homeDao.queryPopular(context, query).thenApplyAsync(entities -> new QueryResult(query, entities));
      default:
        throw new IllegalArgumentException("Unsupported home segment: " + homeSegment);
    }

  }

  private HomeSegment getHomeSegment(RequestContext context, Query query) {
    if (context.isAuthenticated()) {
      return query.getEnumOrElse(HomeSegment.class, RequestParameter.SEGMENT, HomeSegment.FOR_YOU);
    } else {
      return HomeSegment.POPULAR;
    }
  }

}
