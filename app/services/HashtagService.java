package services;

import com.google.inject.ImplementedBy;
import core.Query;
import core.RequestContext;
import entities.Hashtag;
import entities.HashtagFollow;
import entities.QueryResult;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@ImplementedBy(HashtagServiceImpl.class)
public interface HashtagService {

  CompletableFuture<QueryResult> queryHashtags(RequestContext context, Query query);

  CompletableFuture<Hashtag> getHashtagById(RequestContext context, String hashtagId);

  CompletableFuture<List<Hashtag>> resolveHashtags(RequestContext context, List<String> hashtagNames);

  CompletableFuture<HashtagFollow> createHashtagFollow(RequestContext context, String hashtagId);

  CompletableFuture<Void> removeHashtagFollow(RequestContext context, String hashtagId);

}
