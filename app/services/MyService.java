package services;

import com.google.inject.ImplementedBy;
import core.Query;
import core.RequestContext;
import entities.QueryResult;

import java.util.concurrent.CompletableFuture;

@ImplementedBy(MyServiceImpl.class)
public interface MyService {

  CompletableFuture<QueryResult> queryMyPosts(RequestContext context, Query query);

  CompletableFuture<QueryResult> queryMyBookmarks(RequestContext context, Query query);

  CompletableFuture<QueryResult> queryMyFollowers(RequestContext context, Query query);

  CompletableFuture<QueryResult> queryMyFollowings(RequestContext context, Query query);

  CompletableFuture<QueryResult> queryMyFriends(RequestContext context, Query query);

  CompletableFuture<QueryResult> queryMyNotifications(RequestContext context, Query query);

  CompletableFuture<Integer> countMyUnreadNotifications(RequestContext context);

  CompletableFuture<Integer> markMyNotificationsAsRead(RequestContext context);

}
