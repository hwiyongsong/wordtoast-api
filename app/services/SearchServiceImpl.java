package services;

import core.Query;
import core.RequestContext;
import core.RequestParameter;
import entities.Post;
import entities.QueryResult;
import entities.SearchResult;
import entities.User;
import utils.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Singleton
public class SearchServiceImpl implements SearchService {

  private UserService userService;
  private PostService postService;

  @Inject
  public SearchServiceImpl(UserService userService,
                           PostService postService) {
    this.userService = userService;
    this.postService = postService;
  }

  @Override
  public CompletableFuture<SearchResult> search(RequestContext context, Query query) {
    String input = query.getString(RequestParameter.INPUT);
    SearchResult searchResults = new SearchResult(input);

    if (StringUtils.isBlank(input)) {
      return CompletableFuture.completedFuture(searchResults);
    }

    query.put(RequestParameter.LIMIT, 5);
    CompletableFuture<QueryResult> usersQueryResultFuture = userService.queryUsers(context, query);
    CompletableFuture<QueryResult> postsQueryResultFuture = postService.queryPosts(context, query);

    return CompletableFuture.allOf(usersQueryResultFuture, postsQueryResultFuture).thenApplyAsync(ignored -> {
      List<User> users = usersQueryResultFuture.join().getEntities();
      List<Post> posts = postsQueryResultFuture.join().getEntities();

      searchResults.setUsers(users);
      searchResults.setPosts(posts);

      return searchResults;
    });

  }

}
