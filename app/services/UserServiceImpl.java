package services;

import core.AccessToken;
import core.Assert;
import core.Query;
import core.RequestContext;
import core.RequestForm;
import core.RequestParameter;
import database.UserDao;
import entities.FriendRequest;
import entities.FriendRequestStatus;
import entities.Image;
import entities.PublishStatus;
import entities.QueryResult;
import entities.RoleType;
import entities.User;
import entities.UserBlock;
import entities.UserFollow;
import entities.UserFriend;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.ObjectUtils;
import utils.StringUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.OffsetDateTime;
import java.util.concurrent.CompletableFuture;

@Singleton
public class UserServiceImpl implements UserService {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private UserDao userDao;
  private ImageService imageService;
  private NotificationService notificationService;
  private PostService postService;
  private CacheService cacheService;

  @Inject
  public UserServiceImpl(UserDao userDao,
                         ImageService imageService,
                         NotificationService notificationService,
                         PostService postService,
                         CacheService cacheService) {
    this.userDao = userDao;
    this.imageService = imageService;
    this.notificationService = notificationService;
    this.postService = postService;
    this.cacheService = cacheService;
  }

  /**** USERS ****/

  @Override
  public CompletableFuture<QueryResult> queryUsers(RequestContext context, Query query) {
    return userDao.queryUsers(context, query).thenApplyAsync(users -> new QueryResult(query, users));
  }

  @Override
  public CompletableFuture<User> getUserById(RequestContext context, String userId) {
    return userDao.getUserById(context, userId);
  }

  @Override
  public CompletableFuture<User> getUserByAccessToken(AccessToken accessToken) {
    String firebaseUserId = accessToken.toString();
    String cacheKey = "urn:user:firebase:" + firebaseUserId;

    return cacheService.get(cacheKey).thenComposeAsync(cached -> {
      if (cached != null) {
        User cachedUser = (User) cached;
        return CompletableFuture.completedFuture(cachedUser);
      }

      return userDao.getUserByFirebaseUserId(firebaseUserId).thenApply(user -> {
        if (user != null) {
          cacheService.set(cacheKey, user);
        }

        return user;
      });
    });
  }

  @Override
  public CompletableFuture<User> createUser(RequestContext context, RequestForm form) {
    String displayName = form.getString(RequestParameter.DISPLAY_NAME);
    String email = form.getString(RequestParameter.EMAIL);
    String firebaseUserId = form.getString(RequestParameter.FIREBASE_USER_ID);
    String facebookUserId = form.getString(RequestParameter.FACEBOOK_USER_ID);
    String referralCode = ObjectUtils.generateUuid();
    String referredCode = form.getString(RequestParameter.REFERRED_CODE);

    User user = new User();
    user.setRoleType(RoleType.MEMBER);
    user.setDisplayName(displayName);
    user.setEmail(email);
    user.setFirebaseUserId(firebaseUserId);
    user.setFacebookUserId(facebookUserId);
    user.setReferralCode(referralCode);
    user.setReferredCode(referredCode);

    if (StringUtils.isNotBlank(facebookUserId)) {
      Image profileImage = new Image();
      profileImage.setSourceUrl("https://graph.facebook.com/" + facebookUserId + "/picture?type=large");
      profileImage.setCreatedBy(user);
      profileImage.setCreatedAt(OffsetDateTime.now());

      user.setProfileImage(profileImage);
    }

    return userDao.saveUser(context, user);
  }

  @Override
  public CompletableFuture<User> updateUser(RequestContext context, String userId, RequestForm form) {
    String displayName = form.getString(RequestParameter.DISPLAY_NAME);
    String headline = form.getString(RequestParameter.HEADLINE);
    String profileImageId = form.getString(RequestParameter.PROFILE_IMAGE_ID);
    String handle = form.getString(RequestParameter.HANDLE);

    CompletableFuture<User> userFuture = getUserById(context, userId);
    CompletableFuture<Image> profileImageFuture = StringUtils.isNoneBlank(profileImageId) ? imageService.getImageById(context, profileImageId) : CompletableFuture.completedFuture(null);

    return CompletableFuture.allOf(userFuture, profileImageFuture).thenComposeAsync(ignored -> {
      User user = userFuture.join();

      if (form.contains(RequestParameter.DISPLAY_NAME)) {
        user.setDisplayName(displayName);
      }

      if (form.contains(RequestParameter.HEADLINE)) {
        user.setHeadline(headline);
      }

      if (form.contains(RequestParameter.PROFILE_IMAGE_ID)) {
        user.setProfileImage(profileImageFuture.join());
      }

      if (form.contains(RequestParameter.HANDLE)) {
        user.setHandle(handle);
      }

      return userDao.saveUser(context, user);
    });
  }

  /**** USER POSTS ****/

  @Override
  public CompletableFuture<QueryResult> queryUserPosts(RequestContext context, String userId, Query query) {
    query.put(RequestParameter.USER_ID, userId);
    query.put(RequestParameter.PUBLISH_STATUS, PublishStatus.PUBLISHED);
    return postService.queryPosts(context, query);
  }

  /**** USER BOOKMARKS ****/

  @Override
  public CompletableFuture<QueryResult> queryUserBookmarks(RequestContext context, String userId, Query query) {
    query.put(RequestParameter.USER_ID, userId);
    return postService.queryPostBookmarks(context, query);
  }

  /**** USER FOLLOWS ****/

  @Override
  public CompletableFuture<QueryResult> queryUserFollowers(RequestContext context, String userId, Query query) {
    query.put(RequestParameter.USER_ID, userId);
    return userDao.queryUserFollowers(context, query).thenApplyAsync(users -> new QueryResult(query, users));
  }

  @Override
  public CompletableFuture<QueryResult> queryUserFollowings(RequestContext context, String userId, Query query) {
    query.put(RequestParameter.USER_ID, userId);
    return userDao.queryUserFollowings(context, query).thenApplyAsync(users -> new QueryResult(query, users));
  }

  @Override
  public CompletableFuture<UserFollow> createUserFollow(RequestContext context, String userId) {
    return getUserById(context, userId).thenComposeAsync(user -> {
      UserFollow follow = new UserFollow();
      follow.setUser(user);
      follow.setCreatedBy(context.getCurrentUser());

      return userDao.saveUserFollow(context, follow).thenApplyAsync(persistedUserFollow -> {
        notificationService.createUserFollowNotification(context, persistedUserFollow);
        return persistedUserFollow;
      });
    });
  }

  @Override
  public CompletableFuture<Void> removeUserFollow(RequestContext context, String userId) {
    return userDao.getUserFollow(context, userId).thenComposeAsync(userFollow -> {
      return userDao.removeUserFollow(context, userFollow);
    });
  }

  /**** USER FRIENDS ****/

  @Override
  public CompletableFuture<QueryResult> queryUserFriends(RequestContext context, String userId, Query query) {
    return userDao.queryUserFriends(context, userId, query).thenApplyAsync(userFriends -> new QueryResult(query, userFriends));
  }

  /**** FRIEND REQUESTS ****/

  @Override
  public CompletableFuture<FriendRequest> createFriendRequest(RequestContext context, RequestForm form) {
    String userId = form.getString(RequestParameter.USER_ID);
    String notes = form.getString(RequestParameter.NOTES);

    return getUserById(context, userId).thenComposeAsync(user -> {
      FriendRequest friendRequest = new FriendRequest();
      friendRequest.setUser(user);
      friendRequest.setNotes(notes);
      friendRequest.setStatus(FriendRequestStatus.PENDING);
      friendRequest.setCreatedBy(context.getCurrentUser());

      return userDao.saveFriendRequest(context, friendRequest).thenApplyAsync(persistedFriendRequest -> {
        notificationService.createFriendRequestCreateNotification(context, persistedFriendRequest);
        return persistedFriendRequest;
      });
    });
  }

  @Override
  public CompletableFuture<FriendRequest> getFriendRequestById(RequestContext context, String friendRequestId) {
    return userDao.getFriendRequestById(context, friendRequestId);
  }

  @Override
  public CompletableFuture<FriendRequest> updateFriendRequest(RequestContext context, String friendRequestId, RequestForm form) {
    User currentUser = context.getCurrentUser();

    return getFriendRequestById(context, friendRequestId).thenComposeAsync(friendRequest -> {
      User user = friendRequest.getUser();
      User createdBy = friendRequest.getCreatedBy();
      Assert.equals(user, currentUser);

      FriendRequestStatus status = form.getEnum(FriendRequestStatus.class, RequestParameter.STATUS);

      if (status == FriendRequestStatus.ACCEPTED) {
        try {
          UserFriend userFriend = new UserFriend();
          userFriend.setUser(user);
          userFriend.setCreatedBy(createdBy);

          userDao.saveUserFriend(context, userFriend).thenAccept(result -> {
            UserFriend userFriendReverse = new UserFriend();
            userFriendReverse.setUser(createdBy);
            userFriendReverse.setCreatedBy(user);
            userDao.saveUserFriend(context, userFriendReverse);
          });
        } catch (Exception e) {
          logger.error("Unable to create friendship", e);
        }

        try {
          UserFollow userFollow = new UserFollow();
          userFollow.setUser(user);
          userFollow.setCreatedBy(createdBy);

          userDao.saveUserFollow(context, userFollow).thenAccept(result -> {
            UserFollow userFollowReverse = new UserFollow();
            userFollowReverse.setUser(createdBy);
            userFollowReverse.setCreatedBy(user);
            userDao.saveUserFollow(context, userFollowReverse);
          });
        } catch (Exception e) {
          logger.error("Unable to create follow", e);
        }

        friendRequest.setStatus(FriendRequestStatus.ACCEPTED);
        friendRequest.setRespondedAt(OffsetDateTime.now());

        return userDao.saveFriendRequest(context, friendRequest).thenApplyAsync(persistedFriendRequest -> {
          notificationService.createFriendRequestAcceptNotification(context, persistedFriendRequest);
          return persistedFriendRequest;
        });
      }

      if (status == FriendRequestStatus.DECLINED) {
        friendRequest.setStatus(FriendRequestStatus.DECLINED);
        friendRequest.setRespondedAt(OffsetDateTime.now());
        return userDao.saveFriendRequest(context, friendRequest);
      }

      return CompletableFuture.completedFuture(friendRequest);
    });
  }

  @Override
  public CompletableFuture<Void> removeFriendRequest(RequestContext context, String friendRequestId) {
    return getFriendRequestById(context, friendRequestId).thenComposeAsync(friendRequest -> {
      return userDao.removeFriendRequest(context, friendRequest);
    });
  }

  /**** USER BLOCKS ****/

  @Override
  public CompletableFuture<UserBlock> createUserBlock(RequestContext context, String userId) {
    return getUserById(context, userId).thenComposeAsync(user -> {
      UserBlock userBlock = new UserBlock();
      userBlock.setUser(user);
      userBlock.setCreatedBy(context.getCurrentUser());
      return userDao.saveUserBlock(context, userBlock);
    });
  }

  @Override
  public CompletableFuture<Void> removeUserBlock(RequestContext context, String userId) {
    return userDao.getUserBlock(context, userId).thenComposeAsync(userBlock -> {
      return userDao.removeUserBlock(context, userBlock);
    });
  }

}
