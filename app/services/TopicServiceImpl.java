package services;

import core.Query;
import core.RequestContext;
import database.TopicDao;
import entities.QueryResult;
import entities.Topic;
import entities.TopicFollow;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;

@Singleton
public class TopicServiceImpl implements TopicService {

  private TopicDao topicDao;

  @Inject
  public TopicServiceImpl(TopicDao topicDao) {
    this.topicDao = topicDao;
  }

  @Override
  public CompletableFuture<QueryResult> queryTopics(RequestContext context, Query query) {
    return topicDao.queryTopics(context, query).thenApplyAsync(topics -> new QueryResult(query, topics));
  }

  @Override
  public CompletableFuture<Topic> getTopicById(RequestContext context, String topicId) {
    return topicDao.getTopicById(context, topicId);
  }

  @Override
  public CompletableFuture<TopicFollow> createTopicFollow(RequestContext context, String topicId) {
    return getTopicById(context, topicId).thenComposeAsync(topic -> {
      TopicFollow follow = new TopicFollow();
      follow.setTopic(topic);
      follow.setCreatedBy(context.getCurrentUser());
      return topicDao.saveTopicFollow(context, follow);
    });
  }

  @Override
  public CompletableFuture<Void> removeTopicFollow(RequestContext context, String topicId) {
    return topicDao.getTopicFollow(context, topicId).thenComposeAsync(topicFollow -> {
      return topicDao.removeTopicFollow(context, topicFollow);
    });
  }

}
