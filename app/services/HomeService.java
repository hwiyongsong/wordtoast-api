package services;

import com.google.inject.ImplementedBy;
import core.RequestContext;
import core.Query;
import entities.QueryResult;

import java.util.concurrent.CompletableFuture;

@ImplementedBy(HomeServiceImpl.class)
public interface HomeService {

  CompletableFuture<QueryResult> queryHomeFeed(RequestContext context, Query query);

}
