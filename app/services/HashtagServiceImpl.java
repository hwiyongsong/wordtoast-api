package services;

import core.Query;
import core.RequestContext;
import database.HashtagDao;
import entities.Hashtag;
import entities.HashtagFollow;
import entities.QueryResult;
import utils.FutureUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Singleton
public class HashtagServiceImpl implements HashtagService {

  private HashtagDao hashtagDao;

  @Inject
  public HashtagServiceImpl(HashtagDao hashtagDao) {
    this.hashtagDao = hashtagDao;
  }

  @Override
  public CompletableFuture<QueryResult> queryHashtags(RequestContext context, Query query) {
    return hashtagDao.queryHashtags(context, query).thenApplyAsync(hashtags -> new QueryResult(query, hashtags));
  }

  @Override
  public CompletableFuture<Hashtag> getHashtagById(RequestContext context, String hashtagId) {
    return hashtagDao.getHashtagById(context, hashtagId);
  }

  @Override
  public CompletableFuture<List<Hashtag>> resolveHashtags(RequestContext context, List<String> hashtagNames) {
    List<CompletableFuture<Hashtag>> hashtagFutures = hashtagNames.stream()
                                                                  .map(hashtagName -> hashtagDao.resolveHashtag(context, hashtagName))
                                                                  .collect(Collectors.toList());
    return FutureUtils.allOf(hashtagFutures);
  }

  @Override
  public CompletableFuture<HashtagFollow> createHashtagFollow(RequestContext context, String hashtagId) {
    return getHashtagById(context, hashtagId).thenComposeAsync(hashtag -> {
      HashtagFollow follow = new HashtagFollow();
      follow.setHashtag(hashtag);
      follow.setCreatedBy(context.getCurrentUser());
      return hashtagDao.saveHashtagFollow(context, follow);
    });
  }

  @Override
  public CompletableFuture<Void> removeHashtagFollow(RequestContext context, String hashtagId) {
    return hashtagDao.getHashtagFollow(context, hashtagId).thenComposeAsync(hashtagFollow -> {
      return hashtagDao.removeHashtagFollow(context, hashtagFollow);
    });
  }

}
