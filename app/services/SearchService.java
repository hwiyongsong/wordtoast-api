package services;

import com.google.inject.ImplementedBy;
import core.RequestContext;
import core.Query;
import entities.SearchResult;

import java.util.concurrent.CompletableFuture;

@ImplementedBy(SearchServiceImpl.class)
public interface SearchService {

  CompletableFuture<SearchResult> search(RequestContext context, Query query);

}
