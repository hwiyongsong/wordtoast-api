package services;

import core.Query;
import core.RequestContext;
import core.RequestParameter;
import database.NotificationDao;
import entities.FriendRequest;
import entities.Notification;
import entities.NotificationType;
import entities.PostComment;
import entities.PostReaction;
import entities.QueryResult;
import entities.User;
import entities.UserFollow;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;

@Singleton
public class NotificationServiceImpl implements NotificationService {

  private NotificationDao notificationDao;

  @Inject
  public NotificationServiceImpl(NotificationDao notificationDao) {
    this.notificationDao = notificationDao;
  }

  @Override
  public CompletableFuture<QueryResult> queryNotifications(RequestContext context, String recipientId, Query query) {
    query.put(RequestParameter.USER_ID, recipientId);
    return notificationDao.queryNotifications(context, query).thenApplyAsync(notifications -> new QueryResult(query, notifications));
  }

  @Override
  public CompletableFuture<Notification> getNotificationById(RequestContext context, String notificationId) {
    return notificationDao.getNotificationById(context, notificationId);
  }

  @Override
  public CompletableFuture<Void> removeNotification(RequestContext context, String notificationId) {
    return getNotificationById(context, notificationId).thenComposeAsync(notification -> {
      return notificationDao.removeNotification(context, notification);
    });
  }

  @Override
  public CompletableFuture<Integer> countUnreadNotifications(RequestContext context) {
    return notificationDao.countUnreadNotifications(context);
  }

  @Override
  public CompletableFuture<Integer> markNotificationsAsRead(RequestContext context) {
    return notificationDao.markNotificationsAsRead(context);
  }

  @Override
  public CompletableFuture<Notification> createFriendRequestCreateNotification(RequestContext context, FriendRequest friendRequest) {
    Notification notification = new Notification();
    notification.setNotificationType(NotificationType.FRIEND_REQUEST_CREATE);
    notification.setRecipient(friendRequest.getUser());
    notification.setActor(context.getCurrentUser());
    notification.setFriendRequest(friendRequest);

    return saveNotification(context, notification);
  }

  @Override
  public CompletableFuture<Notification> createFriendRequestAcceptNotification(RequestContext context, FriendRequest friendRequest) {
    Notification notification = new Notification();
    notification.setNotificationType(NotificationType.FRIEND_REQUEST_ACCEPT);
    notification.setRecipient(friendRequest.getCreatedBy());
    notification.setActor(context.getCurrentUser());
    notification.setFriendRequest(friendRequest);

    return saveNotification(context, notification);
  }

  @Override
  public CompletableFuture<Notification> createUserFollowNotification(RequestContext context, UserFollow userFollow) {
    Notification notification = new Notification();
    notification.setNotificationType(NotificationType.USER_FOLLOW);
    notification.setRecipient(userFollow.getUser());
    notification.setActor(context.getCurrentUser());
    notification.setUserFollow(userFollow);

    return saveNotification(context, notification);
  }

  @Override
  public CompletableFuture<Notification> createPostReactionNotification(RequestContext context, PostReaction postReaction) {
    Notification notification = new Notification();
    notification.setNotificationType(NotificationType.POST_REACT);
    notification.setRecipient(postReaction.getPost().getCreatedBy());
    notification.setActor(context.getCurrentUser());
    notification.setPost(postReaction.getPost());
    notification.setPostReaction(postReaction);

    return saveNotification(context, notification);
  }

  @Override
  public CompletableFuture<Notification> createPostCommentNotification(RequestContext context, PostComment postComment) {
    Notification notification = new Notification();
    notification.setNotificationType(NotificationType.POST_COMMENT);
    notification.setRecipient(postComment.getPost().getCreatedBy());
    notification.setActor(context.getCurrentUser());
    notification.setPost(postComment.getPost());
    notification.setPostComment(postComment);

    return saveNotification(context, notification);
  }

  private CompletableFuture<Notification> saveNotification(RequestContext context, Notification notification) {
    User recipient = notification.getRecipient();
    User actor = notification.getActor();

    if (recipient.equals(actor)) {
      return CompletableFuture.completedFuture(null);
    }

    return notificationDao.saveNotification(context, notification);
  }

}
