package services;

import com.google.inject.ImplementedBy;

@ImplementedBy(ServiceProviderImpl.class)
public interface ServiceProvider {

  ChromeService getChromeService();

  ExploreService getExploreService();

  HashtagService getHashtagService();

  HomeService getHomeService();

  ImageService getImageService();

  MyService getMyService();

  NotificationService getNotificationService();

  PostService getPostService();

  SearchService getSearchService();

  TopicService getTopicService();

  UserService getUserService();

}
