package services;

import core.RequestContext;
import core.RequestParameter;
import core.Query;
import entities.QueryResult;
import entities.PostType;
import entities.PublishStatus;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;

@Singleton
public class ExploreServiceImpl implements ExploreService {

  private PostService postService;

  @Inject
  public ExploreServiceImpl(PostService postService) {
    this.postService = postService;
  }

  @Override
  public CompletableFuture<QueryResult> queryExploreFeed(RequestContext context, Query query) {
    query.put(RequestParameter.POST_TYPE, PostType.PROMPT);
    query.put(RequestParameter.PUBLISH_STATUS, PublishStatus.PUBLISHED);
    return postService.queryPosts(context, query);
  }

}
