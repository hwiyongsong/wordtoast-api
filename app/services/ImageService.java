package services;

import com.google.inject.ImplementedBy;
import core.RequestContext;
import core.RequestForm;
import entities.Image;

import java.util.concurrent.CompletableFuture;

@ImplementedBy(ImageServiceImpl.class)
public interface ImageService {

  CompletableFuture<Image> getImageById(RequestContext context, String imageId);

  CompletableFuture<Image> createImage(RequestContext context, RequestForm form);

  CompletableFuture<Image> createImage(RequestContext context, String sourceUrl);

}
