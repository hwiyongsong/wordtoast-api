package services;

import akka.Done;
import com.google.inject.ImplementedBy;

import java.util.concurrent.CompletableFuture;

@ImplementedBy(CacheServiceImpl.class)
public interface CacheService {

  CompletableFuture<Object> get(String key);

  CompletableFuture<Done> set(String key, Object value);

  CompletableFuture<Done> set(String key, Object value, int expiryInSeconds);

  CompletableFuture<Done> remove(String key);

}
