package services;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ServiceProviderImpl implements ServiceProvider {

  private ChromeService chromeService;
  private ExploreService exploreService;
  private HashtagService hashtagService;
  private HomeService homeService;
  private ImageService imageService;
  private MyService myService;
  private NotificationService notificationService;
  private PostService postService;
  private SearchService searchService;
  private TopicService topicService;
  private UserService userService;

  @Inject
  public ServiceProviderImpl(ChromeService chromeService,
                             ExploreService exploreService,
                             HashtagService hashtagService,
                             HomeService homeService,
                             ImageService imageService,
                             MyService myService,
                             NotificationService notificationService,
                             PostService postService,
                             SearchService searchService,
                             TopicService topicService,
                             UserService userService) {
    this.chromeService = chromeService;
    this.exploreService = exploreService;
    this.hashtagService = hashtagService;
    this.homeService = homeService;
    this.imageService = imageService;
    this.myService = myService;
    this.notificationService = notificationService;
    this.postService = postService;
    this.searchService = searchService;
    this.topicService = topicService;
    this.userService = userService;
  }

  @Override
  public ChromeService getChromeService() {
    return chromeService;
  }

  @Override
  public ExploreService getExploreService() {
    return exploreService;
  }

  @Override
  public HashtagService getHashtagService() {
    return hashtagService;
  }

  @Override
  public HomeService getHomeService() {
    return homeService;
  }

  @Override
  public ImageService getImageService() {
    return imageService;
  }

  @Override
  public MyService getMyService() {
    return myService;
  }

  @Override
  public NotificationService getNotificationService() {
    return notificationService;
  }

  @Override
  public PostService getPostService() {
    return postService;
  }

  @Override
  public SearchService getSearchService() {
    return searchService;
  }

  @Override
  public TopicService getTopicService() {
    return topicService;
  }

  @Override
  public UserService getUserService() {
    return userService;
  }

}
