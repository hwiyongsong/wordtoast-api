package services;

import com.google.inject.ImplementedBy;
import core.RequestContext;
import core.Query;
import entities.ChromeHeader;

import java.util.concurrent.CompletableFuture;

@ImplementedBy(ChromeServiceImpl.class)
public interface ChromeService {

  CompletableFuture<ChromeHeader> getChromeHeader(RequestContext context, Query query);

}
