package services;

import com.google.inject.ImplementedBy;
import core.Query;
import core.RequestContext;
import entities.QueryResult;
import entities.Topic;
import entities.TopicFollow;

import java.util.concurrent.CompletableFuture;

@ImplementedBy(TopicServiceImpl.class)
public interface TopicService {

  CompletableFuture<QueryResult> queryTopics(RequestContext context, Query query);

  CompletableFuture<Topic> getTopicById(RequestContext context, String topicId);

  CompletableFuture<TopicFollow> createTopicFollow(RequestContext context, String topicId);

  CompletableFuture<Void> removeTopicFollow(RequestContext context, String topicId);

}
