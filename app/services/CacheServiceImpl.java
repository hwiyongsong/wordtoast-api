package services;

import akka.Done;
import play.cache.AsyncCacheApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;

@Singleton
public class CacheServiceImpl implements CacheService {

  public static final int FIVE_MINUTES_IN_SECONDS = 5 * 60;

  private AsyncCacheApi cache;

  @Inject
  public CacheServiceImpl(AsyncCacheApi cache) {
    this.cache = cache;
  }

  @Override
  public CompletableFuture<Object> get(String key) {
    return cache.get(key).toCompletableFuture().thenApply(optional -> optional.orElse(null));
  }

  @Override
  public CompletableFuture<Done> set(String key, Object value) {
    return set(key, value, FIVE_MINUTES_IN_SECONDS);
  }

  @Override
  public CompletableFuture<Done> set(String key, Object value, int expiryInSeconds) {
    return cache.set(key, value, expiryInSeconds).toCompletableFuture();
  }

  @Override
  public CompletableFuture<Done> remove(String key) {
    return cache.remove(key).toCompletableFuture();
  }

}
