package services;

import com.google.inject.ImplementedBy;
import core.Query;
import core.RequestContext;
import entities.FriendRequest;
import entities.Notification;
import entities.PostComment;
import entities.PostReaction;
import entities.QueryResult;
import entities.UserFollow;

import java.util.concurrent.CompletableFuture;

@ImplementedBy(NotificationServiceImpl.class)
public interface NotificationService {

  CompletableFuture<QueryResult> queryNotifications(RequestContext context, String recipientId, Query query);

  CompletableFuture<Notification> getNotificationById(RequestContext context, String notificationId);

  CompletableFuture<Void> removeNotification(RequestContext context, String notificationId);

  CompletableFuture<Integer> countUnreadNotifications(RequestContext context);

  CompletableFuture<Integer> markNotificationsAsRead(RequestContext context);

  CompletableFuture<Notification> createFriendRequestCreateNotification(RequestContext context, FriendRequest friendRequest);

  CompletableFuture<Notification> createFriendRequestAcceptNotification(RequestContext context, FriendRequest friendRequest);

  CompletableFuture<Notification> createUserFollowNotification(RequestContext context, UserFollow userFollow);

  CompletableFuture<Notification> createPostReactionNotification(RequestContext context, PostReaction postReaction);

  CompletableFuture<Notification> createPostCommentNotification(RequestContext context, PostComment postComment);

}
