package services;

import core.Query;
import core.RequestContext;
import core.RequestParameter;
import entities.QueryResult;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;

@Singleton
public class MyServiceImpl implements MyService {

  private NotificationService notificationService;
  private PostService postService;
  private UserService userService;

  @Inject
  public MyServiceImpl(NotificationService notificationService,
                       PostService postService,
                       UserService userService) {
    this.notificationService = notificationService;
    this.postService = postService;
    this.userService = userService;
  }

  @Override
  public CompletableFuture<QueryResult> queryMyPosts(RequestContext context, Query query) {
    query.put(RequestParameter.USER_ID, context.getCurrentUserId());
    return postService.queryPosts(context, query);
  }

  @Override
  public CompletableFuture<QueryResult> queryMyBookmarks(RequestContext context, Query query) {
    query.put(RequestParameter.USER_ID, context.getCurrentUserId());
    return postService.queryPostBookmarks(context, query);
  }

  @Override
  public CompletableFuture<QueryResult> queryMyFollowers(RequestContext context, Query query) {
    return userService.queryUserFollowers(context, context.getCurrentUserId(), query);
  }

  @Override
  public CompletableFuture<QueryResult> queryMyFollowings(RequestContext context, Query query) {
    return userService.queryUserFollowings(context, context.getCurrentUserId(), query);
  }

  @Override
  public CompletableFuture<QueryResult> queryMyFriends(RequestContext context, Query query) {
    return userService.queryUserFriends(context, context.getCurrentUserId(), query);
  }

  @Override
  public CompletableFuture<QueryResult> queryMyNotifications(RequestContext context, Query query) {
    return notificationService.queryNotifications(context, context.getCurrentUserId(), query);
  }

  @Override
  public CompletableFuture<Integer> countMyUnreadNotifications(RequestContext context) {
    return notificationService.countUnreadNotifications(context);
  }

  @Override
  public CompletableFuture<Integer> markMyNotificationsAsRead(RequestContext context) {
    return notificationService.markNotificationsAsRead(context);
  }

}
