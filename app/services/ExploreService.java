package services;

import com.google.inject.ImplementedBy;
import core.RequestContext;
import core.Query;
import entities.QueryResult;

import java.util.concurrent.CompletableFuture;

@ImplementedBy(ExploreServiceImpl.class)
public interface ExploreService {

  CompletableFuture<QueryResult> queryExploreFeed(RequestContext context, Query query);

}
