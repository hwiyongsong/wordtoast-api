package services;

import com.google.inject.ImplementedBy;
import core.Query;
import core.RequestContext;
import core.RequestForm;
import entities.Post;
import entities.PostBlock;
import entities.PostBookmark;
import entities.PostComment;
import entities.PostReaction;
import entities.QueryResult;
import entities.ReactionType;

import java.util.concurrent.CompletableFuture;

@ImplementedBy(PostServiceImpl.class)
public interface PostService {

  /* POSTS */

  CompletableFuture<QueryResult> queryPosts(RequestContext context, Query query);

  CompletableFuture<Post> getPostById(RequestContext context, String postId);

  CompletableFuture<Post> createPost(RequestContext context, RequestForm form);

  CompletableFuture<Post> updatePost(RequestContext context, String postId, RequestForm form);

  CompletableFuture<Void> removePost(RequestContext context, String postId);

  /* POST REACTIONS */

  CompletableFuture<QueryResult> queryPostReactions(RequestContext context, String postId, Query query);

  CompletableFuture<PostReaction> createPostReaction(RequestContext context, String postId, ReactionType reactionType);

  CompletableFuture<Void> removePostReaction(RequestContext context, String postId);

  /* POST COMMENTS */

  CompletableFuture<QueryResult> queryPostComments(RequestContext context, String postId, Query query);

  CompletableFuture<PostComment> createPostComment(RequestContext context, String postId, RequestForm form);

  CompletableFuture<PostComment> updatePostComment(RequestContext context, String postCommentId, RequestForm form);

  CompletableFuture<Void> removePostComment(RequestContext context, String postCommentId);

  /* POST BLOCKS */

  CompletableFuture<PostBlock> getPostBlock(RequestContext context, String postId);

  CompletableFuture<PostBlock> createPostBlock(RequestContext context, String postId);

  CompletableFuture<Void> removePostBlock(RequestContext context, String postId);

  /* POST BOOKMARKS */

  CompletableFuture<QueryResult> queryPostBookmarks(RequestContext context, Query query);

  CompletableFuture<PostBookmark> getPostBookmark(RequestContext context, String postId);

  CompletableFuture<PostBookmark> createPostBookmark(RequestContext context, String postId);

  CompletableFuture<Void> removePostBookmark(RequestContext context, String postBookmarkId);

}
