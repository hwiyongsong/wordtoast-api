package services;

import com.google.inject.ImplementedBy;
import core.AccessToken;
import core.Query;
import core.RequestContext;
import core.RequestForm;
import entities.FriendRequest;
import entities.QueryResult;
import entities.User;
import entities.UserBlock;
import entities.UserFollow;

import java.util.concurrent.CompletableFuture;

@ImplementedBy(UserServiceImpl.class)
public interface UserService {

  /**** USERS ****/

  CompletableFuture<QueryResult> queryUsers(RequestContext context, Query query);

  CompletableFuture<User> getUserById(RequestContext context, String userId);

  CompletableFuture<User> getUserByAccessToken(AccessToken accessToken);

  CompletableFuture<User> createUser(RequestContext context, RequestForm form);

  CompletableFuture<User> updateUser(RequestContext context, String userId, RequestForm form);

  /**** USER POSTS ****/

  CompletableFuture<QueryResult> queryUserPosts(RequestContext context, String userId, Query query);

  /**** USER BOOKMARKS ****/

  CompletableFuture<QueryResult> queryUserBookmarks(RequestContext context, String userId, Query query);

  /**** USER FOLLOWS ****/

  CompletableFuture<QueryResult> queryUserFollowings(RequestContext context, String userId, Query query);

  CompletableFuture<QueryResult> queryUserFollowers(RequestContext context, String userId, Query query);

  CompletableFuture<UserFollow> createUserFollow(RequestContext context, String userId);

  CompletableFuture<Void> removeUserFollow(RequestContext context, String userId);

  /**** USER FRIENDS ****/

  CompletableFuture<QueryResult> queryUserFriends(RequestContext context, String userId, Query query);

  /**** FRIEND REQUESTS ****/

  CompletableFuture<FriendRequest> createFriendRequest(RequestContext context, RequestForm form);

  CompletableFuture<FriendRequest> getFriendRequestById(RequestContext context, String friendRequestId);

  CompletableFuture<FriendRequest> updateFriendRequest(RequestContext context, String friendRequestId, RequestForm form);

  CompletableFuture<Void> removeFriendRequest(RequestContext context, String friendRequestId);

  /**** USER BLOCKS ****/

  CompletableFuture<UserBlock> createUserBlock(RequestContext context, String userId);

  CompletableFuture<Void> removeUserBlock(RequestContext context, String userId);

}
