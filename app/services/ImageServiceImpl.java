package services;

import core.RequestContext;
import core.RequestForm;
import core.RequestParameter;
import database.ImageDao;
import entities.Image;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;

@Singleton
public class ImageServiceImpl implements ImageService {

  private ImageDao imageDao;

  @Inject
  public ImageServiceImpl(ImageDao imageDao) {
    this.imageDao = imageDao;
  }

  @Override
  public CompletableFuture<Image> getImageById(RequestContext context, String imageId) {
    return imageDao.getImageById(context, imageId);
  }

  @Override
  public CompletableFuture<Image> createImage(RequestContext context, RequestForm form) {
    String sourceUrl = form.getString(RequestParameter.SOURCE_URL);
    String bucket = form.getString(RequestParameter.BUCKET);
    String filename = form.getString(RequestParameter.FILENAME);

    Image image = new Image();
    image.setSourceUrl(sourceUrl);
    image.setBucket(bucket);
    image.setFilename(filename);
    image.setCreatedBy(context.getCurrentUser());

    return imageDao.saveImage(context, image);
  }

  @Override
  public CompletableFuture<Image> createImage(RequestContext context, String sourceUrl) {
    Image image = new Image();
    image.setSourceUrl(sourceUrl);
    image.setCreatedBy(context.getCurrentUser());

    return imageDao.saveImage(context, image);
  }

}
