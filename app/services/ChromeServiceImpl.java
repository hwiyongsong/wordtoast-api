package services;

import core.RequestContext;
import core.Query;
import database.TopicDao;
import entities.ChromeHeader;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;

@Singleton
public class ChromeServiceImpl implements ChromeService {

  private TopicDao topicDao;

  @Inject
  public ChromeServiceImpl(TopicDao topicDao) {
    this.topicDao = topicDao;
  }

  @Override
  public CompletableFuture<ChromeHeader> getChromeHeader(RequestContext context, Query query) {
    return topicDao.queryTopics(context, query).thenApplyAsync(topics -> {
      ChromeHeader chromeHeader = new ChromeHeader();
      chromeHeader.addNavItems(topics);
      return chromeHeader;
    });
  }

}
