package services;

import com.google.common.collect.Lists;
import core.Query;
import core.RequestContext;
import core.RequestForm;
import core.RequestParameter;
import database.PostDao;
import entities.Hashtag;
import entities.Image;
import entities.Post;
import entities.PostBlock;
import entities.PostBookmark;
import entities.PostComment;
import entities.PostReaction;
import entities.PostType;
import entities.PublishStatus;
import entities.QueryResult;
import entities.ReactionType;
import entities.Topic;
import entities.Visibility;
import utils.StringUtils;
import utils.TextUtils;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Singleton
public class PostServiceImpl implements PostService {

  private PostDao postDao;
  private HashtagService hashtagService;
  private ImageService imageService;
  private NotificationService notificationService;
  private TopicService topicService;

  @Inject
  public PostServiceImpl(PostDao postDao,
                         HashtagService hashtagService,
                         ImageService imageService,
                         NotificationService notificationService,
                         TopicService topicService) {
    this.postDao = postDao;
    this.hashtagService = hashtagService;
    this.imageService = imageService;
    this.notificationService = notificationService;
    this.topicService = topicService;
  }

  /* POSTS */

  @Override
  public CompletableFuture<QueryResult> queryPosts(RequestContext context, Query query) {
    return postDao.queryPosts(context, query).thenApplyAsync(posts -> new QueryResult(query, posts));
  }

  @Override
  public CompletableFuture<Post> getPostById(RequestContext context, String postId) {
    return postDao.getPostById(context, postId);
  }

  @Override
  public CompletableFuture<Post> createPost(RequestContext context, RequestForm form) {
    String title = form.getString(RequestParameter.TITLE);
    String body = form.getString(RequestParameter.BODY);
    String coverImageId = form.getString(RequestParameter.COVER_IMAGE_ID);
    Visibility visibility = form.getEnum(Visibility.class, RequestParameter.VISIBILITY);
    PublishStatus publishStatus = form.getEnum(PublishStatus.class, RequestParameter.PUBLISH_STATUS);
    String topicId = form.getString(RequestParameter.TOPIC_ID);
    String promptId = form.getString(RequestParameter.PROMPT_ID);
    String slug = form.getString(RequestParameter.SLUG);
    String hashtagText = form.getString(RequestParameter.HASHTAG_TEXT);
    List<String> hashtagNames = TextUtils.parseHashtagNames(hashtagText);

    CompletableFuture<Image> coverImageFuture = StringUtils.isNoneBlank(coverImageId) ? imageService.getImageById(context, coverImageId) : CompletableFuture.completedFuture(null);
    CompletableFuture<Topic> topicFuture = StringUtils.isNotBlank(topicId) ? topicService.getTopicById(context, topicId) : CompletableFuture.completedFuture(null);
    CompletableFuture<Post> promptFuture = StringUtils.isNotBlank(promptId) ? getPostById(context, promptId) : CompletableFuture.completedFuture(null);
    CompletableFuture<List<Hashtag>> hashtagsFuture = (publishStatus == PublishStatus.PUBLISHED) ? hashtagService.resolveHashtags(context, hashtagNames) : CompletableFuture.completedFuture(Lists.newArrayList());

    return CompletableFuture.allOf(coverImageFuture, topicFuture, promptFuture, hashtagsFuture).thenComposeAsync(ignored -> {
      Post prompt = promptFuture.join();
      Topic topic = topicFuture.join();

      Post post = new Post();
      post.setPostType(prompt == null && topic != null && topic.getTopicId().equals("5oG4aUWauW2Rd7fiA5JgAr") ? PostType.PROMPT : PostType.WRITING);
      post.setTitle(title);
      post.setBody(body);
      post.setCoverImage(coverImageFuture.join());
      post.setHashtagText(hashtagText);
      post.setSlug(slug);
      post.setVisibility(visibility);
      post.setPublishStatus(publishStatus);
      post.setTopic(topic);
      post.setPrompt(prompt);
      post.setHashtags(hashtagsFuture.join());
      post.setCreatedBy(context.getCurrentUser());

      return postDao.savePost(context, post);
    });
  }

  @Override
  public CompletableFuture<Post> updatePost(RequestContext context, String postId, RequestForm form) {

    String title = form.getString(RequestParameter.TITLE);
    String body = form.getString(RequestParameter.BODY);
    String coverImageId = form.getString(RequestParameter.COVER_IMAGE_ID);
    String topicId = form.getString(RequestParameter.TOPIC_ID);
    String promptId = form.getString(RequestParameter.PROMPT_ID);
    Visibility visibility = form.getEnum(Visibility.class, RequestParameter.VISIBILITY);
    PublishStatus publishStatus = form.getEnum(PublishStatus.class, RequestParameter.PUBLISH_STATUS);
    String slug = form.getString(RequestParameter.SLUG);
    String hashtagText = form.getString(RequestParameter.HASHTAG_TEXT);
    List<String> hashtagNames = TextUtils.parseHashtagNames(hashtagText);

    CompletableFuture<Post> postFuture = getPostById(context, postId);
    CompletableFuture<Image> coverImageFuture = StringUtils.isNoneBlank(coverImageId) ? imageService.getImageById(context, coverImageId) : CompletableFuture.completedFuture(null);
    CompletableFuture<Topic> topicFuture = StringUtils.isNotBlank(topicId) ? topicService.getTopicById(context, topicId) : CompletableFuture.completedFuture(null);
    CompletableFuture<Post> promptFuture = StringUtils.isNotBlank(promptId) ? getPostById(context, promptId) : CompletableFuture.completedFuture(null);
    CompletableFuture<List<Hashtag>> hashtagsFuture = (publishStatus == PublishStatus.PUBLISHED) ? hashtagService.resolveHashtags(context, hashtagNames) : CompletableFuture.completedFuture(Lists.newArrayList());

    return CompletableFuture.allOf(postFuture, promptFuture, coverImageFuture, topicFuture, hashtagsFuture).thenComposeAsync(ignored -> {
      Post prompt = promptFuture.join();
      Topic topic = topicFuture.join();

      Post post = postFuture.join();
      post.setPostType(prompt == null && topic != null && topic.getTopicId().equals("5oG4aUWauW2Rd7fiA5JgAr") ? PostType.PROMPT : PostType.WRITING);
      post.setTitle(title);
      post.setBody(body);
      post.setCoverImage(coverImageFuture.join());
      post.setHashtagText(hashtagText);
      post.setSlug(slug);
      post.setVisibility(visibility);
      post.setPublishStatus(publishStatus);
      post.setTopic(topicFuture.join());
      post.setPrompt(prompt);
      post.setHashtags(hashtagsFuture.join());

      return postDao.savePost(context, post);
    });
  }

  @Override
  public CompletableFuture<Void> removePost(RequestContext context, String postId) {
    return getPostById(context, postId).thenComposeAsync(post -> {
      return postDao.removePost(context, post);
    });
  }

  /* POST REACTIONS */

  @Override
  public CompletableFuture<QueryResult> queryPostReactions(RequestContext context, String postId, Query query) {
    return postDao.queryPostReactions(context, postId, query).thenApplyAsync(posts -> new QueryResult(query, posts));
  }

  @Override
  public CompletableFuture<PostReaction> createPostReaction(RequestContext context, String postId, ReactionType reactionType) {
    return getPostById(context, postId).thenComposeAsync(post -> {
      PostReaction postReaction = new PostReaction();
      postReaction.setPost(post);
      postReaction.setReactionType(reactionType);
      postReaction.setCreatedBy(context.getCurrentUser());

      return postDao.savePostReaction(context, postReaction).thenApplyAsync(persistedPostReaction -> {
        notificationService.createPostReactionNotification(context, persistedPostReaction);
        return persistedPostReaction;
      });
    });
  }

  @Override
  public CompletableFuture<Void> removePostReaction(RequestContext context, String postId) {
    return postDao.getPostReaction(context, postId).thenComposeAsync(postReaction -> {
      return postDao.removePostReaction(context, postReaction);
    });
  }

  /* POST COMMENTS */

  @Override
  public CompletableFuture<QueryResult> queryPostComments(RequestContext context, String postId, Query query) {
    return postDao.queryPostComments(context, postId, query).thenApplyAsync(posts -> new QueryResult(query, posts));
  }

  @Override
  public CompletableFuture<PostComment> createPostComment(RequestContext context, String postId, RequestForm form) {
    String body = form.getString(RequestParameter.BODY);

    return getPostById(context, postId).thenComposeAsync(post -> {
      PostComment postComment = new PostComment();
      postComment.setPost(post);
      postComment.setBody(body);
      postComment.setCreatedBy(context.getCurrentUser());

      return postDao.savePostComment(context, postComment).thenApplyAsync(persistedPostComment -> {
        notificationService.createPostCommentNotification(context, persistedPostComment);
        return persistedPostComment;
      });
    });
  }

  @Override
  public CompletableFuture<PostComment> updatePostComment(RequestContext context, String postCommentId, RequestForm form) {
    String body = form.getString(RequestParameter.BODY);

    return postDao.getPostCommentById(context, postCommentId).thenComposeAsync(postComment -> {
      postComment.setBody(body);
      return postDao.savePostComment(context, postComment);
    });
  }

  @Override
  public CompletableFuture<Void> removePostComment(RequestContext context, String postCommentId) {
    return postDao.getPostCommentById(context, postCommentId).thenComposeAsync(postComment -> {
      return postDao.removePostComment(context, postComment);
    });
  }

  /* POST BLOCKS */

  @Override
  public CompletableFuture<PostBlock> getPostBlock(RequestContext context, String postId) {
    return postDao.getPostBlock(context, postId);
  }

  @Override
  public CompletableFuture<PostBlock> createPostBlock(RequestContext context, String postId) {
    return getPostById(context, postId).thenComposeAsync(post -> {
      PostBlock postBlock = new PostBlock();
      postBlock.setPost(post);
      postBlock.setCreatedBy(context.getCurrentUser());
      return postDao.savePostBlock(context, postBlock);
    });
  }

  @Override
  public CompletableFuture<Void> removePostBlock(RequestContext context, String postId) {
    return getPostBlock(context, postId).thenComposeAsync(postBlock -> {
      return postDao.removePostBlock(context, postBlock);
    });
  }

  /* POST BOOKMARKS */

  @Override
  public CompletableFuture<QueryResult> queryPostBookmarks(RequestContext context, Query query) {
    return postDao.queryPostBookmarks(context, query).thenApplyAsync(posts -> new QueryResult(query, posts));
  }

  @Override
  public CompletableFuture<PostBookmark> getPostBookmark(RequestContext context, String postId) {
    return postDao.getPostBookmark(context, postId, context.getCurrentUserId());
  }

  @Override
  public CompletableFuture<PostBookmark> createPostBookmark(RequestContext context, String postId) {
    return getPostById(context, postId).thenComposeAsync(post -> {
      PostBookmark postBookmark = new PostBookmark();
      postBookmark.setPost(post);
      postBookmark.setCreatedBy(context.getCurrentUser());
      return postDao.savePostBookmark(context, postBookmark);
    });
  }

  @Override
  public CompletableFuture<Void> removePostBookmark(RequestContext context, String postId) {
    return postDao.getPostBookmark(context, postId, context.getCurrentUserId()).thenComposeAsync(postBookmark -> {
      return postDao.removePostBookmark(context, postBookmark);
    });
  }

}
