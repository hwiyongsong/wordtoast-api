import com.google.common.collect.Lists;
import play.filters.cors.CORSFilter;
import play.http.HttpFilters;
import play.mvc.EssentialFilter;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class Filters implements HttpFilters {

  @Inject
  private CORSFilter corsFilter;

  @Override
  public List<EssentialFilter> getFilters() {
    return Lists.newArrayList(corsFilter.asJava());
  }
}
