package clients.amazon;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.typesafe.config.Config;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Singleton
public class AmazonS3ClientImpl implements AmazonS3Client {

  private AmazonS3 s3Client;
  private String bucketName;
  private String regionName;

  @Inject
  public AmazonS3ClientImpl(Config config) {
    String accessKey = config.getString("aws.accessKey");
    String secretKey = config.getString("aws.secretKey");
    this.bucketName = config.getString("aws.bucketName");
    this.regionName = config.getString("aws.region");

    BasicAWSCredentials creds = new BasicAWSCredentials(accessKey, secretKey);
    this.s3Client = AmazonS3ClientBuilder.standard()
                                         .withCredentials(new AWSStaticCredentialsProvider(creds))
                                         .withRegion(Regions.fromName(this.regionName))
                                         .build();
  }

  @Override
  public String save(Bucket bucket, String key, File file) throws Exception {
    String bucketPath = resolveFolderPath(bucket);

    PutObjectRequest putRequest = new PutObjectRequest(bucketPath, key, file);
    s3Client.putObject(putRequest);

    return resolveFullPath(bucket, key);
  }

  @Override
  public String save(Bucket bucket, String key, byte[] buffer) throws IOException {
    InputStream inputStream = new ByteArrayInputStream(buffer);

    ObjectMetadata meta = new ObjectMetadata();
    meta.setContentType("image/jpeg");
    meta.setContentLength(buffer.length);

    String bucketPath = resolveFolderPath(bucket);

    PutObjectRequest putRequest = new PutObjectRequest(bucketPath, key, inputStream, meta);
    s3Client.putObject(putRequest);

    return resolveFullPath(bucket, key);
  }

  private String resolveFullPath(Bucket mediaType, String fileKey) {
    return "https://s3.amazonaws.com/" + resolveFolderPath(mediaType) + "/" + fileKey;
  }

  private String resolveFolderPath(Bucket mediaType) {
    return bucketName + "/" + mediaType.toString();
  }

}
