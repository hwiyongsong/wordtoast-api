package clients.amazon;

import com.google.inject.ImplementedBy;

import java.io.File;
import java.io.IOException;

@ImplementedBy(AmazonS3ClientImpl.class)
public interface AmazonS3Client {

  String save(Bucket bucket, String key, File file) throws Exception;

  String save(Bucket bucket, String key, byte[] buffer) throws IOException;

}
