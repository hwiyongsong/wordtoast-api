package clients.amazon;

public enum Bucket {

  CONTENT_IMAGES("content/images");

  private String folder;

  private Bucket(String folder) {
    this.folder = folder;
  }

  @Override
  public String toString() {
    return folder;
  }

}
