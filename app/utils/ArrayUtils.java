package utils;

import java.util.Arrays;
import java.util.List;

public class ArrayUtils extends org.apache.commons.lang3.ArrayUtils {

  @SuppressWarnings("unchecked")
  public static <T> List<T> asList(T... array) {
    return Arrays.asList(array);
  }

  public static <T> T getFirst(T[] array) {
    return (array != null && array.length > 0) ? array[0] : null;
  }

}