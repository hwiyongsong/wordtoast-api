package utils;

import entities.EntityType;

public class UrnUtils {

  public static String createUrn(EntityType type) {
    return createUrn(type, ObjectUtils.generateUuid());
  }

  public static String createUrn(EntityType type, Object value) {
    return "urn:" + type.getKey() + ":" + value.toString();
  }

  public static String createUrn(EntityType type, String subtype, Object value) {
    return "urn:" + type.getKey() + ":" + subtype.toString() + ":" + value.toString();
  }

  public static String createUrn(String parentUrn, Object value) {
    return parentUrn + ":" + value.toString();
  }

  public static EntityType getType(String urn) {
    return EntityType.of(urn.split(":")[1]);
  }

  public static String getSubtype(String urn) {
    return urn.split(":")[2];
  }

  public static String getValue(String urn) {
    return StringUtils.substringAfterLast(urn, ":");
  }

  public static boolean isUrn(String str) {
    return StringUtils.startsWith(str, "urn:");
  }

}
