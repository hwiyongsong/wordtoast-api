package utils;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

public class PhoneUtils {

  private static PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

  public static String formatToE164(String phoneNumberStr) {
    try {
      Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(phoneNumberStr, "US");
      return phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

}
