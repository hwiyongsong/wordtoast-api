package utils;

import com.devskiller.friendly_id.FriendlyId;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ObjectUtils extends org.apache.commons.lang3.ObjectUtils {

  public static String generateUuid() {
    return FriendlyId.createFriendlyId();
  }

  public static <T> T silently(T value) {
    try {
      return value;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public static int compare(Object object1, Object object2) {
    return new CompareToBuilder().append(object1, object2).toComparison();
  }

  public static String reflectionToString(Object obj) {
    return ToStringBuilder.reflectionToString(obj, ToStringStyle.SHORT_PREFIX_STYLE);
  }

}
