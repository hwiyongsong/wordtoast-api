package utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextUtils {

  private static final Pattern HASHTAG_PATTERN = Pattern.compile("#(\\S+)");

  public static List<String> parseHashtagNames(String text) {
    if (StringUtils.isEmpty(text)) {
      return Lists.newArrayList();
    }

    Set<String> hashtagNames = Sets.newTreeSet();

    text = text.replaceAll("\\<.*?\\>", " ");
    Matcher matcher = HASHTAG_PATTERN.matcher(text);

    while (matcher.find()) {
      hashtagNames.add(matcher.group(1));
    }

    return Lists.newArrayList(hashtagNames);
  }

}
