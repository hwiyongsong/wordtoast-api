package utils;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class FutureUtils {

  public static <T> CompletableFuture<List<T>> allOf(List<CompletableFuture<T>> futures) {
    return CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()]))
                            .thenApply(ignored -> futures.stream()
                                                         .map(future -> future.join())
                                                         .collect(Collectors.toList()));
  }

}
