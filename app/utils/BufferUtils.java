package utils;

import java.util.Base64;

public class BufferUtils {

  public static boolean isBase64(String str) {
    return StringUtils.startsWith(str, "data:") && StringUtils.contains(str, "base64,");
  }
  
  public static byte[] decodeBase64(String base64) {
    if (StringUtils.contains(base64, "base64,")) {
      base64 = StringUtils.substringAfter(base64, "base64,");
    }

    return Base64.getDecoder().decode(base64);
  }

}