package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils {

  public static JsonNode toJson(Object value) {
    ObjectMapper objectMapper = new ObjectMapper();
    return objectMapper.valueToTree(value);
  }

  public static JsonNode parse(String jsonContent) {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.readTree(jsonContent);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public static String prettyPrint(JsonNode json) {
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

}