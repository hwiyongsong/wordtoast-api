package utils;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtils extends org.apache.commons.lang3.time.DateUtils {

  private static final DateTimeFormatter ISO_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssxxx");

  public static String formatIso(OffsetDateTime dateTime) {
    return dateTime.format(ISO_DATE_TIME_FORMATTER);
  }

}
