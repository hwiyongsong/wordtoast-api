package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.stream.Stream;

public class StringUtils extends org.apache.commons.lang3.StringUtils {

  public static boolean notEquals(String str1, String str2) {
    return !equals(str1, str2);
  }

  public static Stream<String> splitAsStream(String str, String delimiter) {
    String[] tokens = StringUtils.isNotBlank(str) ? split(str, delimiter) : new String[0];
    return Arrays.stream(tokens).map(token -> token.trim());
  }

}
