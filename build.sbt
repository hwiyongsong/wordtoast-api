lazy val root = (project in file("."))
  .enablePlugins(PlayJava)
  .settings(
    name := """wordtoast-api""",
    organization := "com.buzzingsynapses",
    version := "1.0-SNAPSHOT",
    scalaVersion := "2.13.1",
    libraryDependencies ++= Seq(
      caffeine,
      guice,
      javaJpa,
      javaWs,
      "com.amazonaws" % "aws-java-sdk-s3" % "1.11.699",
      "com.devskiller.friendly-id" % "friendly-id" % "1.1.0",
      "com.googlecode.libphonenumber" % "libphonenumber" % "8.11.1",
      "javax.ws.rs" % "javax.ws.rs-api" % "2.1.1",
      "org.apache.commons" % "commons-collections4" % "4.4",
      "org.apache.commons" % "commons-lang3" % "3.9",
      "mysql" % "mysql-connector-java" % "8.0.18",
      "org.hibernate" % "hibernate-core" % "5.4.10.Final",
      "org.awaitility" % "awaitility" % "4.0.1" % "test",
      "org.assertj" % "assertj-core" % "3.14.0" % "test",
    ),
    scalacOptions ++= List("-encoding", "utf8", "-deprecation", "-feature", "-unchecked"),
    javacOptions ++= List("-Xlint:unchecked", "-Xlint:deprecation", "-Werror"),
    PlayKeys.externalizeResourcesExcludes += baseDirectory.value / "conf" / "META-INF" / "persistence.xml"
  )
