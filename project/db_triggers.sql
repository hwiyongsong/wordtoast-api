DROP TRIGGER IF EXISTS trUserFriendAfterInsert;

delimiter |

CREATE TRIGGER trUserFriendAfterInsert
AFTER INSERT
   ON UserFriend FOR EACH ROW

BEGIN

	UPDATE User u
    SET
		u.FriendCount = u.FriendCount + 1
	WHERE u.UserId = NEW.UserId;

END
|

delimiter ;

---

DROP TRIGGER IF EXISTS trUserFriendAfterDelete;

delimiter |

CREATE TRIGGER trUserFriendAfterDelete
AFTER DELETE
   ON UserFriend FOR EACH ROW

BEGIN

	UPDATE User u
    SET
        u.FriendCount = CASE WHEN u.FriendCount > 0 THEN u.FriendCount - 1 ELSE 0 END
	WHERE u.UserId = OLD.UserId;

END
|

delimiter ;

----

DROP TRIGGER IF EXISTS trUserFollowAfterInsert;

delimiter |

CREATE TRIGGER trUserFollowAfterInsert
AFTER INSERT
   ON UserFollow FOR EACH ROW

BEGIN

	UPDATE User u
    SET
		u.FollowerCount = u.FollowerCount + 1
	WHERE u.UserId = NEW.UserId;

	UPDATE User u
    SET
		u.FollowingCount = u.FollowingCount + 1
	WHERE u.UserId = NEW.CreatedBy;

END
|

delimiter ;

---

DROP TRIGGER IF EXISTS trUserFollowAfterDelete;

delimiter |

CREATE TRIGGER trUserFollowAfterDelete
AFTER DELETE
   ON UserFollow FOR EACH ROW

BEGIN

	UPDATE User u
    SET
        u.FollowerCount = CASE WHEN u.FollowerCount > 0 THEN u.FollowerCount - 1 ELSE 0 END
	WHERE u.UserId = OLD.UserId;

	UPDATE User u
    SET
		u.FollowingCount = CASE WHEN u.FollowingCount > 0 THEN u.FollowingCount - 1 ELSE 0 END
	WHERE u.UserId = OLD.CreatedBy;

END
|

delimiter ;

----

DROP TRIGGER IF EXISTS trTopicFollowAfterInsert;

delimiter |

CREATE TRIGGER trTopicFollowAfterInsert
AFTER INSERT
   ON TopicFollow FOR EACH ROW

BEGIN

	UPDATE Topic t
    SET
		t.FollowerCount = t.FollowerCount + 1
	WHERE t.TopicId = NEW.TopicId;

END
|

delimiter ;

---

DROP TRIGGER IF EXISTS trTopicFollowAfterDelete;

delimiter |

CREATE TRIGGER trTopicFollowAfterDelete
AFTER DELETE
   ON TopicFollow FOR EACH ROW

BEGIN

	UPDATE Topic t
    SET
        t.FollowerCount = CASE WHEN t.FollowerCount > 0 THEN t.FollowerCount - 1 ELSE 0 END
	WHERE t.TopicId = OLD.TopicId;

END
|

delimiter ;

----

DROP TRIGGER IF EXISTS trPostReactionAfterInsert;

delimiter |

CREATE TRIGGER trPostReactionAfterInsert
AFTER INSERT
   ON PostReaction FOR EACH ROW

BEGIN

	UPDATE Post p
    SET
		p.LikeCount = IF(NEW.ReactionType = 'LIKE', p.LikeCount + 1, p.LikeCount),
		p.LoveCount = IF(NEW.ReactionType = 'LOVE', p.LoveCount + 1, p.LoveCount),
		p.HahaCount = IF(NEW.ReactionType = 'HAHA', p.HahaCount + 1, p.HahaCount),
		p.SadCount = IF(NEW.ReactionType = 'SAD', p.SadCount + 1, p.SadCount)
	WHERE p.PostId = NEW.PostId;

	UPDATE User u
	    INNER JOIN Post p ON p.CreatedBy = u.UserId
    SET
        u.ReactionPoints = u.ReactionPoints + 1
	WHERE p.PostId = NEW.PostId AND u.UserId != NEW.CreatedBy;

END
|

delimiter ;

----

DROP TRIGGER IF EXISTS trPostReactionAfterDelete;

delimiter |

CREATE TRIGGER trPostReactionAfterDelete
AFTER DELETE
   ON PostReaction FOR EACH ROW

BEGIN

	UPDATE Post p
    SET
		p.LikeCount = IF(OLD.ReactionType = 'LIKE', CASE WHEN p.LikeCount > 0 THEN p.LikeCount - 1 ELSE 0 END, p.LikeCount),
		p.LoveCount = IF(OLD.ReactionType = 'LOVE', CASE WHEN p.LoveCount > 0 THEN p.LoveCount - 1 ELSE 0 END, p.LoveCount),
		p.HahaCount = IF(OLD.ReactionType = 'HAHA', CASE WHEN p.HahaCount > 0 THEN p.HahaCount - 1 ELSE 0 END, p.HahaCount),
		p.SadCount = IF(OLD.ReactionType = 'SAD', CASE WHEN p.SadCount > 0 THEN p.SadCount - 1 ELSE 0 END, p.SadCount)
	WHERE p.PostId = OLD.PostId;

	UPDATE User u
	    INNER JOIN Post p ON p.CreatedBy = u.UserId
    SET
        u.ReactionPoints = CASE WHEN u.ReactionPoints > 0 THEN u.ReactionPoints - 1 ELSE 0 END
	WHERE p.PostId = OLD.PostId AND u.UserId != OLD.CreatedBy;

END
|

delimiter ;

----

DROP TRIGGER IF EXISTS trPostBookmarkAfterInsert;

delimiter |

CREATE TRIGGER trPostBookmarkAfterInsert
AFTER INSERT
   ON PostBookmark FOR EACH ROW

BEGIN

	UPDATE Post p
    SET
		p.BookmarkCount = p.BookmarkCount + 1
	WHERE p.PostId = NEW.PostId;

END
|

delimiter ;

----

DROP TRIGGER IF EXISTS trPostBookmarkAfterDelete;

delimiter |

CREATE TRIGGER trPostBookmarkAfterDelete
AFTER DELETE
   ON PostBookmark FOR EACH ROW

BEGIN

	UPDATE Post p
    SET
		p.BookmarkCount = CASE WHEN p.BookmarkCount > 0 THEN p.BookmarkCount - 1 ELSE 0 END
	WHERE p.PostId = OLD.PostId;

END
|

delimiter ;

----

DROP TRIGGER IF EXISTS trPostCommentAfterInsert;

delimiter |

CREATE TRIGGER trPostCommentAfterInsert
AFTER INSERT
   ON PostComment FOR EACH ROW

BEGIN

	UPDATE Post p
    SET
		p.CommentCount = p.CommentCount + 1
	WHERE p.PostId = NEW.PostId;

END
|

delimiter ;

----

DROP TRIGGER IF EXISTS trPostCommentAfterDelete;

delimiter |

CREATE TRIGGER trPostCommentAfterDelete
AFTER DELETE
   ON PostComment FOR EACH ROW

BEGIN

	UPDATE Post p
    SET
		p.CommentCount = CASE WHEN p.CommentCount > 0 THEN p.CommentCount - 1 ELSE 0 END
	WHERE p.PostId = OLD.PostId;

END
|

delimiter ;

----

DROP TRIGGER IF EXISTS trPostHashtagAfterInsert;

delimiter |

CREATE TRIGGER trPostHashtagAfterInsert
AFTER INSERT
   ON PostHashtag FOR EACH ROW

BEGIN

	UPDATE Hashtag h
    SET
		h.PostCount = h.PostCount + 1
	WHERE h.HashtagId = NEW.HashtagId;

END
|

delimiter ;

---

DROP TRIGGER IF EXISTS trPostHashtagAfterDelete;

delimiter |

CREATE TRIGGER trPostHashtagAfterDelete
AFTER DELETE
   ON PostHashtag FOR EACH ROW

BEGIN

	UPDATE Hashtag h
    SET
        h.PostCount = CASE WHEN h.PostCount > 0 THEN h.PostCount - 1 ELSE 0 END
	WHERE h.HashtagId = OLD.HashtagId;

END
|

delimiter ;

----
