CREATE DATABASE  IF NOT EXISTS `wordtoast` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `wordtoast`;
-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: localhost    Database: wordtoast
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `FriendRequest`
--

DROP TABLE IF EXISTS `FriendRequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FriendRequest` (
  `FriendRequestId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `UserId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedBy` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`FriendRequestId`),
  UNIQUE KEY `FriendRequest_FriendRequestId_UNIQUE` (`FriendRequestId`),
  UNIQUE KEY `FriendRequest_UserId_CreatedBy_UNIQUE` (`UserId`,`CreatedBy`),
  KEY `FriendRequest_CreatedBy_FK_IDX` (`CreatedBy`),
  CONSTRAINT `FriendRequest_CreatedBy_FK` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `FriendRequest_UserId_FK` FOREIGN KEY (`FriendRequestId`) REFERENCES `FriendRequest` (`UserId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Hashtag`
--

DROP TABLE IF EXISTS `Hashtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Hashtag` (
  `HashtagId` char(22) COLLATE utf8mb4_bin NOT NULL,
  `Name` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `PostCount` int unsigned NOT NULL DEFAULT '0',
  `CreatedBy` char(22) COLLATE utf8mb4_bin DEFAULT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`HashtagId`),
  UNIQUE KEY `Hashtag_HashtagId_UNIQUE` (`HashtagId`),
  UNIQUE KEY `Hashtag_Name_UNIQUE` (`Name`),
  KEY `Hashtag_CreatedBy_FK_IDX` (`CreatedBy`),
  CONSTRAINT `Hashtag_CreatedBy_FK` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserId`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HashtagFollow`
--

DROP TABLE IF EXISTS `HashtagFollow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `HashtagFollow` (
  `HashtagFollowId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `HashtagId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedBy` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`HashtagFollowId`),
  UNIQUE KEY `HashtagFollow_HashtagFollowId_UNIQUE` (`HashtagFollowId`),
  UNIQUE KEY `HashtagFollow_HashtagId_CreatedBy_UNIQUE` (`HashtagId`,`CreatedBy`),
  KEY `HashtagFollow_CreatedBy_FK_IDX` (`CreatedBy`),
  CONSTRAINT `HashtagFollow_CreatedBy_FK` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `HashtagFollow_HashtagId_FK` FOREIGN KEY (`HashtagId`) REFERENCES `Hashtag` (`HashtagId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Image`
--

DROP TABLE IF EXISTS `Image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Image` (
  `ImageId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `SourceUrl` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `Bucket` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `Filename` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `CreatedBy` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ImageId`),
  UNIQUE KEY `Image_ImageId_UNIQUE` (`ImageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Notification`
--

DROP TABLE IF EXISTS `Notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Notification` (
  `NotificationId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `NotificationType` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `RecipientId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `ActorId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `UserFollowId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `PostId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `PostReactionId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `PostCommentId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `Read` tinyint unsigned NOT NULL DEFAULT '0',
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`NotificationId`),
  UNIQUE KEY `Notification_NotificationId_UNIQUE` (`NotificationId`),
  KEY `Notification_PostId_FK_IDX` (`PostId`),
  KEY `Notification_RecipientId_FK_IDX` (`RecipientId`),
  KEY `Notification_ActorId_FK_IDX` (`ActorId`),
  KEY `Notification_PostCommentId_FK_IDX` (`PostCommentId`),
  KEY `Notification_PostReactionId_FK_IDX` (`PostReactionId`),
  KEY `Notification_UserFollowId_FK_IDX` (`UserFollowId`),
  CONSTRAINT `Notification_ActorId_FK` FOREIGN KEY (`ActorId`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `Notification_PostCommentId_FK` FOREIGN KEY (`PostCommentId`) REFERENCES `PostComment` (`PostCommentId`) ON DELETE CASCADE,
  CONSTRAINT `Notification_PostId_FK` FOREIGN KEY (`PostId`) REFERENCES `Post` (`PostId`) ON DELETE CASCADE,
  CONSTRAINT `Notification_PostReactionId_FK` FOREIGN KEY (`PostReactionId`) REFERENCES `PostReaction` (`PostReactionId`) ON DELETE CASCADE,
  CONSTRAINT `Notification_RecipientId_FK` FOREIGN KEY (`RecipientId`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `Notification_UserFollowId_FK` FOREIGN KEY (`UserFollowId`) REFERENCES `UserFollow` (`UserFollowId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Post`
--

DROP TABLE IF EXISTS `Post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Post` (
  `PostId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `PostType` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `Title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `Subtitle` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `Body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `CoverImageId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `Visibility` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT 'PROTECTED',
  `PublishStatus` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT 'DRAFT',
  `Slug` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `LikeCount` smallint unsigned NOT NULL DEFAULT '0',
  `LoveCount` smallint unsigned NOT NULL DEFAULT '0',
  `HahaCount` smallint unsigned NOT NULL DEFAULT '0',
  `SadCount` smallint unsigned NOT NULL DEFAULT '0',
  `CommentCount` smallint unsigned NOT NULL DEFAULT '0',
  `BookmarkCount` smallint unsigned NOT NULL DEFAULT '0',
  `TopicId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `PromptId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `CreatedBy` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PostId`),
  UNIQUE KEY `Post_PostId_UNIQUE` (`PostId`),
  KEY `Post_CoverImageId_FK_IDX` (`CoverImageId`),
  KEY `Post_CreatedBy_FK_IDX` (`CreatedBy`),
  KEY `Post_PromptId_FK_IDX` (`PromptId`),
  KEY `Post_TopicId_FK_IDX` (`TopicId`),
  CONSTRAINT `Post_CoverImageId_FK` FOREIGN KEY (`CoverImageId`) REFERENCES `Image` (`ImageId`) ON DELETE SET NULL,
  CONSTRAINT `Post_CreatedBy_FK` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `Post_PromptId_FK` FOREIGN KEY (`PromptId`) REFERENCES `Post` (`PostId`) ON DELETE SET NULL,
  CONSTRAINT `Post_TopicId_FK` FOREIGN KEY (`TopicId`) REFERENCES `Topic` (`TopicId`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PostBlock`
--

DROP TABLE IF EXISTS `PostBlock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PostBlock` (
  `PostBlockId` char(22) COLLATE utf8mb4_bin NOT NULL,
  `PostId` char(22) COLLATE utf8mb4_bin NOT NULL,
  `CreatedBy` char(22) COLLATE utf8mb4_bin NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PostBlockId`),
  UNIQUE KEY `PostBlock_PostBlockId_UNIQUE` (`PostBlockId`),
  UNIQUE KEY `PostBlock_PostId_CreatedBy_UNIQUE` (`PostId`,`CreatedBy`),
  KEY `PostBlock_CreatedBy_FK_IDX` (`CreatedBy`),
  CONSTRAINT `PostBlock_CreatedBy_FK` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `PostBlock_PostId_FK` FOREIGN KEY (`PostId`) REFERENCES `Post` (`PostId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PostBookmark`
--

DROP TABLE IF EXISTS `PostBookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PostBookmark` (
  `PostBookmarkId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `PostId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedBy` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PostBookmarkId`),
  UNIQUE KEY `PostBookmark_PostBookmarkId_UNIQUE` (`PostBookmarkId`),
  UNIQUE KEY `PostBookmark_PostId_CreatedBy_UNIQUE` (`PostId`,`CreatedBy`),
  KEY `PostBookmark_CreatedBy_FK_IDX` (`CreatedBy`),
  CONSTRAINT `PostBookmark_CreatedBy_FK` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `PostBookmark_PostId_FK` FOREIGN KEY (`PostId`) REFERENCES `Post` (`PostId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trPostBookmarkAfterInsert` AFTER INSERT ON `postbookmark` FOR EACH ROW BEGIN

	UPDATE wordtoast.Post p
    SET
		BookmarkCount = BookmarkCount + 1
	WHERE p.PostId = NEW.PostId;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trPostBookmarkAfterDelete` AFTER DELETE ON `postbookmark` FOR EACH ROW BEGIN

	UPDATE wordtoast.Post p
    SET
		BookmarkCount = CASE WHEN BookmarkCount > 0 THEN BookmarkCount - 1 ELSE 0 END
	WHERE p.PostId = OLD.PostId;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `PostComment`
--

DROP TABLE IF EXISTS `PostComment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PostComment` (
  `PostCommentId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `PostId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `Body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `NotificationId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `CreatedBy` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PostCommentId`),
  UNIQUE KEY `PostComment_PostCommentId_UNIQUE` (`PostCommentId`),
  KEY `PostComment_PostId_FK_IDX` (`PostId`),
  KEY `PostComment_CreatedBy_FK_IDX` (`CreatedBy`),
  CONSTRAINT `PostComment_CreatedBy_FK` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `PostComment_PostId_FK` FOREIGN KEY (`PostId`) REFERENCES `Post` (`PostId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trPostCommentAfterInsert` AFTER INSERT ON `postcomment` FOR EACH ROW BEGIN

	UPDATE wordtoast.Post p
    SET
		CommentCount = CommentCount + 1
	WHERE p.PostId = NEW.PostId;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trPostCommentAfterDelete` AFTER DELETE ON `postcomment` FOR EACH ROW BEGIN

	UPDATE wordtoast.Post p
    SET
		CommentCount = CASE WHEN CommentCount > 0 THEN CommentCount - 1 ELSE 0 END
	WHERE p.PostId = OLD.PostId;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `PostHashtag`
--

DROP TABLE IF EXISTS `PostHashtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PostHashtag` (
  `PostId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `HashtagId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`PostId`,`HashtagId`),
  KEY `PostHashtag_HashtagId_FK_IDX` (`HashtagId`),
  CONSTRAINT `PostHashtag_HashtagId_FK` FOREIGN KEY (`HashtagId`) REFERENCES `Hashtag` (`HashtagId`) ON DELETE CASCADE,
  CONSTRAINT `PostHashtag_PostId_FK` FOREIGN KEY (`PostId`) REFERENCES `Post` (`PostId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PostReaction`
--

DROP TABLE IF EXISTS `PostReaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PostReaction` (
  `PostReactionId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `PostId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `ReactionType` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedBy` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PostReactionId`),
  UNIQUE KEY `PostReaction_PostReactionId_UNIQUE` (`PostReactionId`),
  UNIQUE KEY `PostReaction_PostId_CreatedBy_UNIQUE` (`PostId`,`CreatedBy`),
  KEY `PostReaction_CreatedBy_FK_IDX` (`CreatedBy`),
  CONSTRAINT `PostReaction_CreatedBy_FK` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `PostReaction_PostId_FK` FOREIGN KEY (`PostId`) REFERENCES `Post` (`PostId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trPostReactionAfterInsert` AFTER INSERT ON `postreaction` FOR EACH ROW BEGIN

	UPDATE Post p
    SET
		p.LikeCount = IF(NEW.ReactionType = 'LIKE', p.LikeCount + 1, p.LikeCount),
		p.LoveCount = IF(NEW.ReactionType = 'LOVE', p.LoveCount + 1, p.LoveCount),
		p.HahaCount = IF(NEW.ReactionType = 'HAHA', p.HahaCount + 1, p.HahaCount),
		p.SadCount = IF(NEW.ReactionType = 'SAD', p.SadCount + 1, p.SadCount)
	WHERE p.PostId = NEW.PostId;

	UPDATE User u
	    INNER JOIN Post p ON p.CreatedBy = u.UserId
    SET
        u.ReactionPoints = u.ReactionPoints + 1
	WHERE p.PostId = NEW.PostId AND u.UserId != NEW.CreatedBy;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trPostReactionAfterDelete` AFTER DELETE ON `postreaction` FOR EACH ROW BEGIN

	UPDATE Post p
    SET
		p.LikeCount = IF(OLD.ReactionType = 'LIKE', CASE WHEN p.LikeCount > 0 THEN p.LikeCount - 1 ELSE 0 END, p.LikeCount),
		p.LoveCount = IF(OLD.ReactionType = 'LOVE', CASE WHEN p.LoveCount > 0 THEN p.LoveCount - 1 ELSE 0 END, p.LoveCount),
		p.HahaCount = IF(OLD.ReactionType = 'HAHA', CASE WHEN p.HahaCount > 0 THEN p.HahaCount - 1 ELSE 0 END, p.HahaCount),
		p.SadCount = IF(OLD.ReactionType = 'SAD', CASE WHEN p.SadCount > 0 THEN p.SadCount - 1 ELSE 0 END, p.SadCount)
	WHERE p.PostId = OLD.PostId;

	UPDATE User u
	    INNER JOIN Post p ON p.CreatedBy = u.UserId
    SET
        u.ReactionPoints = CASE WHEN u.ReactionPoints > 0 THEN u.ReactionPoints - 1 ELSE 0 END
	WHERE p.PostId = OLD.PostId AND u.UserId != OLD.CreatedBy;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Topic`
--

DROP TABLE IF EXISTS `Topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Topic` (
  `TopicId` char(22) COLLATE utf8mb4_bin NOT NULL,
  `Title` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `Description` text COLLATE utf8mb4_bin,
  `PosterImageId` char(22) COLLATE utf8mb4_bin DEFAULT NULL,
  `FollowerCount` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`TopicId`),
  UNIQUE KEY `Topic_TopicId_UNIQUE` (`TopicId`),
  KEY `Topic_PosterImageId_FK_IDX` (`PosterImageId`),
  CONSTRAINT `Topic_PosterImageId_FK` FOREIGN KEY (`PosterImageId`) REFERENCES `Image` (`ImageId`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TopicFollow`
--

DROP TABLE IF EXISTS `TopicFollow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `TopicFollow` (
  `TopicFollowId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TopicId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedBy` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`TopicFollowId`),
  UNIQUE KEY `TopicFollowId_UNIQUE` (`TopicFollowId`),
  KEY `TopicFollow_TopicId_FK_IDX` (`TopicId`),
  KEY `TopicFollow_UserId_FK_IDX` (`CreatedBy`),
  CONSTRAINT `TopicFollow_CreatedBy_FK` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `TopicFollow_TopicId_FK` FOREIGN KEY (`TopicId`) REFERENCES `Topic` (`TopicId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trTopicFollowAfterInsert` AFTER INSERT ON `topicfollow` FOR EACH ROW BEGIN

	UPDATE Topic t
    SET
		t.FollowerCount = t.FollowerCount + 1
	WHERE t.TopicId = NEW.TopicId;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trTopicFollowAfterDelete` AFTER DELETE ON `topicfollow` FOR EACH ROW BEGIN

	UPDATE Topic t
    SET
        t.FollowerCount = CASE WHEN t.FollowerCount > 0 THEN t.FollowerCount - 1 ELSE 0 END
	WHERE t.TopicId = OLD.TopicId;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `User` (
  `UserId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `DisplayName` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `Headline` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `Email` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `EmailVerified` tinyint unsigned NOT NULL DEFAULT '0',
  `PhoneNumber` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `PhoneNumberVerificationCode` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `PhoneNumberVerified` tinyint unsigned NOT NULL DEFAULT '0',
  `ProfileImageId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `FirebaseUserId` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `FacebookUserId` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `Handle` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `RoleType` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT 'MEMBER',
  `PlanType` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT 'FREE',
  `ReferralCode` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `ReferredCode` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `ReactionPoints` int unsigned NOT NULL DEFAULT '1',
  `FriendCount` smallint unsigned NOT NULL DEFAULT '0',
  `FollowerCount` smallint unsigned NOT NULL DEFAULT '0',
  `FollowingCount` smallint unsigned NOT NULL DEFAULT '0',
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `User_UserId_UNIQUE` (`UserId`),
  UNIQUE KEY `User_Email_UNIQUE` (`Email`),
  UNIQUE KEY `User_FirebaseUserId_UNIQUE` (`FirebaseUserId`),
  UNIQUE KEY `User_FacebookUserId_UNIQUE` (`FacebookUserId`),
  UNIQUE KEY `User_ReferralCode_UNIQUE` (`ReferralCode`),
  KEY `User_ProfileImageId_FK_IDX` (`ProfileImageId`),
  KEY `User_Handle_UNIQUE` (`Handle`),
  CONSTRAINT `User_ProfileImageId_FK` FOREIGN KEY (`ProfileImageId`) REFERENCES `Image` (`ImageId`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserBlock`
--

DROP TABLE IF EXISTS `UserBlock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UserBlock` (
  `UserBlockId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `UserId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedBy` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserBlockId`),
  UNIQUE KEY `UserBlock_UserBlockId_UNIQUE` (`UserBlockId`),
  UNIQUE KEY `UserBlock_UserId_CreatedBy_UNIQUE` (`UserId`,`CreatedBy`),
  KEY `UserBlock_CreatedBy_FK_IDX` (`CreatedBy`),
  CONSTRAINT `UserBlock_CreatedBy_FK` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `UserBlock_UserId_FK` FOREIGN KEY (`UserId`) REFERENCES `User` (`UserId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserFollow`
--

DROP TABLE IF EXISTS `UserFollow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UserFollow` (
  `UserFollowId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `UserId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedBy` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserFollowId`),
  UNIQUE KEY `UserFollow_UserFollowId_UNIQUE` (`UserFollowId`),
  KEY `UserFollow_UserId_FK_IDX` (`UserId`),
  KEY `UserFollow_CreatedBy_FK_IDX` (`CreatedBy`),
  CONSTRAINT `UserFollow_CreatedBy_FK` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `UserFollow_UserId_FK` FOREIGN KEY (`UserId`) REFERENCES `User` (`UserId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trUserFollowAfterInsert` AFTER INSERT ON `userfollow` FOR EACH ROW BEGIN

	UPDATE wordtoast.User u
    SET
		FollowerCount = FollowerCount + 1
	WHERE u.UserId = NEW.UserId;

	UPDATE wordtoast.User u
    SET
		FollowingCount = FollowingCount + 1
	WHERE u.UserId = NEW.CreatedBy;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trUserFollowAfterDelete` AFTER DELETE ON `userfollow` FOR EACH ROW BEGIN

	UPDATE wordtoast.User u
    SET
		FollowerCount = CASE WHEN FollowerCount > 0 THEN FollowerCount - 1 ELSE 0 END
	WHERE u.UserId = OLD.UserId;

	UPDATE wordtoast.User u
    SET
		FollowingCount = CASE WHEN FollowingCount > 0 THEN FollowingCount - 1 ELSE 0 END
	WHERE u.UserId = OLD.CreatedBy;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `UserFriend`
--

DROP TABLE IF EXISTS `UserFriend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UserFriend` (
  `UserFriendId` char(22) COLLATE utf8mb4_bin NOT NULL,
  `UserId` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedBy` char(22) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserFriendId`),
  UNIQUE KEY `UserFriend_UserFriendId_UNIQUE` (`UserFriendId`),
  UNIQUE KEY `UserFriend_UserId_CreatedBy_UNIQUE` (`UserId`,`CreatedBy`),
  KEY `UserFriend_CreatedBy_FK_IDX` (`CreatedBy`),
  CONSTRAINT `UserFriend_CreatedBy_FK` FOREIGN KEY (`CreatedBy`) REFERENCES `User` (`UserId`) ON DELETE CASCADE,
  CONSTRAINT `UserFriend_UserId_FK` FOREIGN KEY (`UserId`) REFERENCES `User` (`UserId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trUserFriendAfterInsert` AFTER INSERT ON `userfriend` FOR EACH ROW BEGIN

	UPDATE User u
    SET
		u.FriendCount = u.FriendCount + 1
	WHERE u.UserId = NEW.UserId;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trUserFriendAfterDelete` AFTER DELETE ON `userfriend` FOR EACH ROW BEGIN

	UPDATE User u
    SET
        u.FriendCount = CASE WHEN u.FriendCount > 0 THEN u.FriendCount - 1 ELSE 0 END
	WHERE u.UserId = OLD.UserId;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'wordtoast'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-22 13:58:56
